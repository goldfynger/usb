#include <stdbool.h>
#include "stm32f1xx_hal.h"
#include "main.h"
#include "board_blink.h"


void BOARD_BLINK_SetBlueLedState(bool state)
{
    HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, state ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

void BOARD_BLINK_SetGreenLedState(bool state)
{
    HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, state ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

void BOARD_BLINK_SetRedLedState(bool state)
{
    HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, state ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

void BOARD_BLINK_SetYellowLedState(bool state)
{
    HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, state ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

bool BOARD_BLINK_GetBlueButtonState(void)
{
    /* Button pushed to ground. */
    return HAL_GPIO_ReadPin(BTN_BLUE_GPIO_Port, BTN_BLUE_Pin) == GPIO_PIN_RESET;
}

bool BOARD_BLINK_GetGreenButtonState(void)
{
    /* Button pushed to ground. */
    return HAL_GPIO_ReadPin(BTN_GREEN_GPIO_Port, BTN_GREEN_Pin) == GPIO_PIN_RESET;
}

bool BOARD_BLINK_GetRedButtonState(void)
{
    /* Button pushed to ground. */
    return HAL_GPIO_ReadPin(BTN_RED_GPIO_Port, BTN_RED_Pin) == GPIO_PIN_RESET;
}

bool BOARD_BLINK_GetYellowButtonState(void)
{
    /* Button pushed to ground. */
    return HAL_GPIO_ReadPin(BTN_YELLOW_GPIO_Port, BTN_YELLOW_Pin) == GPIO_PIN_RESET;
}
