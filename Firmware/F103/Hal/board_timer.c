#include "main.h"
#include "board.h"


/* Weak callback. */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    BOARD_ToggleStateLed();
}
