#ifndef __BOARD_H
#define __BOARD_H


void BOARD_SetUsbPullup(void);
void BOARD_ResetUsbPullup(void);
void BOARD_ToggleStateLed(void);


#endif /* __BOARD_H */
