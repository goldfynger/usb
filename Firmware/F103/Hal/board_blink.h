#ifndef __BLINK_LEDS_H
#define __BLINK_LEDS_H


#include <stdbool.h>


void BOARD_BLINK_SetBlueLedState(bool state);
void BOARD_BLINK_SetGreenLedState(bool state);
void BOARD_BLINK_SetRedLedState(bool state);
void BOARD_BLINK_SetYellowLedState(bool state);
bool BOARD_BLINK_GetBlueButtonState(void);
bool BOARD_BLINK_GetGreenButtonState(void);
bool BOARD_BLINK_GetRedButtonState(void);
bool BOARD_BLINK_GetYellowButtonState(void);


#endif /* __BLINK_LEDS_H */
