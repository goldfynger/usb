#ifndef __BUTTONS_H
#define __BUTTONS_H


#include <stdbool.h>
#include <stdint.h>


#define BUTTONS_COUNT 4
#define COUNT_TO_STABLE 5


typedef struct
{
    uint16_t StateCounter;
    bool StableValue;
    bool LastValue;
}
BUTTONS_StateTypeDef;


void BUTTONS_RegisterSendStateCallback(void (*SendState) (uint8_t state));
void BUTTONS_CheckState(void);


#endif /* __BUTTONS_H */
