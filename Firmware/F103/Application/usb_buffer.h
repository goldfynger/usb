#ifndef __USB_BUFFER_H
#define __USB_BUFFER_H


#include <stdint.h>


typedef struct
{
    /* Address of first byte of record in buffer (Buffer[RecordIndex]) */
    uint16_t RecordIndex;
    /* Length of record in buffer (Buffer[RecordIndex + RecordLength]) */
    uint16_t RecordLength;
}
USB_BUFFER_Record;

typedef struct
{
    /* Index of next record for read (Records[ReadPosition]) */
    uint16_t ReadPosition;
    /* Index of next record for write (Records[WritePosition]) */
    uint16_t WritePosition;
    
    /* Length of Records buffer */
    uint16_t RecordsLength;
    /* Length of Buffer */
    uint16_t BufferLength;
    
    /* Buffer with info about records */
    USB_BUFFER_Record *Records;
    /* Buffer with records */
    uint8_t *Buffer;
    
    /* Callback for data send (called from SendIfNeed()) */
    void (*SendData) (uint8_t *data, uint16_t length);
}
USB_BUFFER_HandleTypeDef;


void USB_BUFFER_SendIfNeed(USB_BUFFER_HandleTypeDef *hUsbBuf);
void USB_BUFFER_WriteRecord(USB_BUFFER_HandleTypeDef *hUsbBuf, uint8_t *data, uint16_t length);


#endif /* __USB_BUFFER_H */
