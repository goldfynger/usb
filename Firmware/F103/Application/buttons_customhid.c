#include "usb_customhid_api.h"
#include "usb_customhid_config.h"
#include "trace.h"
#include "board_blink.h"
#include "buttons.h"
#include "buttons_customhid.h"


static void BUTTONS_CUSTOMHID_SendState(uint8_t state);


void BUTTONS_CUSTOMHID_Init(void)
{
    BUTTONS_RegisterSendStateCallback(BUTTONS_CUSTOMHID_SendState);
}

void BUTTONS_CUSTOMHID_Process(void)
{
    BUTTONS_CheckState();
}

static void BUTTONS_CUSTOMHID_SendState(uint8_t state)
{
    static uint8_t stateToSend[USB_CUSTOMHID_CONFIG_REPORT_SIZE] = {0};
    
    if (!USB_CUSTOMHID_API_IsTransmitBusy())
    {
        stateToSend[0] = state;
        
        USB_CUSTOMHID_API_Transmit(stateToSend);
    }
}

/* Weak callback */
void USB_CUSTOMHID_API_ReceiveCompleteCallback(uint8_t *pBuffer)
{
    uint8_t state = pBuffer[0];
        
    bool isBlue     = (state & 0x01) ? true : false;
    bool isGreen    = (state & 0x02) ? true : false;
    bool isRed      = (state & 0x04) ? true : false;
    bool isYellow   = (state & 0x08) ? true : false;
        
    BOARD_BLINK_SetBlueLedState(isBlue);
    BOARD_BLINK_SetGreenLedState(isGreen);
    BOARD_BLINK_SetRedLedState(isRed);
    BOARD_BLINK_SetYellowLedState(isYellow);
            
    TRACE_Format("USB Event B:%u G:%u R:%u Y:%u\r\n", isBlue, isGreen, isRed, isYellow); /* Use printf inside USB interrupt it is not good idea. */
}
