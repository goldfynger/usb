#ifndef __BUTTONS_CDC_H
#define __BUTTONS_CDC_H


#include <stdint.h>


void BUTTONS_CDC_Init(void);
void BUTTONS_CDC_Process(void);


#endif /* __BUTTONS_CDC_H */
