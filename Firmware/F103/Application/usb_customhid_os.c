#include <string.h>
#include "usb_customhid_api.h"
#include "cmsis_os2.h"
#include "usb_customhid_os.h"


extern osMessageQueueId_t customHidInQueueHandle;


void USB_CUSTOMHID_OS_Loop(void)
{
    static USB_CUSTOMHID_OS_QueueItemTypeDef itemToTransmit;
    
    while(USB_CUSTOMHID_API_IsTransmitBusy())
    {
        osDelay(1);
    }
    
    while (true)
    {
        osMessageQueueGet(customHidInQueueHandle, &itemToTransmit, NULL, osWaitForever);
                                
        while(USB_CUSTOMHID_API_IsTransmitBusy())
        {
            osDelay(1);
        }
        
        USB_CUSTOMHID_API_Transmit(itemToTransmit.Report);
    }
}

/* Weak callback */
void USB_CUSTOMHID_API_ReceiveCompleteCallback(uint8_t *pBuffer)
{
    osMessageQueuePut(customHidInQueueHandle, pBuffer, NULL, 0);
}
