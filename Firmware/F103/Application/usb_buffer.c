#include "usb_buffer.h"


inline static int32_t __restore_irq(int32_t wasMasked)
{
    if (!wasMasked)
        __enable_irq();
    return 0;
}

#define ATOMIC_RESTORESTATE for (int32_t wasMasked = __disable_irq(), flag = 1; flag; flag = __restore_irq(wasMasked))


void USB_BUFFER_SendIfNeed(USB_BUFFER_HandleTypeDef *hUsbBuf)
{
    uint16_t writePosition = 0;
    
    ATOMIC_RESTORESTATE
    {    
        writePosition = hUsbBuf->WritePosition;
    }
    
    uint16_t readPosition = hUsbBuf->ReadPosition;
    
    if (readPosition == writePosition)
        return;
    
    
    USB_BUFFER_Record *pCurrentRecord = &(hUsbBuf->Records[readPosition]);
    
    uint16_t readIndex = pCurrentRecord->RecordIndex;
    uint16_t readLength =  pCurrentRecord->RecordLength;
    
    
    uint16_t nextReadPosition = readPosition + 1;
    
    if (nextReadPosition == hUsbBuf->RecordsLength)
        nextReadPosition = 0;
    
    hUsbBuf->ReadPosition = nextReadPosition;
    
    
    hUsbBuf->SendData(&(hUsbBuf->Buffer[readIndex]), readLength);
}

void USB_BUFFER_WriteRecord(USB_BUFFER_HandleTypeDef *hUsbBuf, uint8_t* data, uint16_t length)
{
    uint16_t writePosition = 0;
    
    ATOMIC_RESTORESTATE
    {
        writePosition = hUsbBuf->WritePosition;
    }
    
    
    USB_BUFFER_Record *pCurrentRecord = &(hUsbBuf->Records[writePosition]);    
    
    uint16_t writeIndex = pCurrentRecord->RecordIndex;
    
    /* Data must be written to buffer without carry */
    if (writeIndex + length >= hUsbBuf->BufferLength)
    {
        writeIndex = 0;
        pCurrentRecord->RecordIndex = 0;
    }
    
    pCurrentRecord->RecordLength = length;
    
        
    for (uint16_t i = 0; i < length; i++)
    {
        hUsbBuf->Buffer[writeIndex++] = data[i];
    }
    
    
    uint16_t nextWritePosition = writePosition + 1;
    
    if (nextWritePosition == hUsbBuf->RecordsLength)
        nextWritePosition = 0;
    
    ATOMIC_RESTORESTATE
    {    
        hUsbBuf->WritePosition = nextWritePosition;
    }
    
    
    USB_BUFFER_Record *pNextRecord = &(hUsbBuf->Records[nextWritePosition]);   
    
    pNextRecord->RecordIndex = writeIndex;
}
