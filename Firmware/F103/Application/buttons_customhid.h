#ifndef __BUTTONS_CUSTOMHID_H
#define __BUTTONS_CUSTOMHID_H


#include <stdint.h>


void BUTTONS_CUSTOMHID_Init(void);
void BUTTONS_CUSTOMHID_Process(void);


#endif /* __BUTTONS_CUSTOMHID_H */
