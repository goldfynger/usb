#ifndef __USB_BUFFER_CDC_H
#define __USB_BUFFER_CDC_H


#include "usb_buffer.h"


#define USB_BUFFEF_CDC_BUFFER_SIZE          4096
#define USB_BUFFER_CDC_MAX_RECORD_LENGTH    64
#define USB_BUFFER_CDC_MAX_RECORDS          (USB_BUFFEF_CDC_BUFFER_SIZE / USB_BUFFER_CDC_MAX_RECORD_LENGTH)


void USB_BUFFER_CDC_Init(void);
void USB_BUFFER_CDC_SendIfNeed(void);


#endif /* __USB_BUFFER_CDC_H */
