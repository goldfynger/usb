//#include <stdio.h>
#include "trace.h"
#include "usb_cdc_api.h"
#include "board_blink.h"
#include "buttons.h"
#include "buttons_cdc.h"


static void BUTTONS_CDC_SendState(uint8_t state);


void BUTTONS_CDC_Init(void)
{
    BUTTONS_RegisterSendStateCallback(BUTTONS_CDC_SendState);
}

void BUTTONS_CDC_Process(void)
{
    BUTTONS_CheckState();
}

static void BUTTONS_CDC_SendState(uint8_t state)
{
    static uint8_t stateToSend = 0;
    
    if (!USB_CDC_API_IsTransmitBusy())
    {
        stateToSend = state;
        
        USB_CDC_API_Transmit(&stateToSend, 1);
    }
}

/* Weak callback */
void USB_CDC_API_ReceiveCompleteCallback(uint8_t *pBuffer, uint16_t length)
{
    if (length > 0)
    {    
        uint8_t state = pBuffer[0];
        
        bool isBlue     = (state & 0x01) ? true : false;
        bool isGreen    = (state & 0x02) ? true : false;
        bool isRed      = (state & 0x04) ? true : false;
        bool isYellow   = (state & 0x08) ? true : false;
        
        BOARD_BLINK_SetBlueLedState(isBlue);
        BOARD_BLINK_SetGreenLedState(isGreen);
        BOARD_BLINK_SetRedLedState(isRed);
        BOARD_BLINK_SetYellowLedState(isYellow);
            
        TRACE_Format("USB Event B:%u G:%u R:%u Y:%u\r\n", isBlue, isGreen, isRed, isYellow);
    }
}
