#include <stdbool.h>
#include "usb_cdc_api.h"
#include "usb_buffer_cdc.h"


static USB_BUFFER_HandleTypeDef _hUsbBuf;

static uint8_t _buffer[USB_BUFFEF_CDC_BUFFER_SIZE];

static USB_BUFFER_Record _records[USB_BUFFER_CDC_MAX_RECORDS];


static void USB_BUFFER_CDC_SendData(uint8_t *pData, uint16_t length);


void USB_BUFFER_CDC_Init(void)
{
    _hUsbBuf.BufferLength = USB_BUFFEF_CDC_BUFFER_SIZE;
    _hUsbBuf.Buffer = _buffer;
    
    _hUsbBuf.RecordsLength = USB_BUFFER_CDC_MAX_RECORDS;
    _hUsbBuf.Records = _records;
    
    _hUsbBuf.SendData = USB_BUFFER_CDC_SendData;
}

void USB_BUFFER_CDC_SendIfNeed(void)
{    
    if (!USB_CDC_API_IsTransmitBusy())
    {    
        USB_BUFFER_SendIfNeed(&_hUsbBuf);
    }
}

static void USB_BUFFER_CDC_SendData(uint8_t *pData, uint16_t length)
{
    if (!USB_CDC_API_IsTransmitBusy())
    {
        USB_CDC_API_Transmit(pData, length);
    }
}

/* Weak callback */
void USB_CDC_API_ReceiveCompleteCallback(uint8_t *pBuffer, uint16_t length)
{    
    USB_BUFFER_WriteRecord(&_hUsbBuf, pBuffer, length);
}
