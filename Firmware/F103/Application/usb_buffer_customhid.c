#include <stdbool.h>
#include "usb_customhid_api.h"
#include "usb_buffer_customhid.h"
#include "usb_customhid_config.h"


static USB_BUFFER_HandleTypeDef _hUsbBuf;

static uint8_t _buffer[USB_BUFFEF_CUSTOMHID_BUFFER_SIZE];

static USB_BUFFER_Record _records[USB_BUFFER_CUSTOMHID_MAX_RECORDS];


static void USB_BUFFER_CUSTOMHID_SendData(uint8_t *pData, uint16_t length);


void USB_BUFFER_CUSTOMHID_Init(void)
{
    _hUsbBuf.BufferLength = USB_BUFFEF_CUSTOMHID_BUFFER_SIZE;
    _hUsbBuf.Buffer = _buffer;
    
    _hUsbBuf.RecordsLength = USB_BUFFER_CUSTOMHID_MAX_RECORDS;
    _hUsbBuf.Records = _records;
    
    _hUsbBuf.SendData = USB_BUFFER_CUSTOMHID_SendData;
}

void USB_BUFFER_CUSTOMHID_SendIfNeed(void)
{    
    if (!USB_CUSTOMHID_API_IsTransmitBusy())
    {    
        USB_BUFFER_SendIfNeed(&_hUsbBuf);
    }
}

static void USB_BUFFER_CUSTOMHID_SendData(uint8_t *pData, uint16_t length)
{
    if (!USB_CUSTOMHID_API_IsTransmitBusy())
    {
        USB_CUSTOMHID_API_Transmit(pData);
    }
}

/* Weak callback */
void USB_CUSTOMHID_API_ReceiveCompleteCallback(uint8_t *pBuffer)
{
    USB_BUFFER_WriteRecord(&_hUsbBuf, pBuffer, USB_CUSTOMHID_CONFIG_REPORT_SIZE);
}
