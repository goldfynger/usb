#ifndef __USB_CUSTOMHID_OS_H
#define __USB_CUSTOMHID_OS_H


#include <stdint.h>
#include "usb_customhid_api.h"


typedef struct
{
    uint8_t Report[64];
}
USB_CUSTOMHID_OS_QueueItemTypeDef;


void USB_CUSTOMHID_OS_Loop(void);


#endif /* __USB_CUSTOMHID_OS_H */
