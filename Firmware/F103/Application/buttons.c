#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include "trace.h"
#include "board_blink.h"
#include "buttons.h"


static void (*_SendState) (uint8_t state);


void BUTTONS_RegisterSendStateCallback(void (*SendState) (uint8_t state))
{
    _SendState = SendState;
}

void BUTTONS_CheckState(void)
{
    static BUTTONS_StateTypeDef btnStates[BUTTONS_COUNT];
    
    bool btnCurentStates[BUTTONS_COUNT] = {false};
    
    btnCurentStates[0] = BOARD_BLINK_GetBlueButtonState();
    btnCurentStates[1] = BOARD_BLINK_GetGreenButtonState();
    btnCurentStates[2] = BOARD_BLINK_GetRedButtonState();
    btnCurentStates[3] = BOARD_BLINK_GetYellowButtonState();
    
    uint8_t stateForSend = 0;    
    
    for (uint8_t i = 0; i < BUTTONS_COUNT; i++)
    {
        // Last check value is equal to current.
        if (btnStates[i].LastValue == btnCurentStates[i])
        {
            // Increment counter.
            if (btnStates[i].StateCounter < UINT16_MAX)
            {            
                btnStates[i].StateCounter++;
            }
            
            
            // If StateCounter == COUNT_TO_STABLE then update StableValue and change led.
            if ((btnStates[i].StateCounter == COUNT_TO_STABLE) && (btnStates[i].StableValue != btnStates[i].LastValue))
            {
                btnStates[i].StableValue = btnStates[i].LastValue;
                
                
                // If button is pushed.
                if (btnStates[i].StableValue)
                {                    
                    stateForSend |= 1 << i;
                    
                    TRACE_Format("Button %u pushed (B:0 G:1 R:2 Y:3)\r\n", i);
                }
            }
        }
        else // Else clear counter.
        {
            btnStates[i].LastValue = btnCurentStates[i];
            btnStates[i].StateCounter = 0;
        }
    }
    
    if (stateForSend) // If some buttons stable pushed.
    {
        if (_SendState != NULL)
        {
            _SendState(stateForSend);
        }
        
        TRACE_Format("Send state %u (0b0000YRGB)\r\n", stateForSend);
    }
}
