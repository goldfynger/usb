#ifndef __APP_USB_CUSTOMHID_H
#define __APP_USB_CUSTOMHID_H


#include <stdbool.h>
#include <stdint.h>


#define APP_USB_CUSTOMHID_REPORT_LENGTH 64


bool APP_USB_CUSTOMHID_Transmit(uint8_t *pBuffer);
bool APP_USB_CUSTOMHID_IsTransmitBusy(void);
void APP_USB_CUSTOMHID_RegisterTransmitCompleteCallback(void (*TransmitCompleteCallback) (void));
void APP_USB_CUSTOMHID_RegisterReceiveCompleteCallback(void (*ReceiveCompleteCallback) (uint8_t *pBuffer));
void APP_USB_CUSTOMHID_Initialize(void);
bool APP_USB_CUSTOMHID_IsConnected(void);


#endif /* __APP_USB_CUSTOMHID_H */
