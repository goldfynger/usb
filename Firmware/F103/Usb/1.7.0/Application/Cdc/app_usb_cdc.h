#ifndef __APP_USB_CDC_H
#define __APP_USB_CDC_H


#include <stdbool.h>
#include <stdint.h>


bool APP_USB_CDC_Transmit(uint8_t *pBuffer, uint16_t length);
bool APP_USB_CDC_IsTransmitBusy(void);
void APP_USB_CDC_RegisterTransmitCompleteCallback(void (*TransmitCompleteCallback) (void));
void APP_USB_CDC_RegisterReceiveCompleteCallback(void (*ReceiveCompleteCallback) (uint8_t *pBuffer, uint16_t length));
void APP_USB_CDC_Initialize(void);
bool APP_USB_CDC_IsConnected(void);


#endif /* __APP_USB_CDC_H */
