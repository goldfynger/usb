#ifndef __USB_CUSTOMHID_API_H
#define __USB_CUSTOMHID_API_H


#include <stdbool.h>
#include <stdint.h>


bool USB_CUSTOMHID_API_Transmit(uint8_t *pBuffer);
bool USB_CUSTOMHID_API_IsTransmitBusy(void);
void USB_CUSTOMHID_API_Initialize(void);
bool USB_CUSTOMHID_API_IsConnected(void);

void USB_CUSTOMHID_API_TransmitCompleteCallback(void);
void USB_CUSTOMHID_API_ReceiveCompleteCallback(uint8_t *pBuffer);


#endif /* __USB_CUSTOMHID_API_H */
