#ifndef __USB_CDC_API_H
#define __USB_CDC_API_H


#include <stdbool.h>
#include <stdint.h>


bool USB_CDC_API_Transmit(uint8_t *pBuffer, uint16_t length);
bool USB_CDC_API_IsTransmitBusy(void);
void USB_CDC_API_Initialize(void);
bool USB_CDC_API_IsConnected(void);

void USB_CDC_API_TransmitCompleteCallback(void);
void USB_CDC_API_ReceiveCompleteCallback(uint8_t *pBuffer, uint16_t length);


#endif /* __USB_CDC_API_H */
