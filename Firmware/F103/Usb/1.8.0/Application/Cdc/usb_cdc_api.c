#include <stdbool.h>
#include <stdint.h>
#include "usbd_def.h"
#include "usb_device.h"
#include "usbd_cdc.h"
#include "usbd_cdc_if.h"
#include "usb_cdc_api.h"


bool USB_CDC_API_Transmit(uint8_t *pBuffer, uint16_t length)
{
    if (USB_CDC_API_IsTransmitBusy())
    {
        return false;
    }
    
    return CDC_Transmit_FS(pBuffer, length) == USBD_OK;
}

bool USB_CDC_API_IsTransmitBusy(void)
{
    return CDC_IsTransmitBusy();
}

void USB_CDC_API_Initialize(void)
{
    MX_USB_DEVICE_Init();
    MX_USB_DEVICE_HardwareStart();
}

bool USB_CDC_API_IsConnected(void)
{
    return MX_USB_DEVICE_IsConfigured();
}

__weak void USB_CDC_API_TransmitCompleteCallback(void)
{
}

__weak void USB_CDC_API_ReceiveCompleteCallback(uint8_t *pBuffer, uint16_t length)
{
    UNUSED(pBuffer);
}
