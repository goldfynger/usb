/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : usbd_conf.h
  * @version        : v2.0_Cube
  * @brief          : Header for usbd_conf.c file.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USBD_CONF__H__
#define __USBD_CONF__H__

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "stm32f1xx.h"
#include "stm32f1xx_hal.h"

/* USER CODE BEGIN INCLUDE */
#include "usb_cdc_config.h"
/* USER CODE END INCLUDE */

/** @addtogroup USBD_OTG_DRIVER
  * @{
  */

/** @defgroup USBD_CONF USBD_CONF
  * @brief Configuration file for Usb otg low level driver.
  * @{
  */

/** @defgroup USBD_CONF_Exported_Variables USBD_CONF_Exported_Variables
  * @brief Public variables.
  * @{
  */

/**
  * @}
  */

/** @defgroup USBD_CONF_Exported_Defines USBD_CONF_Exported_Defines
  * @brief Defines for configuration of the Usb device.
  * @{
  */

/*---------- -----------*/
#define USBD_MAX_NUM_INTERFACES     1
/*---------- -----------*/
#define USBD_MAX_NUM_CONFIGURATION     1
/*---------- -----------*/
#define USBD_MAX_STR_DESC_SIZ     /* USER CODE *//*512*/USB_CDC_CONFIG_MAX_STRING_DESCRIPTOR_SIZE/* USER CODE */
/*---------- -----------*/
#define USBD_DEBUG_LEVEL     /* USER CODE *//*3*/USB_CDC_CONFIG_DEBUG_LEVEL/* USER CODE */
/*---------- -----------*/
#define USBD_SELF_POWERED          /* USER CODE *//*0*/USB_CDC_CONFIG_SELF_POWERED/* USER CODE */
/*---------- -----------*/
#define MAX_STATIC_ALLOC_SIZE     512

/****************************************/
/* #define for FS and HS identification */
#define DEVICE_FS 		0

/**
  * @}
  */

/** @defgroup USBD_CONF_Exported_Macros USBD_CONF_Exported_Macros
  * @brief Aliases.
  * @{
  */

/* Memory management macros */

/** Alias for memory allocation. */
/* USER CODE *//*#define USBD_malloc         (uint32_t *)USBD_static_malloc*//* USER CODE */

/** Alias for memory release. */
/* USER CODE *//*#define USBD_free           USBD_static_free*//* USER CODE */

/** Alias for memory set. */
#define USBD_memset         /* Not used */

/** Alias for memory copy. */
#define USBD_memcpy         /* Not used */

/** Alias for delay. */
#define USBD_Delay          HAL_Delay

/* For footprint reasons and since only one allocation is handled in the HID class
   driver, the malloc/free is changed into a static allocation method */
/*void *USBD_static_malloc(uint32_t size);
void USBD_static_free(void *p);*/

/* DEBUG macros */

#if (USBD_DEBUG_LEVEL > 0)
#define USBD_UsrLog(...)    USB_CDC_CONFIG_TRACE_FORMAT(__VA_ARGS__);\
                            USB_CDC_CONFIG_TRACE_FORMAT("\n");
#else
#define USBD_UsrLog(...)
#endif

#if (USBD_DEBUG_LEVEL > 1)

#define USBD_ErrLog(...)    USB_CDC_CONFIG_TRACE_FORMAT("ERROR: ") ;\
                            USB_CDC_CONFIG_TRACE_FORMAT(__VA_ARGS__);\
                            USB_CDC_CONFIG_TRACE_FORMAT("\n");
#else
#define USBD_ErrLog(...)
#endif

#if (USBD_DEBUG_LEVEL > 2)
#define USBD_DbgLog(...)    USB_CDC_CONFIG_TRACE_FORMAT("DEBUG : ") ;\
                            USB_CDC_CONFIG_TRACE_FORMAT(__VA_ARGS__);\
                            USB_CDC_CONFIG_TRACE_FORMAT("\n");
#else
#define USBD_DbgLog(...)
#endif

/**
  * @}
  */

/** @defgroup USBD_CONF_Exported_Types USBD_CONF_Exported_Types
  * @brief Types.
  * @{
  */

/**
  * @}
  */

/** @defgroup USBD_CONF_Exported_FunctionsPrototype USBD_CONF_Exported_FunctionsPrototype
  * @brief Declaration of public functions for Usb device.
  * @{
  */

/* Exported functions -------------------------------------------------------*/

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* __USBD_CONF__H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
