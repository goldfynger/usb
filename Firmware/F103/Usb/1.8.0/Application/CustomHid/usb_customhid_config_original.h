#ifndef __USB_CUSTOMHID_CONFIG_H
#define __USB_CUSTOMHID_CONFIG_H


#include "trace.h"


#define USB_CUSTOMHID_CONFIG_VID                            1155    /* Vendor ID. */
#define USB_CUSTOMHID_CONFIG_PID                            22352   /* Product ID. */
#define USB_CUSTOMHID_CONFIG_MAX_STRING_DESCRIPTOR_SIZE     64U     /* Max length of string descriptor (2 + (strlen(str) * 2)). strlen() - length of string w/o ending zero. USB used UTF-16. */

                                                         /* 4 8  16  24  32  40  48  56  64 */
                                                         /* ^ ^   ^   ^   ^   ^   ^   ^   ^ */
#define USB_CUSTOMHID_CONFIG_MANUFACTURER_STRING           "STMicroelectronics"
#define USB_CUSTOMHID_CONFIG_PRODUCT_STRING_FS             "STM32 Custom Human interface"
#define USB_CUSTOMHID_CONFIG_CONFIGURATION_STRING_FS       "Custom HID Config"
#define USB_CUSTOMHID_CONFIG_INTERFACE_STRING_FS           "Custom HID Interface"
                                                         /* ^ ^   ^   ^   ^   ^   ^   ^   ^ */
                                                         /* 4 8  16  24  32  40  48  56  64 */

#define USB_CUSTOMHID_CONFIG_SELF_POWERED                   0U      /* 1U - device powered from self source. */
#define USB_CUSTOMHID_CONFIG_MAX_POWER                      50U     /* (Value x2) == max current in mA. 250 (500 mA) max. */
#define USB_CUSTOMHID_CONFIG_POLLING_INTERVAL               1U      /* EP pool interval 1...255. */

#define USB_CUSTOMHID_CONFIG_EPIN_SIZE                      0x40U   /* Maximum size of custom HID in EP. */
#define USB_CUSTOMHID_CONFIG_EPOUT_SIZE                     0x40U   /* Maximum size of custom HID out EP. */

#define USB_CUSTOMHID_CONFIG_REPORT_SIZE                    64U     /* Size of custom HID report. */

#define USB_CUSTOMHID_CONFIG_DEBUG_LEVEL                    3       /* 0...3 */

#define USB_CUSTOMHID_CONFIG_RX0_INT_PREEMPTION_PRIORITY    0       /* 0...15 if preemtion priority used 4 bits. 0...4 is non-maskable interrupts in os2 by default. */
#define USB_CUSTOMHID_CONFIG_TX_INT_PREEMPTION_PRIORITY     0       /* 0...15 if preemtion priority used 4 bits. 0...4 is non-maskable interrupts in os2 by default. */

#define USB_CUSTOMHID_CONFIG_TRACE_FORMAT                   TRACE_Format


#endif /* __USB_CUSTOMHID_CONFIG_H */
