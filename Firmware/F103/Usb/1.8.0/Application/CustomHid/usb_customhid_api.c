#include <stdbool.h>
#include <stdint.h>
#include "usbd_def.h"
#include "usb_device.h"
#include "usbd_customhid.h"
#include "usbd_custom_hid_if.h"
#include "usb_customhid_config.h"
#include "usb_customhid_api.h"


bool USB_CUSTOMHID_API_Transmit(uint8_t *pBuffer)
{
    if (USB_CUSTOMHID_API_IsTransmitBusy())
    {
        return false;
    }
    
    return USBD_CUSTOM_HID_SendReport_FS(pBuffer, USB_CUSTOMHID_CONFIG_REPORT_SIZE) == USBD_OK;
}

bool USB_CUSTOMHID_API_IsTransmitBusy(void)
{
    return USBD_CUSTOM_HID_IsTransmitBusy();
}

void USB_CUSTOMHID_API_Initialize(void)
{
    MX_USB_DEVICE_Init();
    MX_USB_DEVICE_HardwareStart();
}

bool USB_CUSTOMHID_API_IsConnected(void)
{
    return MX_USB_DEVICE_IsConfigured();
}

__weak void USB_CUSTOMHID_API_TransmitCompleteCallback(void)
{
}

__weak void USB_CUSTOMHID_API_ReceiveCompleteCallback(uint8_t *pBuffer)
{
    UNUSED(pBuffer);
}
