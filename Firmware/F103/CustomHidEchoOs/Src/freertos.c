/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "board.h"
#include "trace.h"
#include "trace_config.h"
#include "usb_customhid_os.h"
#include "cmsis_os2_ex.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for customHidInTask */
osThreadId_t customHidInTaskHandle;
const osThreadAttr_t customHidInTask_attributes = {
  .name = "customHidInTask",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for customHidInQueue */
osMessageQueueId_t customHidInQueueHandle;
const osMessageQueueAttr_t customHidInQueue_attributes = {
  .name = "customHidInQueue"
};
/* Definitions for indicationTimer */
osTimerId_t indicationTimerHandle;
const osTimerAttr_t indicationTimer_attributes = {
  .name = "indicationTimer"
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
   
/* USER CODE END FunctionPrototypes */

void StartCustomHidInTask(void *argument);
void IndicationTimerCallback(void *argument);

extern void MX_USB_DEVICE_Init(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
       TRACE_InitialiseOs(osPriorityNormal7);
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* creation of indicationTimer */
  indicationTimerHandle = osTimerNew(IndicationTimerCallback, osTimerPeriodic, NULL, &indicationTimer_attributes);

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
    osTimerStart(indicationTimerHandle, 500);
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* creation of customHidInQueue */
  customHidInQueueHandle = osMessageQueueNew (64, 64, &customHidInQueue_attributes);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of customHidInTask */
  customHidInTaskHandle = osThreadNew(StartCustomHidInTask, NULL, &customHidInTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartCustomHidInTask */
/**
  * @brief  Function implementing the customHidInTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_StartCustomHidInTask */
void StartCustomHidInTask(void *argument)
{
  /* init code for USB_DEVICE */
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN StartCustomHidInTask */
    BOARD_SetUsbPullup();
    USB_CUSTOMHID_OS_Loop(); /* Loop here */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartCustomHidInTask */
}

/* IndicationTimerCallback function */
void IndicationTimerCallback(void *argument)
{
  /* USER CODE BEGIN IndicationTimerCallback */
    BOARD_ToggleStateLed();
  /* USER CODE END IndicationTimerCallback */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
     
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
