﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

using Goldfynger.ExchangeMonitor.ConsoleView.Communication;

namespace Goldfynger.ExchangeMonitor.ConsoleView.Run
{
    internal class Program
    {
        private delegate bool ConsoleEventDelegate(int eventType);

        private static ConsoleEventDelegate _handler;

        private static async Task Main()
        {
            try
            {
#if DEBUG
                using var readMonitor = await MonitorActivator
                    .Activate(@"C:\_bitbucket\Usb\Software\ExchangeMonitor\Goldfynger.ExchangeMonitor.ConsoleView.Monitor\bin\Debug\net5.0-windows\Goldfynger.ExchangeMonitor.ConsoleView.Monitor.exe");
                using var writeMonitor = await MonitorActivator
                    .Activate(@"C:\_bitbucket\Usb\Software\ExchangeMonitor\Goldfynger.ExchangeMonitor.ConsoleView.Monitor\bin\Debug\net5.0-windows\Goldfynger.ExchangeMonitor.ConsoleView.Monitor.exe");
#else
                using var readMonitor = await MonitorActivator
                    .Activate(@"C:\_bitbucket\Usb\Software\ExchangeMonitor\Goldfynger.ExchangeMonitor.ConsoleView.Monitor\bin\Release\net5.0-windows\Goldfynger.ExchangeMonitor.ConsoleView.Monitor.exe");
                using var writeMonitor = await MonitorActivator
                    .Activate(@"C:\_bitbucket\Usb\Software\ExchangeMonitor\Goldfynger.ExchangeMonitor.ConsoleView.Monitor\bin\Release\net5.0-windows\Goldfynger.ExchangeMonitor.ConsoleView.Monitor.exe");
#endif

                _handler = new ConsoleEventDelegate(eventType =>
                {
                    if (eventType == 2)
                    {
                        readMonitor?.Dispose();
                        writeMonitor?.Dispose();
                    }
                    return false;
                });

                SetConsoleCtrlHandler(_handler, true);


                var array = new byte[256];

                for (var i = 0; i < array.Length; i++)
                {
                    array[i] = (byte)(i & 0xFF);
                }

                Console.WriteLine("Press ENTER to write 16-wide.");
                Console.ReadLine();

                readMonitor.Client.WriteInQueue(new MonitorSettingsMessage(new MonitorSettings(16, "Read monitor", ConsoleColor.Blue, ConsoleColor.Black, ConsoleColor.White)));
                writeMonitor.Client.WriteInQueue(new MonitorSettingsMessage(new MonitorSettings(16, "Write monitor", ConsoleColor.Red, ConsoleColor.Black, ConsoleColor.White)));

                readMonitor.Client.WriteInQueue(new MonitorDataMessage(new ReadOnlyCollection<byte>(array)));
                writeMonitor.Client.WriteInQueue(new MonitorDataMessage(new ReadOnlyCollection<byte>(array)));


                Console.WriteLine("Press ENTER to write 32-wide.");
                Console.ReadLine();

                readMonitor.Client.WriteInQueue(new MonitorSettingsMessage(new MonitorSettings(32, "Read monitor", ConsoleColor.Blue, ConsoleColor.Black, ConsoleColor.White)));
                writeMonitor.Client.WriteInQueue(new MonitorSettingsMessage(new MonitorSettings(32, "Write monitor", ConsoleColor.Red, ConsoleColor.Black, ConsoleColor.White)));

                readMonitor.Client.WriteInQueue(new MonitorDataMessage(new ReadOnlyCollection<byte>(array)));
                writeMonitor.Client.WriteInQueue(new MonitorDataMessage(new ReadOnlyCollection<byte>(array)));


                Console.WriteLine("Press ENTER to write 48-wide.");
                Console.ReadLine();

                readMonitor.Client.WriteInQueue(new MonitorSettingsMessage(new MonitorSettings(48, "Read monitor", ConsoleColor.Blue, ConsoleColor.Black, ConsoleColor.White)));
                writeMonitor.Client.WriteInQueue(new MonitorSettingsMessage(new MonitorSettings(48, "Write monitor", ConsoleColor.Red, ConsoleColor.Black, ConsoleColor.White)));

                readMonitor.Client.WriteInQueue(new MonitorDataMessage(new ReadOnlyCollection<byte>(array)));
                writeMonitor.Client.WriteInQueue(new MonitorDataMessage(new ReadOnlyCollection<byte>(array)));


                Console.WriteLine("Press ENTER to write 64-wide.");
                Console.ReadLine();

                readMonitor.Client.WriteInQueue(new MonitorSettingsMessage(new MonitorSettings(64, "Read monitor", ConsoleColor.Blue, ConsoleColor.Black, ConsoleColor.White)));
                writeMonitor.Client.WriteInQueue(new MonitorSettingsMessage(new MonitorSettings(64, "Write monitor", ConsoleColor.Red, ConsoleColor.Black, ConsoleColor.White)));

                readMonitor.Client.WriteInQueue(new MonitorDataMessage(new ReadOnlyCollection<byte>(array)));
                writeMonitor.Client.WriteInQueue(new MonitorDataMessage(new ReadOnlyCollection<byte>(array)));


                while (true)
                {
                    Console.Write("Write something to read monitor here: ");

                    readMonitor.Client.WriteInQueue(new MonitorStringMessage(Console.ReadLine()));

                    Console.Write("Write something to write monitor here: ");

                    writeMonitor.Client.WriteInQueue(new MonitorStringMessage(Console.ReadLine()));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message.Replace("\r", "").Replace("\n", "")}");
                Console.ReadLine();
                return;
            }
        }


        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetConsoleCtrlHandler(ConsoleEventDelegate callback, bool add);
    }
}
