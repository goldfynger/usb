﻿using System;
using System.Diagnostics;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Goldfynger.ExchangeMonitor.ConsoleView.Communication;

namespace Goldfynger.ExchangeMonitor.ConsoleView.Monitor
{
    internal class Program
    {
        private const string __dateTimeFormat = "HH:mm:ss.ffffff";

        private const string __byteFormat = "X2";

        private const string __decimalFormat = "D6";

        private const int __dateTimeFormatLength = 15;

        private const int __decimalFormatLength = 6;

        private const int __defaultWidth = 16;

        private static readonly int _serviceWidth = __dateTimeFormatLength + 1 + 1 + __decimalFormatLength + 1; /* "HH:mm:ss.ffffff:"...":000001 " */

        private static readonly int _maxWidth = Console.LargestWindowWidth - 1;

        private static int _bytesWidth = __defaultWidth;

        private static ConsoleColor _dataColor = ConsoleColor.Gray;

        private static ConsoleColor _messageColor = ConsoleColor.Gray;


        private static async Task Main(string[] args)
        {
            Trace.Listeners.Add(new ConsoleTraceListener());


            UpdateWidth(__defaultWidth);
            Console.CursorVisible = false;


            if (args.Length != 1 || !Guid.TryParse(args[0], out Guid guid))
            {
                if (args.Length != 0)
                {
                    Trace.WriteLine($"args[0]: {args[0]}");
                }

                Console.WriteLine("Input parameter must be GUID as name of the server.");
                Console.ReadLine();
                return;
            }


            Trace.WriteLine($"guid: {guid}");


            try
            {
                using var namedPipeServerStream = new NamedPipeServerStream(guid.ToString());

                Trace.WriteLine("Server named pipe started.");

                namedPipeServerStream.WaitForConnection();

                Trace.WriteLine("Client is connected.");

                using var communicator = new ServerCommunicator(namedPipeServerStream);

                while (true)
                {
                    var message = await communicator.ReadAsync();

                    switch (message.Type)
                    {
                        case MonitorMessageTypes.Settings:
                            var settingsMessage = message as MonitorSettingsMessage;
                            Trace.WriteLine($"Settings message: DateTime:{settingsMessage.DateTime.ToString(__dateTimeFormat)} WidthInBytes:{settingsMessage.Settings.WidthInBytes}.");
                            ApplySettings(settingsMessage.Settings);
                            break;

                        case MonitorMessageTypes.Data:
                            var dataMessage = message as MonitorDataMessage;
                            Trace.WriteLine($"Data message: DateTime:{dataMessage.DateTime.ToString(__dateTimeFormat)} Length:{dataMessage.Data.Count}.");
                            WriteData(dataMessage.Data.ToArray(), dataMessage.DateTime);
                            break;

                        case MonitorMessageTypes.String:
                            var stringMessage = message as MonitorStringMessage;
                            Trace.WriteLine($"String message: DateTime:{stringMessage.DateTime.ToString(__dateTimeFormat)}.");
                            Console.ForegroundColor = _messageColor;
                            Console.WriteLine($"{stringMessage.DateTime}:{stringMessage.Value}");
                            break;

                        case MonitorMessageTypes.Command:
                            var commandMessage = message as MonitorCommandMessage;
                            Trace.WriteLine($"String message: DateTime:{commandMessage.DateTime.ToString(__dateTimeFormat)} Command:{commandMessage.Command}.");
                            ExecuteCommand(commandMessage.Command);
                            break;

                        default:
                            throw new ApplicationException("Unknown type of message.");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message.Replace("\r", "").Replace("\n", "")}");
                Console.ReadLine();
                return;
            }
        }

        private static string WriteData(byte[] buffer, DateTime dateTime)
        {
            var lineLengthInBytes = _bytesWidth;
            var lineLengthInChars = _bytesWidth * 4;
            var lineLengthFull = lineLengthInChars + _serviceWidth;

            var isLastLineFull = buffer.Length % lineLengthInBytes == 0;
            var lastLineBytes = isLastLineFull ? lineLengthInBytes : buffer.Length % lineLengthInBytes;
            var lines = buffer.Length / lineLengthInBytes + (isLastLineFull ? 0 : 1);

            var bufferBuilder = new StringBuilder(lines * lineLengthFull);
            var lineBuffer = new char[lineLengthInChars];
            var stringRepresentation = new string(Encoding.ASCII.GetChars(buffer).Select(ch => char.IsLetterOrDigit(ch) || char.IsSymbol(ch) ? ch : '?').ToArray());

            for (var line = 0; line < lines; line++)
            {
                Console.ForegroundColor = _messageColor;
                Console.Write($"{dateTime.ToString(__dateTimeFormat)}:");

                var bytesInCurrentLine = (line == lines - 1) ? lastLineBytes : lineLengthInBytes;

                for (var charIdx = 0; charIdx < lineLengthInChars; charIdx++)
                {
                    lineBuffer[charIdx] = ' ';
                }

                for (var byteIdx = 0; byteIdx < bytesInCurrentLine; byteIdx++)
                {
                    var currentByte = buffer[(line * lineLengthInBytes) + byteIdx];
                    var byteRepresentation = currentByte.ToString(__byteFormat);
                    var charRepresentation = stringRepresentation[(line * lineLengthInBytes) + byteIdx];

                    lineBuffer[byteIdx * 3] = byteRepresentation[0];
                    lineBuffer[(byteIdx * 3) + 1] = byteRepresentation[1];
                    lineBuffer[(lineLengthInBytes * 3) + byteIdx] = charRepresentation;
                }

                Console.ForegroundColor = _dataColor;
                Console.Write($"{new string(lineBuffer)}");

                Console.ForegroundColor = _messageColor;
                Console.Write($":{((line * lineLengthInBytes) + bytesInCurrentLine).ToString(__decimalFormat)}{Environment.NewLine}");
            }

            return bufferBuilder.ToString();
        }

        private static void ApplySettings(MonitorSettings settings)
        {
            UpdateWidth(settings.WidthInBytes);
            Console.Title = settings.Title;
            _dataColor = settings.DataColor;
            _messageColor = settings.MessageColor;
            Console.BackgroundColor = settings.BackgroundColor;
        }

        private static void UpdateWidth(int newBytesWidth)
        {
            var newCharsWidth = newBytesWidth * 4;

            if ((_serviceWidth + newCharsWidth) > _maxWidth)
            {
                newCharsWidth = _maxWidth - _serviceWidth;
                _bytesWidth = newCharsWidth / 4;
            }
            else
            {
                _bytesWidth = newBytesWidth;
            }

            Console.SetWindowSize((_bytesWidth * 4) + _serviceWidth, Console.WindowHeight);

            Trace.WriteLine($"Console size updated with W:{Console.WindowWidth} H:{Console.WindowHeight}.");
        }

        private static void ExecuteCommand(MonitorCommandTypes command)
        {
            switch (command)
            {
                case MonitorCommandTypes.Close:
                    Environment.Exit(0);
                    break;

                default:
                    throw new ApplicationException("Unknown type of command.");
            }
        }
    }
}
