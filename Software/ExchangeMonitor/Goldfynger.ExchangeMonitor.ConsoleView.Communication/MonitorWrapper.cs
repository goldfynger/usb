﻿using System;
using System.Diagnostics;
using System.IO.Pipes;

namespace Goldfynger.ExchangeMonitor.ConsoleView.Communication
{
    /// <summary>
    /// Wrapper for Exchange Monitor.
    /// </summary>
    public sealed class MonitorWrapper : IDisposable
    {
        /// <summary>
        /// Flag: Has Dispose already been called?
        /// </summary>
        private bool _disposed = false;

        private readonly Process _process;

        internal MonitorWrapper(Process process, Guid guid)
        {
            _process = process;

            NamedPipeClientStream stream = null;

            try
            {
                stream = new NamedPipeClientStream(guid.ToString());

                stream.Connect();

                Client = new ClientCommunicator(stream);
            }
            catch
            {
                stream?.Dispose();
                Client?.Dispose();
                process?.Dispose();

                throw;
            }
        }


        /// <summary>
        /// Gets client for monitor connections.
        /// </summary>
        public ClientCommunicator Client { get; }


        /// <summary>
        /// Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing"></param>
        private void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                Client?.Dispose();
                _process?.Dispose();
            }

            _disposed = true;
        }
    }
}
