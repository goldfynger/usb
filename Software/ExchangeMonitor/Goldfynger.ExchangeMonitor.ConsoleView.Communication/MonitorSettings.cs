﻿using System;
using System.Runtime.Serialization;

namespace Goldfynger.ExchangeMonitor.ConsoleView.Communication
{
    /// <summary>
    /// Settings for monitor.
    /// </summary>
    [DataContract]
    public sealed class MonitorSettings
    {
        [DataMember]
        private readonly int _widthInBytes;

        [DataMember]
        private readonly string _title;

        [DataMember]
        private readonly ConsoleColor _dataColor;

        [DataMember]
        private readonly ConsoleColor _messageColor;

        [DataMember]
        private readonly ConsoleColor _backgroundColor;


        /// <summary>
        /// Creates new instance of settings.
        /// </summary>
        /// <param name="widthInBytes">Width of monitor window in bytes for show.</param>
        /// <param name="title">Title of window.</param>
        /// <param name="dataColor">Color of data.</param>
        /// <param name="messageColor">Color of message.</param>
        /// <param name="backgroundColor">Background color.</param>
        public MonitorSettings(int widthInBytes, string title, ConsoleColor dataColor, ConsoleColor messageColor, ConsoleColor backgroundColor)
        {
            if (widthInBytes < 1)
                throw new ArgumentOutOfRangeException(nameof(widthInBytes));
            _widthInBytes = widthInBytes;

            _title = title ?? throw new ArgumentNullException(nameof(title));

            _dataColor = dataColor;
            _messageColor = messageColor;
            _backgroundColor = backgroundColor;
        }


        /// <summary>
        /// Width of monitor window in bytes for show.
        /// </summary>
        public int WidthInBytes => _widthInBytes;

        /// <summary>
        /// Title of window.
        /// </summary>
        public string Title => _title;

        /// <summary>
        /// Color of data.
        /// </summary>
        public ConsoleColor DataColor => _dataColor;

        /// <summary>
        /// Color of message.
        /// </summary>
        public ConsoleColor MessageColor => _messageColor;

        /// <summary>
        /// Background color.
        /// </summary>
        public ConsoleColor BackgroundColor => _backgroundColor;
    }
}
