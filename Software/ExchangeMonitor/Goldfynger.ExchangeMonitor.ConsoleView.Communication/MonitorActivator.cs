﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Goldfynger.ExchangeMonitor.ConsoleView.Communication
{
    /// <summary>
    /// Contains method for <see cref="MonitorWrapper"/> activations.
    /// </summary>
    public static class MonitorActivator
    {
        /// <summary>
        /// Monitor executable file name.
        /// </summary>
        public const string MonitorFileName = "Goldfynger.ExchangeMonitor.ConsoleView.Monitor.exe";

        /// <summary>
        /// Activates new instance of <see cref="MonitorWrapper"/>.
        /// </summary>
        /// <param name="monitorPath">Path to monitor executable.</param>
        /// <returns><see cref="Task"/> for asynchronous activation.</returns>
        public static Task<MonitorWrapper> Activate(string monitorPath = MonitorFileName)
        {
            return Task.Run(() =>
            {
                var guid = Guid.NewGuid();

                var monitorProcess = new Process();
                monitorProcess.StartInfo.UseShellExecute = true; /* Start process in another window. */
                monitorProcess.StartInfo.FileName = monitorPath;
                monitorProcess.StartInfo.Arguments = guid.ToString();
                monitorProcess.Start();

                return new MonitorWrapper(monitorProcess, guid);
            });
        }
    }
}
