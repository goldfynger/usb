﻿using System;
using System.Collections.Concurrent;
using System.IO.Pipes;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Goldfynger.ExchangeMonitor.ConsoleView.Communication
{
    /// <summary>
    /// Contains communication infrastructure.
    /// </summary>
    public abstract class MonitorCommunicator : IDisposable
    {
        /// <summary>
        /// Flag: Has Dispose already been called?
        /// </summary>
        private bool _disposed = false;

        private const int __lengthFieldSize = 4;

        private protected readonly PipeStream _stream;

        private protected MonitorCommunicator(PipeStream stream)
        {
            _stream = stream ?? throw new ArgumentNullException(nameof(stream));

            if (!stream.IsConnected)
                throw new ApplicationException("Pipe stream must be connected.");
        }


        private protected MonitorMessage ReadPrivateProtected()
        {
            var lengthBuffer = new byte[__lengthFieldSize];
            if (_stream.Read(lengthBuffer, 0, __lengthFieldSize) != __lengthFieldSize)
                throw new ApplicationException("Invalid length of length field.");

            var messageLength = BitConverter.ToInt32(lengthBuffer, 0);
            if (messageLength < 0)
                throw new ApplicationException("Invalid length of message.");

            var readBuffer = new byte[messageLength];
            if (_stream.Read(readBuffer, 0, messageLength) != messageLength)
                throw new ApplicationException("Invalid length of message.");

            return MonitorMessage.ConvertFromJson(Encoding.UTF8.GetString(readBuffer));
        }

        private protected void WritePrivateProtected(MonitorMessage message)
        {
            var writeBuffer = Encoding.UTF8.GetBytes(message.ConvertToJson());

            var lengthBuffer = BitConverter.GetBytes(writeBuffer.Length);

            var commonBuffer = new byte[writeBuffer.Length + lengthBuffer.Length];

            Array.Copy(lengthBuffer, 0, commonBuffer, 0, lengthBuffer.Length);
            Array.Copy(writeBuffer, 0, commonBuffer, lengthBuffer.Length, writeBuffer.Length);            

            _stream.Write(commonBuffer, 0, commonBuffer.Length);

            _stream.Flush();
        }

        /// <summary>
        /// Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                _stream?.Dispose();
            }

            _disposed = true;
        }
    }

    /// <summary>
    /// Server-side infrastructure.
    /// </summary>
    public sealed class ServerCommunicator : MonitorCommunicator
    {
        /// <summary>
        /// Initialize a new instance of <see cref="ServerCommunicator"/> using <see cref="NamedPipeServerStream"/>.
        /// </summary>
        /// <param name="stream"><see cref="NamedPipeServerStream"/>.</param>
        public ServerCommunicator(NamedPipeServerStream stream) : base(stream)
        {
        }

        /// <summary>
        /// Read message from stream.
        /// </summary>
        /// <returns>Task for for asynchronous reading.</returns>
        public Task<MonitorMessage> ReadAsync()
        {
            return Task.Run(() =>
            {
                return ReadPrivateProtected();
            });
        }
    }

    /// <summary>
    /// Client-side infrastructure.
    /// </summary>
    public sealed class ClientCommunicator : MonitorCommunicator, IDisposable
    {
        /// <summary>
        /// Flag: Has Dispose already been called?
        /// </summary>
        private bool _disposed = false;

        private readonly ConcurrentQueue<MonitorMessage> _queue = new ConcurrentQueue<MonitorMessage>();

        private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(0);

        /// <summary>
        /// Initialize a new instance of <see cref="ClientCommunicator"/> using <see cref="NamedPipeClientStream"/>.
        /// </summary>
        /// <param name="stream"><see cref="NamedPipeClientStream"/>.</param>
        public ClientCommunicator(NamedPipeClientStream stream) : base(stream)
        {
            try
            {
                Task.Factory.StartNew(() =>
                {
                    while (true)
                    {
                        _semaphore.Wait();

                        var result = false;
                        MonitorMessage message = null;

                        do
                        {
                            result = _queue.TryDequeue(out message);

                            if (!result)
                            {
                                Thread.Sleep(1);
                            }
                        }
                        while (!result);

                        WritePrivateProtected(message);
                    }
                }, CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default);
            }
            catch
            {
                _semaphore?.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Write message in queue and return immediately.
        /// </summary>
        /// <param name="message">Message to monitor.</param>
        public void WriteInQueue(MonitorMessage message)
        {
            _queue.Enqueue(message);
            _semaphore.Release();
        }

        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                _semaphore?.Dispose();
                while (_queue.TryDequeue(out _))
                {
                }
                WritePrivateProtected(new MonitorCommandMessage(MonitorCommandTypes.Close));
            }

            _disposed = true;

            base.Dispose(disposing);
        }
    }
}
