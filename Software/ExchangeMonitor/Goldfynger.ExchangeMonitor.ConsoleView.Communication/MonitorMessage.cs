﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Goldfynger.ExchangeMonitor.ConsoleView.Communication
{
    /// <summary>
    /// Types of messages to monitor.
    /// </summary>
    public enum MonitorMessageTypes : int
    {
        /// <summary></summary>
        Settings,
        /// <summary></summary>
        Data,
        /// <summary></summary>
        String,
        /// <summary></summary>
        Command,
    }

    /// <summary>
    /// Types of command to monitor.
    /// </summary>
    public enum MonitorCommandTypes : int
    {
        /// <summary></summary>
        Close,
    }

    /// <summary>
    /// Base of message to monitor.
    /// </summary>
    [DataContract]
    public abstract class MonitorMessage
    {
        [DataMember]
        private readonly MonitorMessageTypes _type;

        [DataMember]
        private readonly DateTime _dateTime = DateTime.Now;

        
        private protected MonitorMessage(MonitorMessageTypes messageType)
        {
            _type = messageType;            
        }


        /// <summary>
        /// Type of this instace of message.
        /// </summary>
        public MonitorMessageTypes Type => _type;

        /// <summary>
        /// Date and time of message.
        /// </summary>
        public DateTime DateTime => _dateTime;


        internal string ConvertToJson()
        {
            var serializer = new DataContractJsonSerializer(typeof(MonitorMessage), new[]
            {
                typeof(MonitorSettingsMessage),
                typeof(MonitorDataMessage),
                typeof(MonitorStringMessage),
                typeof(MonitorCommandMessage)
            });

            using var memoryStream = new MemoryStream();

            serializer.WriteObject(memoryStream, this);

            memoryStream.Flush();

            memoryStream.Seek(0, SeekOrigin.Begin);

            return Encoding.UTF8.GetString(memoryStream.ToArray());
        }

        internal static MonitorMessage ConvertFromJson(string message)
        {
            var deserializer = new DataContractJsonSerializer(typeof(MonitorMessage), new[]
            {
                typeof(MonitorSettingsMessage),
                typeof(MonitorDataMessage),
                typeof(MonitorStringMessage),
                typeof(MonitorCommandMessage)
            });

            using var memoryStream = new MemoryStream();

            var array = Encoding.UTF8.GetBytes(message);

            memoryStream.Write(array, 0, array.Length);

            memoryStream.Flush();

            memoryStream.Seek(0, SeekOrigin.Begin);

            return deserializer.ReadObject(memoryStream) as MonitorMessage;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [DataContract]
    public abstract class MonitorMessage<T> : MonitorMessage
    {
        [DataMember]
        private readonly T _data;


        private protected MonitorMessage(MonitorMessageTypes messageType, T data) : base(messageType)
        {
            _data = data;
        }


        private protected T DataPrivateProtected => _data;
    }

    /// <summary>
    /// Message with settings.
    /// </summary>
    [DataContract]
    public sealed class MonitorSettingsMessage : MonitorMessage<MonitorSettings>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="settings"></param>
        public MonitorSettingsMessage(MonitorSettings settings) : base (MonitorMessageTypes.Settings, settings)
        {            
        }


        /// <summary>
        /// 
        /// </summary>
        public MonitorSettings Settings => DataPrivateProtected;
    }

    /// <summary>
    /// Message with data.
    /// </summary>
    [DataContract]
    public sealed class MonitorDataMessage : MonitorMessage<ReadOnlyCollection<byte>>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        public MonitorDataMessage(ReadOnlyCollection<byte> data) : base(MonitorMessageTypes.Data, data)
        {
        }


        /// <summary>
        /// 
        /// </summary>
        public ReadOnlyCollection<byte> Data => DataPrivateProtected;
    }

    /// <summary>
    /// Message with string.
    /// </summary>
    [DataContract]
    public sealed class MonitorStringMessage : MonitorMessage<string>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public MonitorStringMessage(string value) : base(MonitorMessageTypes.String, value)
        {
        }


        /// <summary>
        /// 
        /// </summary>
        public string Value => DataPrivateProtected;
    }

    /// <summary>
    /// Message with command.
    /// </summary>
    [DataContract]
    public sealed class MonitorCommandMessage : MonitorMessage<MonitorCommandTypes>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        public MonitorCommandMessage(MonitorCommandTypes command) : base(MonitorMessageTypes.Command, command)
        {
        }


        /// <summary>
        /// 
        /// </summary>
        public MonitorCommandTypes Command => DataPrivateProtected;
    }
}
