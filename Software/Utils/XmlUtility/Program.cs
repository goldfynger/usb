﻿using System;
using System.IO;
using System.Xml.Linq;

namespace XmlUtility
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Please use path to XML file as first command line argument. Example: C:\\doc.xml");

                Console.ReadLine();
                return;
            }

            if (!File.Exists(args[0]))
            {
                Console.WriteLine($"File {args[0]} does not exist.");

                Console.ReadLine();
                return;
            }

            try
            {
                var document = XDocument.Load(args[0]);

                Console.WriteLine(document.Declaration);
                Console.WriteLine(document);

                Console.ReadLine();
                return;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                Console.ReadLine();
                return;
            }
        }
    }
}
