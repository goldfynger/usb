﻿using System;

namespace IntToUintUtility
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Converts Int32 to UInt32 in hexadecimal representation.");

            while (true)
            {
                Console.Write("Enter integer:");
                var inputStr = Console.ReadLine();

                if (string.Compare("exit", inputStr, true) == 0 || string.Compare("quit", inputStr, true) == 0)
                    return;

                if (int.TryParse(inputStr, out var inputInt))
                {
                    Console.WriteLine($"Result: 0x{(uint)inputInt:X8}");
                }
                else
                {
                    Console.WriteLine("Can not convert parse as 32-bit integer. Try again.");
                }
            }
        }
    }
}
