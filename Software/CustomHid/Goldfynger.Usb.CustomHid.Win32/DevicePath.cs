﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;

namespace Goldfynger.Usb.CustomHid.Win32
{
    /// <summary>
    /// Container for USB HID path.
    /// </summary>
    public sealed record DevicePath : IEquatable<DevicePath>
    {
        private const string __vidPrefix = "vid_";

        private const string __pidPrefix = "pid_";


        private DevicePath(string devicePath)
        {
            Path = devicePath;

            Vid = ushort.Parse(devicePath.Substring(devicePath.IndexOf(__vidPrefix) + __vidPrefix.Length, 4), NumberStyles.HexNumber);

            Pid = ushort.Parse(devicePath.Substring(devicePath.IndexOf(__pidPrefix) + __pidPrefix.Length, 4), NumberStyles.HexNumber);
        }


        /// <summary>
        /// Win32 device path.
        /// </summary>
        public string Path { get; }

        /// <summary>
        /// Vendor ID.
        /// </summary>
        public ushort Vid { get; }

        /// <summary>
        /// Product ID.
        /// </summary>
        public ushort Pid { get; }


        /// <summary>
        /// Returns path of device.
        /// </summary>
        /// <returns>Win32 device path.</returns>
        public override string ToString() => Path;

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns><see langword="true"/> if the current object is equal to the other parameter; otherwise, <see langword="false"/>.</returns>
        public bool Equals(DevicePath other) => other != null && other.Path == Path;

        /// <summary>
        /// Returns the hash code for this object.
        /// </summary>
        /// <returns>A 32-bit signed integer hash code.</returns>
        public override int GetHashCode() => Path.GetHashCode();

        /// <summary>
        /// Returns information about USB HID paths.
        /// </summary>
        /// <returns>Read only collection of all HID paths ordered by path string representation.</returns>
        public static ReadOnlyCollection<DevicePath> FindDevicePaths()
        {
            var paths = new List<DevicePath>();

            // Gets the GUID from Windows that it uses to represent the HID USB interface.
            Win32Native.HidD_GetHidGuid(out Guid hidGuid);

            // Gets a list of all HID devices currently connected to the computer (InfoSet).
            using var devInfoSet = Win32Native.SetupDiGetClassDevs(
                ref hidGuid,
                null,
                IntPtr.Zero,
                Win32Native.DIGCF.DIGCF_PRESENT | Win32Native.DIGCF.DIGCF_DEVICEINTERFACE);

            if (devInfoSet.IsInvalid)
            {
                var win32Exception = new Win32Exception(Marshal.GetLastWin32Error());
                throw new ApplicationException($"Failed to get device info set: ({win32Exception.NativeErrorCode:X8}) {win32Exception.Message}", win32Exception);
            }

            var devInterfaceData = new Win32Native.SP_DEVICE_INTERFACE_DATA();
            devInterfaceData.cbSize = (uint)Marshal.SizeOf(devInterfaceData);

            // Iterates through the InfoSet memory block assigned within Windows in the call to SetupDiGetClassDevs to get device details for each device connected.
            uint idx = 0;
            var enumerationResult = false;
            do
            {
                enumerationResult = Win32Native.SetupDiEnumDeviceInterfaces(
                    devInfoSet,
                    IntPtr.Zero,
                    ref hidGuid,
                    idx,
                    ref devInterfaceData);

                if (enumerationResult)
                {
                    uint requredSize = 0;

                    // Calls function first time - gets the size of buffer.
                    if (!Win32Native.SetupDiGetDeviceInterfaceDetail(
                        devInfoSet,
                        ref devInterfaceData,
                        IntPtr.Zero,
                        0,
                        ref requredSize,
                        IntPtr.Zero))
                    {
                        var win32Exception = new Win32Exception(Marshal.GetLastWin32Error());
                        if (win32Exception.NativeErrorCode != (int)Win32Native.ERROR.ERROR_INSUFFICIENT_BUFFER)
                        {
                            throw new ApplicationException($"Failed to get required size of device path buffer: ({win32Exception.NativeErrorCode:X8}) {win32Exception.Message}", win32Exception);
                        }
                    }

                    if (requredSize > Win32Native.DevicePathMaxSize)
                    {
                        throw new ApplicationException("Required buffer size for device path is larger than predefined buffer size.");
                    }


                    var devInterfaceDetailData = new Win32Native.SP_DEVICE_INTERFACE_DETAIL_DATA { cbSize = (uint)(IntPtr.Size == 8 ? 8 : 5) };

                    // Calls function second time - gets the device interface details.
                    if (!Win32Native.SetupDiGetDeviceInterfaceDetail(
                        devInfoSet,
                        ref devInterfaceData,
                        ref devInterfaceDetailData,
                        requredSize,
                        ref requredSize,
                        IntPtr.Zero))
                    {
                        var win32Exception = new Win32Exception(Marshal.GetLastWin32Error());
                        throw new ApplicationException($"Failed to get device path: ({win32Exception.NativeErrorCode:X8}) {win32Exception.Message}", win32Exception);
                    }

                    paths.Add(new DevicePath(devInterfaceDetailData.DevicePath));


                    idx++;
                }
                else
                {
                    var win32Exception = new Win32Exception(Marshal.GetLastWin32Error());
                    if (win32Exception.NativeErrorCode != (int)Win32Native.ERROR.ERROR_NO_MORE_ITEMS)
                    {
                        throw new ApplicationException($"Failed to enumerate devices: ({win32Exception.NativeErrorCode:X8}) {win32Exception.Message}", win32Exception);
                    }
                }
            }
            while (enumerationResult);

            return new ReadOnlyCollection<DevicePath>(paths.OrderBy(p => p.Path).ToList());
        }

        /// <summary>
        /// Looking for USB HID paths with specified vendor ID.
        /// </summary>
        /// <param name="vid">Vendor ID.</param>
        /// <returns>Read only collection of HID paths with specified vendor ID.</returns>
        public static ReadOnlyCollection<DevicePath> FindDevicePaths(ushort vid) => new(FindDevicePaths().Where(dp => dp.Vid == vid).ToList());

        /// <summary>
        /// Looking for USB HID paths with specified vendor ID and product ID.
        /// </summary>
        /// <param name="vid">Vendor ID.</param>
        /// <param name="pid">Product ID.</param>
        /// <returns>Read only collection of HID paths with specified vendor ID and product ID.</returns>
        public static ReadOnlyCollection<DevicePath> FindDevicePaths(ushort vid, ushort pid) => new(FindDevicePaths().Where(dp => dp.Vid == vid && dp.Pid == pid).ToList());
    }
}
