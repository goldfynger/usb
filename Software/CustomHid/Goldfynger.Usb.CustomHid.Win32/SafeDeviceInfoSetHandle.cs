﻿using System;
using System.Security;

using Microsoft.Win32.SafeHandles;

namespace Goldfynger.Usb.CustomHid.Win32
{
    /// <summary>
    /// Represents a wrapper class for a InfoSet handle.
    /// </summary>
    [SecurityCritical]
    internal sealed class SafeDeviceInfoSetHandle : SafeHandleZeroOrMinusOneIsInvalid
    {
        /// <summary>
        /// Creates a new SafeDeviceInfoSetHandle. This constructor should only be called by P/Invoke.
        /// </summary>
        private SafeDeviceInfoSetHandle() : base(true)
        {
        }

        /// <summary>
        /// Creates a new SafeDeviceInfoSetHandle with the specified pre-existing handle.
        /// </summary>
        /// <param name="preexistingHandle">The pre-existing handle to use.</param>
        /// <param name="ownHandle"><c>true</c> to close the token when this object is disposed or finalized, <c>false</c> otherwise.</param>
        public SafeDeviceInfoSetHandle(IntPtr preexistingHandle, bool ownHandle) : base(ownHandle) => SetHandle(preexistingHandle);

        /// <summary>
        /// Executes the code required to free the handle.
        /// </summary>
        /// <returns><c>true</c> if success, <c>false</c> otherwise.</returns>
        [SecurityCritical]
        protected override bool ReleaseHandle() => Win32Native.SetupDiDestroyDeviceInfoList(handle);
    }
}
