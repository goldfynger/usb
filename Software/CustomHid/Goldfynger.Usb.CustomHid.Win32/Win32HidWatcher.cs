﻿using System;
using System.Runtime.InteropServices;

namespace Goldfynger.Usb.CustomHid.Win32
{
    /// <summary>
    /// Represents HID devices watcher.
    /// </summary>
    public class Win32HidWatcher : IDisposable
    {
        /// <summary>
        /// Internal device notification handle.
        /// </summary>
        private readonly SafeDeviceNotificationHandle _notifyHandle;


        /// <summary>
        /// This event will be triggered when a device is arrived.
        /// </summary>
        public event EventHandler DeviceArrived;

        /// <summary>
        /// This event will be triggered when a device is removed.
        /// </summary>
        public event EventHandler DeviceRemoved;


        /// <summary>
        /// Creates new instance of HID watcher.
        /// </summary>
        /// <param name="wndHandle">Windows handle.</param>
        /// <param name="addWndProcHook">Callback to return a function that needs to be called from WndProc.</param>
        public Win32HidWatcher(IntPtr wndHandle, Action<Action<int, IntPtr>> addWndProcHook)
        {
            addWndProcHook(new Action<int, IntPtr>((msg, wParam) =>
            {
                if (msg == (int)Win32Native.WM.WM_DEVICECHANGE)
                {
                    switch (wParam.ToInt64())
                    {
                        case (long)Win32Native.DBT.DBT_DEVICEARRIVAL:
                            OnDeviceArrived();
                            break;

                        case (long)Win32Native.DBT.DBT_DEVICEREMOVECOMPLETE:
                            OnDeviceRemoved();
                            break;
                    }
                }
            }));


            // Gets the GUID from Windows that it uses to represent the HID USB interface.
            Win32Native.HidD_GetHidGuid(out Guid hidGuid);

            var devInterface = new Win32Native.DEV_BROADCAST_DEVICEINTERFACE
            {
                dbcc_classguid = hidGuid,
                dbcc_devicetype = Win32Native.DBT_DEVTYP.DBT_DEVTYP_DEVICEINTERFACE,
                dbcc_reserved = 0
            };
            devInterface.dbcc_size = (uint)Marshal.SizeOf(devInterface);

            // Registry for notifications.
            _notifyHandle = Win32Native.RegisterDeviceNotification(wndHandle, ref devInterface, Win32Native.DEVICE_NOTIFY.DEVICE_NOTIFY_WINDOW_HANDLE);
        }


        /// <summary>
        /// Function invoked when USB device arrived.
        /// </summary>
        protected virtual void OnDeviceArrived() => DeviceArrived?.Invoke(this, EventArgs.Empty);

        /// <summary>
        /// Function invoked when USB device removed.
        /// </summary>
        protected virtual void OnDeviceRemoved() => DeviceRemoved?.Invoke(this, EventArgs.Empty);


        /// <summary>
        /// Flag: Has Dispose already been called?
        /// </summary>
        private bool _disposed = false;

        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    _notifyHandle?.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~Win32Hid()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        /// <summary>
        /// Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
