﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;

namespace Goldfynger.Usb.CustomHid.Win32
{
    /// <summary>
    /// Simple generic implementation of abstract <see cref="Win32Hid"/> class.
    /// </summary>
    /// <typeparam name="TInputReport">Used type of input report.</typeparam>
    /// <typeparam name="TOutputReport">Used type of output report.</typeparam>
    public class GenericUsbHid<TInputReport, TOutputReport> : Win32Hid where TInputReport : BaseInputReport where TOutputReport : BaseOutputReport
    {
        private readonly Func<byte[], TInputReport> _reportActivator;

        private EventHandler<ReadCompletedEventArgs<TInputReport>> _readCompletedEventHandler;

        private EventHandler<ExchangeExceptionEventArgs> _readErrorEventHandler;

        private Exception _readException;


        /// <summary>
        /// Fires all read input reports asynchronously.
        /// </summary>
        public event EventHandler<ReadCompletedEventArgs<TInputReport>> ReadCompleted
        {
            add
            {
                if (ReadMode != GenericUsbHidReadModes.Continuous)
                {
                    throw new InvalidOperationException("Can be called only in continuous read mode.");
                }

                _readCompletedEventHandler += value;
            }
            remove
            {
                if (ReadMode != GenericUsbHidReadModes.Continuous)
                {
                    throw new InvalidOperationException("Can be called only in continuous read mode.");
                }

                _readCompletedEventHandler -= value;
            }
        }

        /// <summary>
        /// Fires all read exceptions asynchronously.
        /// </summary>
        public event EventHandler<ExchangeExceptionEventArgs> ReadExceptionOccurred
        {
            add
            {
                if (ReadMode != GenericUsbHidReadModes.Continuous)
                {
                    throw new InvalidOperationException("Can be called only in continuous read mode.");
                }

                _readErrorEventHandler += value;
            }
            remove
            {
                if (ReadMode != GenericUsbHidReadModes.Continuous)
                {
                    throw new InvalidOperationException("Can be called only in continuous read mode.");
                }

                _readErrorEventHandler -= value;
            }
        }


        /// <summary>
        /// Creates new <see cref="GenericUsbHid{TInputReport, TOutputReport}"/> using <see cref="DevicePath"/>, input report activator and <see cref="GenericUsbHidReadModes"/> read mode.
        /// </summary>
        /// <param name="devPath">Win32 device path.</param>
        /// <param name="reportActivator">Input report instance activator.</param>
        /// <param name="readMode">Read mode.</param>
        public GenericUsbHid(DevicePath devPath, Func<byte[], TInputReport> reportActivator, GenericUsbHidReadModes readMode) : base(devPath)
        {
            _reportActivator = reportActivator;
            ReadMode = readMode;

            if (readMode == GenericUsbHidReadModes.Continuous)
            {
                Task.Run(async () =>
                {
                    while (true)
                    {
                        _readCompletedEventHandler?.Invoke(this, new ReadCompletedEventArgs<TInputReport>(await ReadAsyncProtected(reportActivator).ConfigureAwait(false)));
                    }
                }).ContinueWith(task =>
                {
                    try
                    {
                        task.Wait();
                    }
                    catch (Exception ex)
                    {
                        _readException = ex;
                        _readErrorEventHandler?.Invoke(this, new ExchangeExceptionEventArgs(ex));
                    }
                },
                TaskContinuationOptions.ExecuteSynchronously | TaskContinuationOptions.OnlyOnFaulted);
            }
        }


        /// <summary>
        /// Current read mode.
        /// </summary>
        public GenericUsbHidReadModes ReadMode { get; }

        /// <summary>
        /// Read exception.
        /// </summary>
        public Exception ReadException
        {
            get
            {
                if (ReadMode != GenericUsbHidReadModes.Continuous)
                {
                    throw new InvalidOperationException("Can be called only in continuous read mode.");
                }

                return _readException;
            }
        }


        /// <summary>
        /// Reads single input report asynchronously.
        /// </summary>
        /// <param name="cancellationToken">Operation cancellation token.</param>
        /// <returns>Task for awaiting.</returns>
        public async Task<TInputReport> ReadAsync(CancellationToken cancellationToken = default)
        {
            if (ReadMode == GenericUsbHidReadModes.Continuous)
            {
                throw new InvalidOperationException("Can be called only in single read mode.");
            }

            return await ReadAsyncProtected(_reportActivator, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Reads single input report asynchronously.
        /// </summary>
        /// <param name="timeout">Operation timeout.</param>
        /// <param name="cancellationToken">Operation cancellation token.</param>
        /// <returns>Task for awaiting.</returns>
        public async Task<TInputReport> ReadAsync(TimeSpan timeout, CancellationToken cancellationToken = default)
        {
            if (ReadMode == GenericUsbHidReadModes.Continuous)
            {
                throw new InvalidOperationException("Can be called only in single read mode.");
            }

            return await ReadAsyncProtected(_reportActivator, timeout, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Writes ouput report asynchronously.
        /// </summary>
        /// <param name="report">Report to write to the device.</param>
        /// <param name="cancellationToken">Operation cancellation token.</param>
        /// <returns>Task for awaiting.</returns>
        public async Task WriteAsync(TOutputReport report, CancellationToken cancellationToken = default)
        {
            await WriteAsyncProtected(report, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Writes ouput report asynchronously.
        /// </summary>
        /// <param name="report">Report to write to the device.</param>
        /// <param name="timeout">Operation timeout.</param>
        /// <param name="cancellationToken">Operation cancellation token.</param>
        /// <returns>Task for awaiting.</returns>
        public async Task WriteAsync(TOutputReport report, TimeSpan timeout, CancellationToken cancellationToken = default)
        {
            await WriteAsyncProtected(report, timeout, cancellationToken).ConfigureAwait(false);
        }
    }

    /// <summary>
    /// Container for specified input report.
    /// </summary>
    public sealed class ReadCompletedEventArgs<TInputReport> : EventArgs where TInputReport : BaseInputReport
    {
        /// <summary>
        /// Creates new <see cref="ReadCompletedEventArgs{TInputReport}"/>.
        /// </summary>
        /// <param name="inputReport">Input report.</param>
        public ReadCompletedEventArgs(TInputReport inputReport)
        {
            InputReport = inputReport;
        }

        /// <summary>
        /// Input report.
        /// </summary>
        public TInputReport InputReport { get; }
    }

    /// <summary>
    /// Container with exchange exception.
    /// </summary>
    public sealed class ExchangeExceptionEventArgs : EventArgs
    {
        /// <summary>
        /// Creates new <see cref="ExchangeExceptionEventArgs"/>.
        /// </summary>
        /// <param name="exception">Exchange exception.</param>
        public ExchangeExceptionEventArgs(Exception exception)
        {
            Exception = exception;
        }

        /// <summary>
        /// Exchange exception.
        /// </summary>
        public Exception Exception { get; }
    }

    /// <summary>
    /// Input report reading modes.
    /// </summary>
    public enum GenericUsbHidReadModes
    {
        /// <summary>Single mode - one report at a time.</summary>
        Single,
        /// <summary>Continuous mode - continuous reading.</summary>
        Continuous
    }

    /// <summary>
    /// Simple implementation of <see cref="GenericUsbHid{TInputReport, TOutputReport}"/> class.
    /// </summary>
    public sealed class GenericUsbHid : GenericUsbHid<GenericUsbInputReport, GenericUsbOutputReport>
    {
        /// <summary>
        /// Creates new <see cref="GenericUsbHid"/> using <see cref="DevicePath"/> and <see cref="GenericUsbHidReadModes"/> read mode.
        /// </summary>
        /// <param name="devPath">Win32 device path.</param>
        /// <param name="readMode">Read mode.</param>
        public GenericUsbHid(DevicePath devPath, GenericUsbHidReadModes readMode) : base(devPath, array => new GenericUsbInputReport(array), readMode)
        {
        }
    }

    /// <summary>
    /// Simple implementation of abstract <see cref="BaseInputReport"/> class.
    /// </summary>
    public sealed class GenericUsbInputReport : BaseInputReport
    {
        internal GenericUsbInputReport(byte[] rawData) : base(rawData)
        {
        }
    }

    /// <summary>
    /// Simple implementation of abstract <see cref="BaseOutputReport"/> class.
    /// </summary>
    public sealed class GenericUsbOutputReport : BaseOutputReport
    {
        /// <summary>
        /// Creates base output report using data and report ID.
        /// </summary>
        /// <param name="data">Data of report.</param>
        /// <param name="reportId">Report ID</param>
        public GenericUsbOutputReport(ReadOnlyCollection<byte> data, byte reportId = 0) : base(data, reportId)
        {
        }
    }
}
