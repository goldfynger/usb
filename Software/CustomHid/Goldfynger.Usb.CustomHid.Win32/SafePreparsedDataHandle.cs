﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Goldfynger.Usb.CustomHid.Win32
{
    /// <summary>
    /// Represents a wrapper class for a PreparsedData handle.
    /// </summary>
    [SecurityCritical]
    internal sealed class SafePreparsedDataHandle : SafeHandle
    {
        /// <summary>
        /// Creates a new SafePreparsedDataHandle. This constructor should only be called by P/Invoke.
        /// </summary>
        private SafePreparsedDataHandle() : base(IntPtr.Zero, true)
        {
        }

        /// <summary>
        /// Creates a new SafePreparsedDataHandle with the specified pre-existing handle.
        /// </summary>
        /// <param name="preexistingHandle">The pre-existing handle to use.</param>
        /// <param name="ownHandle"><c>true</c> to close the token when this object is disposed or finalized, <c>false</c> otherwise.</param>
        public SafePreparsedDataHandle(IntPtr preexistingHandle, bool ownHandle) : base(IntPtr.Zero, ownHandle) => SetHandle(preexistingHandle);

        /// <summary>
        /// <c>true</c> if handle is invalid, <c>false</c> otherwise. 
        /// </summary>
        public override bool IsInvalid
        {
            [SecurityCritical]
            get => handle == IntPtr.Zero;
        }

        /// <summary>
        /// Executes the code required to free the handle.
        /// </summary>
        /// <returns><c>true</c> if success, <c>false</c> otherwise.</returns>
        [SecurityCritical]
        protected override bool ReleaseHandle() => Win32Native.HidD_FreePreparsedData(ref handle);
    }
}
