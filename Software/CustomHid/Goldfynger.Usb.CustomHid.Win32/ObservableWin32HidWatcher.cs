﻿using System;
using System.Collections.ObjectModel;

namespace Goldfynger.Usb.CustomHid.Win32
{
    /// <summary>
    /// Represents observable HID devices watcher.
    /// </summary>
    public class ObservableWin32HidWatcher : Win32HidWatcher
    {
        /// <summary>
        /// Collection of available HIDs.
        /// </summary>
        private readonly ObservableCollection<DevicePath> _devicePath;


        /// <summary>
        /// Creates new instance observable of HID watcher.
        /// </summary>
        /// <param name="wndHandle">Windows handle.</param>
        /// <param name="addWndProcHook">Callback to return a function that needs to be called from WndProc.</param>
        public ObservableWin32HidWatcher(IntPtr wndHandle, Action<Action<int, IntPtr>> addWndProcHook) : base(wndHandle, addWndProcHook)
        {
            if (addWndProcHook == null)
            {
                throw new ArgumentNullException(nameof(addWndProcHook));
            }


            _devicePath = new ObservableCollection<DevicePath>(DevicePath.FindDevicePaths()); /* Search for devices first time. */

            DevicePaths = new ReadOnlyObservableCollection<DevicePath>(_devicePath);
        }


        /// <summary>
        /// Read only collection of available devices.
        /// </summary>
        public ReadOnlyObservableCollection<DevicePath> DevicePaths { get; }


        /// <summary>
        /// Function invoked when USB device arrived.
        /// </summary>
        protected override void OnDeviceArrived()
        {
            base.OnDeviceArrived();

            UpdateCollection();
        }

        /// <summary>
        /// Function invoked when USB device removed.
        /// </summary>
        protected override void OnDeviceRemoved()
        {
            base.OnDeviceRemoved();

            UpdateCollection();
        }

        /// <summary>
        /// Update device collection.
        /// </summary>
        private void UpdateCollection()
        {
            var actualDevices = DevicePath.FindDevicePaths();

            var removeIdx = 0;

            while (true) /* Remove all unavailable paths. */
            {
                if (removeIdx >= _devicePath.Count) /* End of collection. */
                {
                    break;
                }

                if (actualDevices.IndexOf(_devicePath[removeIdx]) < 0) /* Not found in actual collection - device removed. */
                {
                    _devicePath.RemoveAt(removeIdx);
                }
                else /* Else increment index. */
                {
                    removeIdx++;
                }
            }

            /* Here we have old collection and new actual collection that can have more items than old. */
            /* We can save order returned by FindDevicePaths and insert new devices in right places. */

            for (var addIdx = 0; addIdx < actualDevices.Count; addIdx++) /* Add. */
            {
                if (_devicePath.IndexOf(actualDevices[addIdx]) < 0) /* Not found in exisiting collection - new device. */
                {
                    _devicePath.Insert(addIdx, actualDevices[addIdx]);
                }
            }
        }
    }
}
