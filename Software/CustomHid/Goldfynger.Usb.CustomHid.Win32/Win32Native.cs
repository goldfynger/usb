﻿using System;
using System.Runtime.InteropServices;
using System.Text;

using Microsoft.Win32.SafeHandles;

namespace Goldfynger.Usb.CustomHid.Win32
{
    /// <summary>
    /// Internal class with WinAPI infrastructure.
    /// </summary>
    internal static class Win32Native
    {
        /// <summary>
        /// Max size of device path in <see cref="SP_DEVICE_INTERFACE_DETAIL_DATA"/>.
        /// </summary>
        internal const int DevicePathMaxSize = 256;


        /// <summary>
        /// Defines a device interface in a device information set.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        internal struct SP_DEVICE_INTERFACE_DATA
        {
            public uint cbSize;
            public Guid InterfaceClassGuid;
            public uint Flags;
            public UIntPtr Reserved;
        }

        /// <summary>
        /// Contains the path for a device interface.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode, Pack = 1)]
        internal struct SP_DEVICE_INTERFACE_DETAIL_DATA
        {
            public uint cbSize;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DevicePathMaxSize)]
            public string DevicePath;
        }

        /// <summary>
        /// Contains information about a top-level collection's capability.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        internal struct HIDP_CAPS
        {
            public ushort Usage;
            public ushort UsagePage;
            public ushort InputReportByteLength;
            public ushort OutputReportByteLength;
            public ushort FeatureReportByteLength;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 17)]
            public ushort[] Reserved;
            public ushort NumberLinkCollectionNodes;
            public ushort NumberInputButtonCaps;
            public ushort NumberInputValueCaps;
            public ushort NumberInputDataIndices;
            public ushort NumberOutputButtonCaps;
            public ushort NumberOutputValueCaps;
            public ushort NumberOutputDataIndices;
            public ushort NumberFeatureButtonCaps;
            public ushort NumberFeatureValueCaps;
            public ushort NumberFeatureDataIndices;
        }

        /// <summary>
        /// Contains vendor information about a HIDClass device.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        internal struct HIDD_ATTRIBUTES
        {
            public uint Size;
            public ushort VendorID;
            public ushort ProductID;
            public ushort VersionNumber;
        }

        /// <summary>
        /// Contains information about a class of devices.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode, Pack = 1)]
        internal struct DEV_BROADCAST_DEVICEINTERFACE
        {
            public uint dbcc_size;
            [MarshalAs(UnmanagedType.U4)]
            public DBT_DEVTYP dbcc_devicetype;
            public uint dbcc_reserved;
            public Guid dbcc_classguid;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string dbcc_name;
        }


        /// <summary>
        /// System error codes returned by the GetLastError function.
        /// </summary>
        internal enum ERROR : uint
        {
            /// <summary>The data area passed to a system call is too small.</summary>
            ERROR_INSUFFICIENT_BUFFER = 122,
            /// <summary>No more data is available.</summary>
            ERROR_NO_MORE_ITEMS = 259,
        }

        /// <summary>
        /// HID errors defined in hidpi.h.
        /// </summary>
        internal enum HIDP_STATUS : uint
        {
            /// <summary>The routine successfully returned the collection capability information.</summary>
            HIDP_STATUS_SUCCESS                 = (0x00U << 28) | (0x11 << 16) | 0,
            /// <summary>The specified preparsed data is invalid.</summary>
            HIDP_STATUS_INVALID_PREPARSED_DATA  = (0x0CU << 28) | (0x11 << 16) | 1,
        }

        /// <summary>
        /// Used in <see cref="SetupDiGetClassDevs"/> function. Specifies control options that filter the device information elements that are added to the device information set.
        /// </summary>
        [Flags]
        internal enum DIGCF : uint
        {
            /// <summary><see cref="SetupDiGetClassDevs"/>: Return only devices that are currently present in a system.</summary>
            DIGCF_PRESENT = 0x00000002,
            /// <summary><see cref="SetupDiGetClassDevs"/>: Return devices that support device interfaces for the specified device interface classes.</summary>
            DIGCF_DEVICEINTERFACE = 0x00000010,
        }

        /// <summary>
        /// Used in <see cref="CreateFile"/> function. Specifies type of requested access to the device.
        /// </summary>
        [Flags]
        internal enum CfDesiredAccess : uint
        {
            /// <summary><see cref="CreateFile"/>: Open device for read.</summary>
            GENERIC_READ = 0x80000000,
            /// <summary><see cref="CreateFile"/>: Open device for write.</summary>
            GENERIC_WRITE = 0x40000000,
        }

        /// <summary>
        /// Used in <see cref="CreateFile"/> function. Specifies type of requested sharing mode to the device.
        /// </summary>
        [Flags]
        internal enum CfShareMode : uint
        {
            /// <summary><see cref="CreateFile"/>: Enables subsequent open operations on a device to request read access.</summary>
            FILE_SHARE_READ = 0x00000001,
            /// <summary><see cref="CreateFile"/>: Enables subsequent open operations on a device to request write access.</summary>
            FILE_SHARE_WRITE = 0x00000002,
        }

        /// <summary>
        /// Used in <see cref="CreateFile"/> function. Specifies an action to take on a device that exists or does not exist.
        /// </summary>
        [Flags]
        internal enum CfCreationDisposition : uint
        {
            /// <summary><see cref="CreateFile"/>: Opens a device, only if it exists.</summary>
            OPEN_EXISTING = 0x00000003,
        }

        /// <summary>
        /// Used in <see cref="CreateFile"/> function. Specifies the device attributes and flags.
        /// </summary>
        [Flags]
        internal enum CfFlagsAndAttributes : uint
        {
            /// <summary><see cref="CreateFile"/>: The device is being opened or created for asynchronous I/O.</summary>
            FILE_FLAG_OVERLAPPED = 0x40000000,
        }

        /// <summary>
        /// Used to select device type.
        /// </summary>
        internal enum DBT_DEVTYP : uint
        {
            /// <summary>Class of devices.</summary>
            DBT_DEVTYP_DEVICEINTERFACE = 0x00000005,
        }

        /// <summary>
        /// Used in <see cref="RegisterDeviceNotification"/> function. Specifies function paramters.
        /// </summary>
        [Flags]
        internal enum DEVICE_NOTIFY : uint
        {
            /// <summary>RegisterDeviceNotification : The hRecipient parameter is a window handle.</summary>
            DEVICE_NOTIFY_WINDOW_HANDLE = 0x00000000,
            /// <summary>RegisterDeviceNotification : The hRecipient parameter is a service status handle.</summary>
            DEVICE_NOTIFY_SERVICE_HANDLE = 0x00000001,
        }

        /// <summary>
        /// Windows messages.
        /// </summary>
        internal enum WM : uint
        {
            /// <summary>Device management message.</summary>
            WM_DEVICECHANGE = 0x00000219,
        }

        /// <summary>
        /// Device management events.
        /// </summary>
        internal enum DBT : ulong
        {
            /// <summary>A device or piece of media has been inserted and is now available.</summary>
            DBT_DEVICEARRIVAL = 0x0000000000008000,
            /// <summary>A device or piece of media has been removed.</summary>
            DBT_DEVICEREMOVECOMPLETE = 0x0000000000008004,
        }


        /// <summary>
        /// Allocates an InfoSet memory block within Windows that contains details of devices.
        /// </summary>
        /// <param name="ClassGuid">Class guid (e.g. HID guid).</param>
        /// <param name="Enumerator">Not used.</param>
        /// <param name="hwndParent">Not used.</param>
        /// <param name="Flags">Type of device details required (DIGCF_ constants).</param>
        /// <returns>A reference to the InfoSet.</returns>
        [DllImport("setupapi.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern SafeDeviceInfoSetHandle SetupDiGetClassDevs(
            ref Guid ClassGuid,
            [MarshalAs(UnmanagedType.LPWStr)] string Enumerator,
            IntPtr hwndParent,
            [MarshalAs(UnmanagedType.U4)] DIGCF Flags);

        /// <summary>
        /// Frees InfoSet allocated in call to above.
        /// </summary>
        /// <param name="DeviceInfoSet">Reference to InfoSet.</param>
        /// <returns>True if successful.</returns>
        [DllImport("setupapi.dll", SetLastError = true)]
        internal static extern bool SetupDiDestroyDeviceInfoList(IntPtr DeviceInfoSet);

        /// <summary>
        /// Gets the <see cref="SP_DEVICE_INTERFACE_DATA"/> for a device from an InfoSet.
        /// </summary>
        /// <param name="DeviceInfoSet">InfoSet to access.</param>
        /// <param name="DeviceInfoData">Not used.</param>
        /// <param name="InterfaceClassGuid">Device class guid.</param>
        /// <param name="MemberIndex">Index into InfoSet for device.</param>
        /// <param name="DeviceInterfaceData">DeviceInterfaceData to fill with data.</param>
        /// <returns>True if successful, false if not (e.g. when index is passed end of InfoSet).</returns>
        [DllImport("setupapi.dll", SetLastError = true)]
        internal static extern bool SetupDiEnumDeviceInterfaces(
            SafeDeviceInfoSetHandle DeviceInfoSet,
            IntPtr DeviceInfoData,
            ref Guid InterfaceClassGuid,
            uint MemberIndex,
            ref SP_DEVICE_INTERFACE_DATA DeviceInterfaceData);

        /// <summary>
        /// Gets the interface detail from a <see cref="SP_DEVICE_INTERFACE_DATA"/>. This is pretty much the device path.
        /// You call this twice, once to get the size of the struct you need to send (DeviceInterfaceDetailDataSize = 0)
        /// and once again when you've allocated the required space.
        /// </summary>
        /// <param name="DeviceInfoSet">InfoSet to access.</param>
        /// <param name="DeviceInterfaceData">DeviceInterfaceData to use.</param>
        /// <param name="DeviceInterfaceDetailData">DeviceInterfaceDetailData to fill with data.</param>
        /// <param name="DeviceInterfaceDetailDataSize">The size of the above.</param>
        /// <param name="RequiredSize">The required size of the above when above is set as zero.</param>
        /// <param name="DeviceInfoData">Not used.</param>
        /// <returns>True if successful.</returns>
        [DllImport("setupapi.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern bool SetupDiGetDeviceInterfaceDetail(
            SafeDeviceInfoSetHandle DeviceInfoSet,
            ref SP_DEVICE_INTERFACE_DATA DeviceInterfaceData,
            IntPtr DeviceInterfaceDetailData,
            uint DeviceInterfaceDetailDataSize,
            ref uint RequiredSize,
            IntPtr DeviceInfoData);

        /// <summary>
        /// Gets the interface detail from a <see cref="SP_DEVICE_INTERFACE_DATA"/>. This is pretty much the device path.
        /// You call this twice, once to get the size of the struct you need to send (nDeviceInterfaceDetailDataSize = 0)
        /// and once again when you've allocated the required space.
        /// </summary>
        /// <param name="DeviceInfoSet">InfoSet to access.</param>
        /// <param name="DeviceInterfaceData">DeviceInterfaceData to use.</param>
        /// <param name="DeviceInterfaceDetailData">DeviceInterfaceDetailData to fill with data.</param>
        /// <param name="DeviceInterfaceDetailDataSize">The size of the above.</param>
        /// <param name="RequiredSize">The required size of the above when above is set as zero.</param>
        /// <param name="DeviceInfoData">Not used.</param>
        /// <returns>True if successful.</returns>
        [DllImport("setupapi.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern bool SetupDiGetDeviceInterfaceDetail(
            SafeDeviceInfoSetHandle DeviceInfoSet,
            ref SP_DEVICE_INTERFACE_DATA DeviceInterfaceData,
            ref SP_DEVICE_INTERFACE_DETAIL_DATA DeviceInterfaceDetailData,
            uint DeviceInterfaceDetailDataSize,
            ref uint RequiredSize,
            IntPtr DeviceInfoData);

        /// <summary>
        /// Gets the GUID that Windows uses to represent HID class devices.
        /// </summary>
        /// <param name="HidGuid">An out parameter to take the Guid.</param>
        [DllImport("hid.dll")]
        internal static extern void HidD_GetHidGuid(out Guid HidGuid);

        /// <summary>
        /// Gets details from an open device. Reserves a block of memory which must be freed.
        /// </summary>
        /// <param name="HidDeviceObject">Device file handle.</param>
        /// <param name="PreparsedData">Reference to the preparsed data block.</param>
        /// <returns>True if successful.</returns>
        [DllImport("hid.dll", SetLastError = true)]
        internal static extern bool HidD_GetPreparsedData(
            SafeFileHandle HidDeviceObject,
            out SafePreparsedDataHandle PreparsedData);

        /// <summary>
        /// Frees the memory block reserved above.
        /// </summary>
        /// <param name="PreparsedData">Reference to preparsed data returned in call to GetPreparsedData.</param>
        /// <returns>True if successful.</returns>
        [DllImport("hid.dll", SetLastError = true)]
        internal static extern bool HidD_FreePreparsedData(ref IntPtr PreparsedData);

        /// <summary>
        /// Gets a device's capabilities from the preparsed data.
        /// </summary>
        /// <param name="PreparsedData">Preparsed data reference.</param>
        /// <param name="Capabilities">HidCaps structure to receive the capabilities.</param>
        /// <returns>True if successful.</returns>
        [DllImport("hid.dll")]
        [return: MarshalAs(UnmanagedType.U4)]
        internal static extern HIDP_STATUS HidP_GetCaps(
            SafePreparsedDataHandle PreparsedData,
            out HIDP_CAPS Capabilities);

        /// <summary>
        /// Gets the attributes of a specified device.
        /// </summary>
        /// <param name="HidDeviceObject">Specifies an open handle to a top-level collection.</param>
        /// <param name="Attributes">Pointer to a caller-allocated <see cref="HIDD_ATTRIBUTES"/> structure that returns the attributes of the collection specified by <paramref name="HidDeviceObject"/>.</param>
        /// <returns>True if successful.</returns>
        [DllImport("hid.dll")]
        internal static extern bool HidD_GetAttributes(
            SafeFileHandle HidDeviceObject,
            out HIDD_ATTRIBUTES Attributes);

        /// <summary>
        /// Gets a string that identifies the manufacturer.
        /// </summary>
        /// <param name="HidDeviceObject">Device file handle.</param>
        /// <param name="Buffer">Manufacturer string</param>
        /// <param name="BufferLength">Length of buffer.</param>
        /// <returns>True if successful.</returns>
        [DllImport("hid.dll", SetLastError = true)]
        internal static extern bool HidD_GetManufacturerString(
            SafeFileHandle HidDeviceObject,
            [MarshalAs(UnmanagedType.LPWStr)] StringBuilder Buffer,
            int BufferLength);

        /// <summary>
        /// Gets s string that identifies the manufacturer's product.
        /// </summary>
        /// <param name="HidDeviceObject">Device file handle.</param>
        /// <param name="Buffer">Product string.</param>
        /// <param name="BufferLength">Length of buffer.</param>
        /// <returns>True if successful.</returns>
        [DllImport("hid.dll", SetLastError = true)]
        internal static extern bool HidD_GetProductString(
            SafeFileHandle HidDeviceObject,
            [MarshalAs(UnmanagedType.LPWStr)] StringBuilder Buffer,
            int BufferLength);

        /// <summary>
        /// Gets s string that identifies the the serial number of the physical device.
        /// </summary>
        /// <param name="HidDeviceObject">Device file handle.</param>
        /// <param name="Buffer">Serial number string.</param>
        /// <param name="BufferLength">Length of buffer.</param>
        /// <returns>True if successful.</returns>
        [DllImport("hid.dll", SetLastError = true)]
        internal static extern bool HidD_GetSerialNumberString(
            SafeFileHandle HidDeviceObject,
            [MarshalAs(UnmanagedType.LPWStr)] StringBuilder Buffer,
            int BufferLength);

        /// <summary>
        /// Creates/opens a file, serial port, USB device... etc.
        /// </summary>
        /// <param name="lpFileName">Path to object to open.</param>
        /// <param name="dwDesiredAccess">Access mode. e.g. Read, write.</param>
        /// <param name="dwShareMode">Sharing mode.</param>
        /// <param name="lpSecurityAttributes">Security details (can be null).</param>
        /// <param name="dwCreationDisposition">Specifies if the file is created or opened.</param>
        /// <param name="dwFlagsAndAttributes">Any extra attributes? e.g. open overlapped.</param>
        /// <param name="hTemplateFile">Not used.</param>
        /// <returns>If the function succeeds, the return value is an open handle to the specified file, device, named pipe, or mail slot.</returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern SafeFileHandle CreateFile(
            [MarshalAs(UnmanagedType.LPWStr)] string lpFileName,
            [MarshalAs(UnmanagedType.U4)] CfDesiredAccess dwDesiredAccess,
            [MarshalAs(UnmanagedType.U4)] CfShareMode dwShareMode,
            IntPtr lpSecurityAttributes,
            [MarshalAs(UnmanagedType.U4)] CfCreationDisposition dwCreationDisposition,
            [MarshalAs(UnmanagedType.U4)] CfFlagsAndAttributes dwFlagsAndAttributes,
            IntPtr hTemplateFile);

        /// <summary>
        /// Registers a window or service for device insert/remove messages.
        /// </summary>
        /// <param name="hRecipient">Handle to the window or service that will receive the messages.</param>
        /// <param name="NotificationFilter">DeviceBroadcastInterrface structure.</param>
        /// <param name="Flags">Specified flags.</param>
        /// <returns>A handle used when unregistering.</returns>
        [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern SafeDeviceNotificationHandle RegisterDeviceNotification(
            IntPtr hRecipient,
            ref DEV_BROADCAST_DEVICEINTERFACE NotificationFilter,
            [MarshalAs(UnmanagedType.U4)] DEVICE_NOTIFY Flags);

        /// <summary>
        /// Closes device notification handle.
        /// </summary>
        /// <param name="Handle">Handle returned in call to RegisterDeviceNotification.</param>
        /// <returns>True if successful.</returns>
        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool UnregisterDeviceNotification(IntPtr Handle);
    }
}
