﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Goldfynger.Usb.CustomHid.Win32
{
    /// <summary>
    /// Base type of input/output reports.
    /// </summary>
    public abstract class BaseReport : IList<byte>, ICollection<byte>, IEnumerable<byte>, IList, ICollection, IEnumerable, IReadOnlyList<byte>, IReadOnlyCollection<byte>
    {
        /// <summary>
        /// Report data.
        /// </summary>
        private readonly List<byte> _data;


        /// <summary>
        /// Creates base report using data.
        /// </summary>
        /// <param name="data">Data of report.</param>
        /// <param name="reportId">Report identifier.</param>
        private protected BaseReport(ReadOnlyCollection<byte> data, byte reportId)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            _data = data.ToList();

            var rawData = new byte[_data.Count + 1];

            rawData[0] = reportId;

            _data.CopyTo(0, rawData, 1, _data.Count);

            RawData = new ReadOnlyCollection<byte>(rawData);
        }


        /// <summary>
        /// Report data.
        /// </summary>
        /// <param name="index">The zero-based index of the element to get.</param>
        /// <returns>The byte at the specified index.</returns>
        public byte this[int index] => _data[index];

        /// <summary>
        /// Raw report data including report ID.
        /// </summary>
        public ReadOnlyCollection<byte> RawData { get; }

        /// <summary>
        /// Raw data length.
        /// </summary>
        public int RawLength => RawData.Count;

        /// <summary>
        /// Report ID.
        /// </summary>
        public byte ReportId => RawData[0];

        /// <summary>
        /// Gets the number of elements contained in the <see cref="ICollection"/>.
        /// </summary>
        public int Count => _data.Count;


        /// <summary>
        /// Determines whether the <see cref="Collection{T}"/> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="Collection{T}"/>.</param>
        /// <returns><code>true</code> if item is found in the <see cref="Collection{T}"/>; otherwise, <code>false</code>.</returns>
        public bool Contains(byte item) => _data.Contains(item);

        /// <summary>
        /// Copies the elements of the <see cref="Collection{T}"/> to an <see cref="Array"/>, starting at a particular <see cref="Array"/> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="Array"/> that is the destination of the elements copied from <see cref="Collection{T}"/>. The <see cref="Array"/> must have zero-based indexing.</param>
        /// <param name="arrayIndex">The zero-based index in array at which copying begins.</param>
        public void CopyTo(byte[] array, int arrayIndex) => _data.CopyTo(array, arrayIndex);

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>A <see cref="IEnumerator{T}"/> that can be used to iterate through the collection.</returns>
        public IEnumerator<byte> GetEnumerator() => _data.GetEnumerator();

        /// <summary>
        /// Determines the index of a specific item in the <see cref="IList{T}"/>.
        /// </summary>
        /// <param name="item">The object to locate in the <see cref="IList{T}"/>.</param>
        /// <returns>The index of item if found in the list; otherwise, <code>-1</code>.</returns>
        public int IndexOf(byte item) => _data.IndexOf(item);


        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An <see cref="IEnumerator"/> object that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator() => (_data as IEnumerable).GetEnumerator();


        /// <summary>
        /// Gets a value indicating whether the <see cref="ICollection{T}"/> is read-only. Always <code>true</code>.
        /// </summary>
        bool ICollection<byte>.IsReadOnly => true;

        /// <summary>
        /// Gets a value indicating whether access to the <see cref="ICollection"/> is synchronized (thread safe). Always <code>false</code>.
        /// </summary>
        bool ICollection.IsSynchronized => false;

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="ICollection"/>.
        /// </summary>
        object ICollection.SyncRoot => (_data as ICollection).SyncRoot;

        /// <summary>
        /// Copies the elements of the <see cref="ICollection"/> to an <see cref="Array"/>, starting at a particular <see cref="Array"/> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="Array"/> that is the destination of the elements copied from <see cref="ICollection"/>. The <see cref="Array"/> must have zero-based indexing.</param>
        /// <param name="index">The zero-based index in array at which copying begins.</param>
        void ICollection.CopyTo(Array array, int index) => (_data as ICollection).CopyTo(array, index);


        /// <summary>
        /// Gets or sets the element at the specified index. The <code>set</code> operation always throws <see cref="NotSupportedException"/>.
        /// </summary>
        /// <param name="index">The zero-based index of the element to get or set.</param>
        /// <returns>The element at the specified index.</returns>
        object IList.this[int index]
        {
            get => _data[index];
            set => throw new NotSupportedException();
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="IList"/> has a fixed size. Always <code>true</code>.
        /// </summary>
        bool IList.IsFixedSize => true;

        /// <summary>
        /// Gets a value indicating whether the <see cref="IList"/> is read-only. Always <code>true</code>.
        /// </summary>
        bool IList.IsReadOnly => true;

        /// <summary>
        /// Adds an item to the <see cref="IList"/>. This implementation always throws <see cref="NotSupportedException"/>.
        /// </summary>
        /// <param name="value">The object to add to the <see cref="IList"/>.</param>
        /// <returns>The position into which the new element was inserted, or <code>-1</code> to indicate that the item was not inserted into the collection.</returns>
        int IList.Add(object value) => throw new NotSupportedException();

        /// <summary>
        /// Removes all items from the <see cref="IList"/>. This implementation always throws <see cref="NotSupportedException"/>.
        /// </summary>
        void IList.Clear() => throw new NotSupportedException();

        /// <summary>
        /// Determines whether the <see cref="IList"/> contains a specific value.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="IList"/>.</param>
        /// <returns><code>true</code> if the <see cref="object"/> is found in the <see cref="IList"/>; otherwise, <code>false</code>.</returns>
        bool IList.Contains(object value) => (_data as IList).Contains(value);

        /// <summary>
        /// Determines the index of a specific item in the <see cref="IList"/>.
        /// </summary>
        /// <param name="value">The object to locate in the <see cref="IList"/>.</param>
        /// <returns>The index of value if found in the list; otherwise, <code>-1</code>.</returns>
        int IList.IndexOf(object value) => (_data as IList).IndexOf(value);

        /// <summary>
        /// Inserts an item to the <see cref="IList"/> at the specified index. This implementation always throws <see cref="NotSupportedException"/>.
        /// </summary>
        /// <param name="index">The zero-based index at which value should be inserted.</param>
        /// <param name="value">The object to insert into the <see cref="IList"/>.</param>
        void IList.Insert(int index, object value) => throw new NotSupportedException();

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="IList"/>. This implementation always throws <see cref="NotSupportedException"/>.
        /// </summary>
        /// <param name="value">The object to remove from the <see cref="IList"/>.</param>
        void IList.Remove(object value) => throw new NotSupportedException();

        /// <summary>
        /// Removes the <see cref="IList"/> item at the specified index. This implementation always throws <see cref="NotSupportedException"/>.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        void IList.RemoveAt(int index) => throw new NotSupportedException();


        /// <summary>
        /// Adds an item to the <see cref="Collection{T}"/>. This implementation always throws <see cref="NotSupportedException"/>.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="Collection{T}"/>.</param>
        void ICollection<byte>.Add(byte item) => throw new NotSupportedException();

        /// <summary>
        /// Removes all items from the <see cref="ICollection{T}"/>. This implementation always throws <see cref="NotSupportedException"/>.
        /// </summary>
        void ICollection<byte>.Clear() => throw new NotSupportedException();

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="ICollection{T}"/>. This implementation always throws <see cref="NotSupportedException"/>.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="ICollection{T}"/>.</param>
        /// <returns><code>true</code> if item was successfully removed from the <see cref="ICollection{T}"/>; otherwise, <code>false</code>.
        /// This method also returns <code>false</code> if item is not found in the original <see cref="ICollection{T}"/>.</returns>
        bool ICollection<byte>.Remove(byte item) => throw new NotSupportedException();


        /// <summary>
        /// Gets or sets the element at the specified index. The <code>set</code> operation always throws <see cref="NotSupportedException"/>.
        /// </summary>
        /// <param name="index">The zero-based index of the element to get or set.</param>
        /// <returns>The element at the specified index.</returns>
        byte IList<byte>.this[int index]
        {
            get => _data[index];
            set => throw new NotSupportedException();
        }

        /// <summary>
        /// Inserts an item to the <see cref="IList{T}"/> at the specified index. This implementation always throws <see cref="NotSupportedException"/>.
        /// </summary>
        /// <param name="index">The zero-based index at which item should be inserted.</param>
        /// <param name="item">The object to insert into the <see cref="IList{T}"/>.</param>
        void IList<byte>.Insert(int index, byte item) => throw new NotSupportedException();

        /// <summary>
        /// Removes the <see cref="IList{T}"/> item at the specified index. This implementation always throws <see cref="NotSupportedException"/>.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        void IList<byte>.RemoveAt(int index) => throw new NotSupportedException();
    }


    /// <summary>
    /// Base type of input reports.
    /// </summary>
    public abstract class BaseInputReport : BaseReport
    {
        /// <summary>
        /// Creates base input report using raw data.
        /// </summary>
        /// <param name="rawData">Raw data.</param>
        protected BaseInputReport(byte[] rawData) : base(GetDataFromRaw(rawData), rawData[0])
        {
        }

        private static ReadOnlyCollection<byte> GetDataFromRaw(byte[] rawData)
        {
            var data = new byte[rawData.Length - 1];

            Array.Copy(rawData, 1, data, 0, data.Length);

            return new ReadOnlyCollection<byte>(data);
        }
    }


    /// <summary>
    /// Base type of output reports.
    /// </summary>
    public abstract class BaseOutputReport : BaseReport
    {
        /// <summary>
        /// Creates base output report using data and report ID.
        /// </summary>
        /// <param name="data">Data of report.</param>
        /// <param name="reportId">Report ID.</param>
        protected BaseOutputReport(ReadOnlyCollection<byte> data, byte reportId = 0) : base(data, reportId)
        {
        }
    }
}
