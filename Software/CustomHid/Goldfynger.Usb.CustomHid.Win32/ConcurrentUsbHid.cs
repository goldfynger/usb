﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Goldfynger.Utils.Collections;

namespace Goldfynger.Usb.CustomHid.Win32
{
    /// <summary>
    /// Simple generic implementation of abstract <see cref="Win32Hid"/> class with read and write concurrent access.
    /// </summary>
    /// <typeparam name="TInputReport">Used type of input report.</typeparam>
    /// <typeparam name="TOutputReport">Used type of output report.</typeparam>
    public class ConcurrentUsbHid<TInputReport, TOutputReport> : Win32Hid where TInputReport : BaseInputReport where TOutputReport : BaseOutputReport
    {
        private readonly AsyncConcurrentQueue<Tuple<TOutputReport, TimeSpan, CancellationToken>> _writeQueue;

        private readonly AsyncConcurrentQueue<TInputReport> _readQueue;

        private EventHandler<ReadCompletedEventArgs<TInputReport>> _readCompletedEventHandler;


        /// <summary>
        /// Fires all read input reports asynchronously.
        /// </summary>
        public event EventHandler<ReadCompletedEventArgs<TInputReport>> ReadCompleted
        {
            add
            {
                if (ReadMode != GenericUsbHidReadModes.Continuous)
                {
                    throw new InvalidOperationException("Can be called only in continuous read mode.");
                }

                _readCompletedEventHandler += value;
            }
            remove
            {
                if (ReadMode != GenericUsbHidReadModes.Continuous)
                {
                    throw new InvalidOperationException("Can be called only in continuous read mode.");
                }

                _readCompletedEventHandler -= value;
            }
        }

        /// <summary>
        /// Fires all read exceptions asynchronously.
        /// </summary>
        public event EventHandler<ExchangeExceptionEventArgs> ReadExceptionOccurred;

        /// <summary>
        /// Fires all write exceptions asynchronously.
        /// </summary>
        public event EventHandler<ExchangeExceptionEventArgs> WriteExceptionOccurred;


        /// <summary>
        /// Creates new <see cref="ConcurrentUsbHid{TInputReport, TOutputReport}"/> using <see cref="DevicePath"/>, input report activator and <see cref="GenericUsbHidReadModes"/> read mode.
        /// </summary>
        /// <param name="devPath">Win32 device path.</param>
        /// <param name="reportActivator">Input report instance activator.</param>
        /// <param name="readMode">Read mode.</param>
        public ConcurrentUsbHid(DevicePath devPath, Func<byte[], TInputReport> reportActivator, GenericUsbHidReadModes readMode) : base(devPath)
        {
            ReadMode = readMode;

            if (readMode == GenericUsbHidReadModes.Continuous)
            {
                Task.Run(async () =>
                {
                    while (true)
                    {
                        _readCompletedEventHandler?.Invoke(this, new ReadCompletedEventArgs<TInputReport>(await ReadAsyncProtected(reportActivator).ConfigureAwait(false)));
                    }
                }).ContinueWith(task =>
                {
                    try
                    {
                        task.Wait();
                    }
                    catch (Exception ex)
                    {
                        ReadException = ex;
                        ReadExceptionOccurred?.Invoke(this, new ExchangeExceptionEventArgs(ex));
                    }
                },
                TaskContinuationOptions.ExecuteSynchronously | TaskContinuationOptions.OnlyOnFaulted);
            }
            else
            {
                _readQueue = new AsyncConcurrentQueue<TInputReport>();

                Task.Run(async () =>
                {
                    while (true)
                    {
                        _readQueue.Enqueue(await ReadAsyncProtected(reportActivator).ConfigureAwait(false));
                    }
                }).ContinueWith(task =>
                {
                    try
                    {
                        task.Wait();
                    }
                    catch (Exception ex)
                    {
                        ReadException = ex;
                        ReadExceptionOccurred?.Invoke(this, new ExchangeExceptionEventArgs(ex));
                    }
                },
                TaskContinuationOptions.ExecuteSynchronously | TaskContinuationOptions.OnlyOnFaulted);
            }

            _writeQueue = new AsyncConcurrentQueue<Tuple<TOutputReport, TimeSpan, CancellationToken>>();

            Task.Run(async () =>
            {
                while (true)
                {
                    var tuple = await _writeQueue.DequeueAsync();

                    await WriteAsyncProtected(tuple.Item1, tuple.Item2, tuple.Item3);
                }
            }).ContinueWith(task =>
            {
                try
                {
                    task.Wait();
                }
                catch (Exception ex)
                {
                    WriteException = ex;
                    WriteExceptionOccurred?.Invoke(this, new ExchangeExceptionEventArgs(ex));
                }
            },
            TaskContinuationOptions.ExecuteSynchronously | TaskContinuationOptions.OnlyOnFaulted);
        }


        /// <summary>
        /// Current read mode.
        /// </summary>
        public GenericUsbHidReadModes ReadMode { get; }

        /// <summary>
        /// Read exception.
        /// </summary>
        public Exception ReadException { get; private set; }

        /// <summary>
        /// Read exception.
        /// </summary>
        public Exception WriteException { get; private set; }


        /// <summary>
        /// Reads single input report asynchronously.
        /// </summary>
        /// <param name="cancellationToken">Operation cancellation token.</param>
        /// <returns>Task for awaiting.</returns>
        public async Task<TInputReport> ReadAsync(CancellationToken cancellationToken = default)
        {
            if (ReadMode == GenericUsbHidReadModes.Continuous)
            {
                throw new InvalidOperationException("Can be called only in single read mode.");
            }

            return await _readQueue.DequeueAsync(cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Reads single input report asynchronously.
        /// </summary>
        /// <param name="timeout">Operation timeout.</param>
        /// <param name="cancellationToken">Operation cancellation token.</param>
        /// <returns>Task for awaiting.</returns>
        public async Task<TInputReport> ReadAsync(TimeSpan timeout, CancellationToken cancellationToken = default)
        {
            if (ReadMode == GenericUsbHidReadModes.Continuous)
            {
                throw new InvalidOperationException("Can be called only in single read mode.");
            }

            return await _readQueue.DequeueAsync(timeout, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Adds report to write queue.
        /// </summary>
        /// <param name="report">Report to write to the device.</param>
        /// <param name="cancellationToken">Operation cancellation token.</param>
        public void Enqueue(TOutputReport report, CancellationToken cancellationToken = default)
        {
            _writeQueue.Enqueue(new Tuple<TOutputReport, TimeSpan, CancellationToken>(report, Timeout.InfiniteTimeSpan, cancellationToken));
        }

        /// <summary>
        /// Adds report to write queue.
        /// </summary>
        /// <param name="report">Report to write to the device.</param>
        /// <param name="timeout">Operation timeout.</param>
        /// <param name="cancellationToken">Operation cancellation token.</param>
        public void Enqueue(TOutputReport report, TimeSpan timeout, CancellationToken cancellationToken = default)
        {
            _writeQueue.Enqueue(new Tuple<TOutputReport, TimeSpan, CancellationToken>(report, timeout, cancellationToken));
        }


        /// <summary>
        /// Flag: Has Dispose already been called?
        /// </summary>
        private bool _disposed = false;

        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    _readQueue?.Dispose();
                    _writeQueue?.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }

            base.Dispose(disposing);
        }
    }

    /// <summary>
    /// Simple implementation of abstract <see cref="ConcurrentUsbHid{TInputReport, TOutputReport}"/> class with read and write concurrent access.
    /// </summary>
    public sealed class ConcurrentUsbHid : ConcurrentUsbHid<GenericUsbInputReport, GenericUsbOutputReport>
    {
        /// <summary>
        /// Creates new <see cref="ConcurrentUsbHid"/> using <see cref="DevicePath"/> and <see cref="GenericUsbHidReadModes"/> read mode.
        /// </summary>
        /// <param name="devPath">Win32 device path.</param>
        /// <param name="readMode">Read mode.</param>
        public ConcurrentUsbHid(DevicePath devPath, GenericUsbHidReadModes readMode) : base(devPath, array => new GenericUsbInputReport(array), readMode)
        {
        }
    }
}
