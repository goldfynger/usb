﻿using System;

namespace Goldfynger.Usb.CustomHid.Win32
{
    /// <summary>
    /// Container for USB HID info.
    /// </summary>
    public sealed record HidInfo : IEquatable<HidInfo>
    {
        private readonly Win32Native.HIDP_CAPS _hidCaps;

        private readonly Win32Native.HIDD_ATTRIBUTES _hidAttributes;


        internal HidInfo(DevicePath devPath, Win32Native.HIDP_CAPS hidCaps, Win32Native.HIDD_ATTRIBUTES hidAttributes, string manufacturerDesc, string productDesc, string serialNumber)
        {
            DevicePath = devPath;

            _hidCaps = hidCaps;
            _hidAttributes = hidAttributes;

            ManufacturerDescription = manufacturerDesc;
            ProductDescription = productDesc;
            SerialNumber = serialNumber;
        }


        /// <summary>
        /// Win32 device path.
        /// </summary>    
        public DevicePath DevicePath { get; }

        /// <summary>
        /// Vendor ID.
        /// </summary>
        public ushort Vid => _hidAttributes.VendorID;

        /// <summary>
        /// Product ID.
        /// </summary>
        public ushort Pid => _hidAttributes.ProductID;

        /// <summary>
        /// Output report length.
        /// </summary>
        public int OutputReportLength => _hidCaps.OutputReportByteLength;

        /// <summary>
        /// Input report length.
        /// </summary>
        public int InputReportLength => _hidCaps.InputReportByteLength;

        /// <summary>
        /// Description of manufacturer.
        /// </summary>
        public string ManufacturerDescription { get; }

        /// <summary>
        /// Description of product.
        /// </summary>
        public string ProductDescription { get; }

        /// <summary>
        /// Device serial number.
        /// </summary>
        public string SerialNumber { get; }


        /// <summary>
        /// Returns a string that represents the HID info.
        /// </summary>
        /// <returns>HID info.</returns>
        public override string ToString()
        {
            if (ManufacturerDescription == null && ProductDescription == null)
            {
                return DevicePath.ToString();
            }
            else if (ManufacturerDescription != null && ProductDescription != null)
            {
                return $"{ManufacturerDescription} {ProductDescription}";
            }
            else
            {
                if (ManufacturerDescription != null)
                {
                    return ManufacturerDescription;
                }
                else
                {
                    return ProductDescription;
                }
            }
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns><see langword="true"/> if the current object is equal to the other parameter; otherwise, <see langword="false"/>.</returns>
        public bool Equals(HidInfo other) => other != null && other._hidCaps.Equals(_hidCaps) && other._hidAttributes.Equals(_hidCaps) &&
            other.DevicePath == DevicePath && other.ManufacturerDescription == ManufacturerDescription && other.ProductDescription == ProductDescription && other.SerialNumber == SerialNumber;

        /// <summary>
        /// Returns the hash code for this object.
        /// </summary>
        /// <returns>A 32-bit signed integer hash code.</returns>
        public override int GetHashCode() => HashCode.Combine(_hidCaps, _hidAttributes, DevicePath, ManufacturerDescription, ProductDescription, SerialNumber);
    }
}
