﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Goldfynger.Utils.Extensions;

using Microsoft.Win32.SafeHandles;

namespace Goldfynger.Usb.CustomHid.Win32
{
    /// <summary>
    /// Represents USB HID.
    /// </summary>
    public abstract class Win32Hid : IDisposable
    {
        /// <summary>
        /// Internal handle of opened USB device.
        /// </summary>
        private readonly SafeFileHandle _handle;

        /// <summary>
        /// Info about USB HID.
        /// </summary>
        private readonly HidInfo _info;

        /// <summary>
        /// File stream to read from or write to USB device.
        /// </summary>
        private readonly FileStream _fileStream;


        /// <summary>
        /// Creates new <see cref="Win32Hid"/> using <see cref="DevicePath"/>.
        /// </summary>
        /// <param name="devPath">Win32 device path.</param>
        protected Win32Hid(DevicePath devPath)
        {
            try
            {
                // Create handle.
                _handle = CreateDeviceHandle(devPath);


                // Gets info about device.
                _info = FindDeviceInfo(devPath, _handle);


                // Creates file stream to read from device.
                _fileStream = new FileStream(_handle, FileAccess.Read | FileAccess.Write, Math.Max(Info.InputReportLength, Info.OutputReportLength), true);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"{nameof(Win32Hid)}: .ctor failed with exception: {ex.Message}");


                // Tries to dispose all resources before re-throwing exception.
                Dispose();

                throw;
            }

            Debug.WriteLine($"{nameof(Win32Hid)}: .ctor end");
        }


        /// <summary>
        /// Info about USB HID.
        /// </summary>    
        public HidInfo Info
        {
            get
            {
                CheckDispose();

                return _info;
            }
        }

        /// <summary>
        /// Read input report asynchronously.
        /// </summary>
        /// <typeparam name="T">Specified type of input report.</typeparam>
        /// <param name="reportActivator">Specified report activator.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        /// <returns>Task for awaiting.</returns>
        protected Task<T> ReadAsyncProtected<T>(Func<byte[], T> reportActivator, CancellationToken cancellationToken = default) where T : BaseInputReport
        {
            return ReadAsyncProtected(reportActivator, Timeout.InfiniteTimeSpan, cancellationToken);
        }

        /// <summary>
        /// Read input report asynchronously.
        /// </summary>
        /// <typeparam name="T">Specified type of input report.</typeparam>
        /// <param name="reportActivator">Specified report activator.</param>
        /// /// <param name="timeout">Read operation timeout.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        /// <returns>Task for awaiting.</returns>
        protected async Task<T> ReadAsyncProtected<T>(Func<byte[], T> reportActivator, TimeSpan timeout, CancellationToken cancellationToken = default) where T : BaseInputReport
        {
            CheckDispose();

            using var readCancellationTokenSource = cancellationToken.CanBeCanceled ? CancellationTokenSource.CreateLinkedTokenSource(cancellationToken, default) : new CancellationTokenSource();

            var readTask = Task.Run(async () =>
            {
                var buffer = new byte[Info.InputReportLength];

                Debug.WriteLine($"{nameof(Win32Hid)}: {nameof(ReadAsyncProtected)} begin read.");

                await _fileStream.ReadAsync(buffer.AsMemory(0, buffer.Length), readCancellationTokenSource.Token).ConfigureAwait(false);

                Debug.WriteLine($"{nameof(Win32Hid)}: {nameof(ReadAsyncProtected)} readed data: {string.Join(" ", buffer.Select(x => x.ToString("X2")))}");

                Debug.WriteLine($"{nameof(Win32Hid)}: {nameof(ReadAsyncProtected)} end read.");

                return reportActivator(buffer);
            },
            readCancellationTokenSource.Token);

            if (timeout == Timeout.InfiniteTimeSpan)
            {
                return await readTask.ConfigureAwait(false);
            }
            else
            {
                Debug.WriteLine($"{nameof(Win32Hid)}: {nameof(ReadAsyncProtected)} timeout: {timeout.GetWholeMilliseconds()} milliseconds.");

                using var timeoutCancellationTokenSource = cancellationToken.CanBeCanceled ? CancellationTokenSource.CreateLinkedTokenSource(cancellationToken, default) : new CancellationTokenSource();

                var completedTask = await Task.WhenAny(readTask, Task.Delay(timeout, timeoutCancellationTokenSource.Token)).ConfigureAwait(false);

                if (completedTask == readTask)
                {
                    timeoutCancellationTokenSource.Cancel();

                    return await readTask.ConfigureAwait(false);
                }
                else
                {
                    readCancellationTokenSource.Cancel();

                    throw new TimeoutException();
                }
            }
        }

        /// <summary>
        /// Write ouput report asynchronously.
        /// </summary>
        /// <param name="report">Report to write to the device.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        /// <returns>Task for awaiting.</returns>
        protected Task WriteAsyncProtected(BaseOutputReport report, CancellationToken cancellationToken = default) => WriteAsyncProtected(report, Timeout.InfiniteTimeSpan, cancellationToken);

        /// <summary>
        /// Write ouput report asynchronously.
        /// </summary>
        /// <param name="report">Report to write to the device.</param>
        /// <param name="timeout">Write operation timeout.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        /// <returns>Task for awaiting.</returns>
        protected async Task WriteAsyncProtected(BaseOutputReport report, TimeSpan timeout, CancellationToken cancellationToken = default)
        {
            CheckDispose();

            using var writeCancellationTokenSource = cancellationToken.CanBeCanceled ? CancellationTokenSource.CreateLinkedTokenSource(cancellationToken, default) : new CancellationTokenSource();

            var writeTask = Task.Run(async () =>
            {
                if (report.RawLength != Info.OutputReportLength)
                {
                    throw new ApplicationException("Report length does not equal to output report length specified for connected device.");
                }

                var buffer = report.RawData.ToArray();

                Debug.WriteLine($"{nameof(Win32Hid)}: {nameof(WriteAsyncProtected)} begin write.");

                Debug.WriteLine($"{nameof(Win32Hid)}: {nameof(WriteAsyncProtected)} data for write: {string.Join(" ", buffer.Select(x => x.ToString("X2")))}");

                await _fileStream.WriteAsync(buffer.AsMemory(0, buffer.Length), writeCancellationTokenSource.Token).ConfigureAwait(false);
                
                _fileStream.Flush();

                Debug.WriteLine($"{nameof(Win32Hid)}: {nameof(WriteAsyncProtected)} end write.");
            },
            writeCancellationTokenSource.Token);

            if (timeout == Timeout.InfiniteTimeSpan)
            {
                await writeTask.ConfigureAwait(false);
            }
            else
            {
                Debug.WriteLine($"{nameof(Win32Hid)}: {nameof(WriteAsyncProtected)} timeout: {timeout.GetWholeMilliseconds()} milliseconds.");

                using var timeoutCancellationTokenSource = cancellationToken.CanBeCanceled ? CancellationTokenSource.CreateLinkedTokenSource(cancellationToken, default) : new CancellationTokenSource();

                var completedTask = await Task.WhenAny(writeTask, Task.Delay(timeout, timeoutCancellationTokenSource.Token)).ConfigureAwait(false);

                if (completedTask == writeTask)
                {
                    timeoutCancellationTokenSource.Cancel();

                    await writeTask.ConfigureAwait(false);
                }
                else
                {
                    writeCancellationTokenSource.Cancel();

                    throw new TimeoutException();
                }
            }
        }

        /// <summary>
        /// Returns a string that represents the current Human Interface Device.
        /// </summary>
        /// <returns>HID info.</returns>
        public override string ToString()
        {
            CheckDispose();

            return Info.ToString();
        }

        /// <summary>
        /// Creates USB device handle.
        /// </summary>
        /// <param name="devPath">Win32 device path.</param>
        private static SafeFileHandle CreateDeviceHandle(DevicePath devPath)
        {
            // Create the handle from the device path.
            var handle = Win32Native.CreateFile(
                devPath.Path,
                Win32Native.CfDesiredAccess.GENERIC_READ | Win32Native.CfDesiredAccess.GENERIC_WRITE,
                Win32Native.CfShareMode.FILE_SHARE_READ | Win32Native.CfShareMode.FILE_SHARE_WRITE,
                IntPtr.Zero,
                Win32Native.CfCreationDisposition.OPEN_EXISTING,
                Win32Native.CfFlagsAndAttributes.FILE_FLAG_OVERLAPPED,
                IntPtr.Zero);

            if (handle.IsInvalid)
            {
                var win32Exception = new Win32Exception(Marshal.GetLastWin32Error());
                throw new ApplicationException($"Failed to create device file: ({win32Exception.NativeErrorCode:X8}) {win32Exception.Message}", win32Exception);
            }

            Debug.WriteLine($"{nameof(Win32Hid)}: {nameof(CreateDeviceHandle)}: CreateFile executed with handle " +
                $"0x{((IntPtr)typeof(SafeFileHandle).GetField("handle", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(handle)).ToString($"X{IntPtr.Size * 2}")}.");

            return handle;
        }

        /// <summary>
        /// Looking for device special strings: manufacturer and product descriptions, serial number.
        /// </summary>
        /// <param name="devPath">Win32 device path.</param>
        /// <param name="devHandle">USB device handle.</param>
        /// <returns></returns>
        private static HidInfo FindDeviceInfo(DevicePath devPath, SafeFileHandle devHandle)
        {
            if (!Win32Native.HidD_GetAttributes(
                devHandle,
                out var hidAttributes))
            {
                throw new ApplicationException("Failed to get HID attributes.");
            }


            Win32Native.HIDP_CAPS hidCaps;

            SafePreparsedDataHandle preparsedDataHandle = null;

            try
            {
                // Get Windows to read the device data into an internal buffer.
                if (!Win32Native.HidD_GetPreparsedData(
                    devHandle,
                    out preparsedDataHandle))
                {
                    var win32Exception = new Win32Exception(Marshal.GetLastWin32Error());
                    throw new ApplicationException($"Failed to get preparsed data: ({win32Exception.NativeErrorCode:X8}) {win32Exception.Message}", win32Exception);
                }

                Debug.WriteLine($"{nameof(Win32Hid)}: {nameof(FindDeviceInfo)}: HidD_GetPreparsedData executed with handle " +
                    $"0x{((IntPtr)typeof(SafePreparsedDataHandle).GetField("handle", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(preparsedDataHandle)).ToString($"X{IntPtr.Size * 2}")}.");

                // Extract the device capabilities from the internal buffer, get the input and output report lengths.
                var capsResult = Win32Native.HidP_GetCaps(
                    preparsedDataHandle,
                    out hidCaps);

                if (capsResult != Win32Native.HIDP_STATUS.HIDP_STATUS_SUCCESS)
                {
                    throw new ApplicationException($"Failed to get caps with error {capsResult}.");
                }

                Debug.WriteLine($"{nameof(Win32Hid)}: {nameof(FindDeviceInfo)}: HidP_GetCaps executed with OutputReportLength: {hidCaps.OutputReportByteLength} and InputReportLength: {hidCaps.InputReportByteLength}.");
            }
            finally
            {
                preparsedDataHandle?.Dispose();
            }


            // Get manufacturer, product descriptions and device serial number.
            var manufacturerDescription = new StringBuilder(128);
            var productStrDescription = new StringBuilder(128);
            var serialNumberStrDescription = new StringBuilder(128);

            if (!Win32Native.HidD_GetManufacturerString(
                devHandle,
                manufacturerDescription,
                manufacturerDescription.Capacity))
            {
                manufacturerDescription = null;

                var win32Exception = new Win32Exception(Marshal.GetLastWin32Error());
                Debug.WriteLine($"{nameof(Win32Hid)}: {nameof(FindDeviceInfo)}: Failed to get HID manufacturer string: ({win32Exception.NativeErrorCode:X8}) {win32Exception.Message}");
            }

            if (!Win32Native.HidD_GetProductString(
                devHandle,
                productStrDescription,
                productStrDescription.Capacity))
            {
                productStrDescription = null;

                var win32Exception = new Win32Exception(Marshal.GetLastWin32Error());
                Debug.WriteLine($"{nameof(Win32Hid)}: {nameof(FindDeviceInfo)}: Failed to get HID product string: ({win32Exception.NativeErrorCode:X8}) {win32Exception.Message}");
            }

            if (!Win32Native.HidD_GetSerialNumberString(
                devHandle,
                serialNumberStrDescription,
                serialNumberStrDescription.Capacity))
            {
                serialNumberStrDescription = null;

                var win32Exception = new Win32Exception(Marshal.GetLastWin32Error());
                Debug.WriteLine($"{nameof(Win32Hid)}: {nameof(FindDeviceInfo)}: Failed to get HID serial number string: ({win32Exception.NativeErrorCode:X8}) {win32Exception.Message}");
            }

            Debug.WriteLine($"{nameof(Win32Hid)}: FindDeviceInfo: Manufacturer: \"{manufacturerDescription}\" Product: \"{productStrDescription}\" SerialNumber: \"{serialNumberStrDescription}\".");

            return new HidInfo(devPath, hidCaps, hidAttributes, manufacturerDescription?.ToString(), productStrDescription?.ToString(), serialNumberStrDescription?.ToString());
        }


        /// <summary>
        /// Throws <see cref="ObjectDisposedException"/> if <see cref="Win32Hid"/> already disposed.
        /// </summary>
        private void CheckDispose()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(nameof(Win32Hid));
            }
        }

        /// <summary>
        /// Flag: Has Dispose already been called?
        /// </summary>
        private bool _disposed = false;

        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    try
                    {
                        _fileStream?.Flush();
                    }
                    catch (IOException ex)
                    {
                        Debug.WriteLine($"{nameof(Win32Hid)}: {nameof(Dispose)}: {nameof(_fileStream)} flush exception: {ex.Message}.");
                    }

                    try
                    {
                        _fileStream?.Dispose();
                    }
                    catch (IOException ex)
                    {
                        Debug.WriteLine($"{nameof(Win32Hid)}: {nameof(Dispose)}: {nameof(_fileStream)} dispose exception: {ex.Message}.");
                    }

                    _handle?.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~Win32Hid()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        /// <summary>
        /// Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
