﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Goldfynger.Usb.CustomHid.Win32;

namespace Goldfynger.Usb.CustomHid.ConcurrentEcho
{
    class Program
    {
        static async Task Main(string[] args)
        {
            if (args.Length < 1 || !ushort.TryParse(args[0], out ushort vid))
            {
                Console.WriteLine("First argument must be VID of device.");
                Console.ReadLine();
                return;
            }

            if (args.Length < 2 || !ushort.TryParse(args[1], out ushort pid))
            {
                Console.WriteLine("Second argument must be PID of device.");
                Console.ReadLine();
                return;
            }

            Console.WriteLine($"Echo started with VID:{vid} PID:{pid}.");

            while (true)
            {
                Console.WriteLine($"Waiting for device...");

                try
                {
                    while (true)
                    {
                        var devicePath = DevicePath.FindDevicePaths(vid, pid).FirstOrDefault();

                        if (devicePath == null)
                        {
                            Thread.Sleep(1000);
                            continue;
                        }

                        using var usbDevice = new GenericUsbHid(devicePath, GenericUsbHidReadModes.Single);

                        Console.WriteLine($"Connected to device {usbDevice}. InputReportLength:{usbDevice.Info.InputReportLength} OutputReportLength:{usbDevice.Info.InputReportLength}");

                        if (usbDevice.Info.InputReportLength < 2 || usbDevice.Info.InputReportLength > 65)
                        {
                            throw new ApplicationException($"Invalid {nameof(ConcurrentUsbHid.Info.InputReportLength)}: {usbDevice.Info.InputReportLength}.");
                        }

                        if (usbDevice.Info.OutputReportLength < 2 || usbDevice.Info.OutputReportLength > 65)
                        {
                            throw new ApplicationException($"Invalid {nameof(ConcurrentUsbHid.Info.OutputReportLength)}: {usbDevice.Info.OutputReportLength}.");
                        }

                        if (usbDevice.Info.InputReportLength != usbDevice.Info.OutputReportLength)
                        {
                            throw new ApplicationException($"{nameof(ConcurrentUsbHid.Info.InputReportLength)} must be equal to {nameof(ConcurrentUsbHid.Info.OutputReportLength)}.");
                        }

                        while (true)
                        {
                            Console.WriteLine($"Enter number of concurrent requests (1 - 255):");
                            var numberString = Console.ReadLine();
                            if (!byte.TryParse(numberString, out byte number) || number == 0)
                            {
                                Console.WriteLine($"Invalid input string \"{numberString}\".");
                                continue;
                            }

                            await SingleEcho(usbDevice, number);
                            await ContinuousEcho(usbDevice, number);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Exception: {ex.Message}");
                }
            }
        }


        private static async Task SingleEcho(GenericUsbHid generic, byte echoNumber)
        {
            Console.WriteLine($"Single:");

            using var singleConcurrent = new ConcurrentUsbHid(generic.Info.DevicePath, GenericUsbHidReadModes.Single);

            var dictionary = new Dictionary<byte, byte[]>();

            var tasks = new List<Task>();

            for (byte taskNumber = 0; taskNumber < echoNumber; taskNumber++)
            {
                var taskNumberCopy = taskNumber;
                var data = new byte[singleConcurrent.Info.InputReportLength - 2];
                new Random().NextBytes(data);

                dictionary.Add(taskNumberCopy, data);

                var writeTask = Task.Run(() =>
                {
                    var outputData = dictionary[taskNumberCopy];
                    var outputReportData = new List<byte> { taskNumberCopy };
                    outputReportData.AddRange(outputData);
                    var outputReport = new GenericUsbOutputReport(new ReadOnlyCollection<byte>(outputReportData));

                    singleConcurrent.Enqueue(outputReport);
                });

                var readTask = Task.Run(async () =>
                {
                    var inputReport = await singleConcurrent.ReadAsync();

                    var inputReportData = inputReport.ToArray();
                    var inputNumber = inputReport[0];
                    var inputData = new byte[inputReport.Count - 1];
                    Array.Copy(inputReportData, 1, inputData, 0, inputData.Length);

                    var result = dictionary[inputNumber].SequenceEqual(inputData);

                    Console.WriteLine($"Number: {inputNumber}, result: {result}.");
                });

                tasks.Add(writeTask);
                tasks.Add(readTask);
            }

            await Task.WhenAll(tasks);
        }

        private static async Task ContinuousEcho(GenericUsbHid generic, byte echoNumber)
        {
            Console.WriteLine($"Continuous:");

            using var singleConcurrent = new ConcurrentUsbHid(generic.Info.DevicePath, GenericUsbHidReadModes.Continuous);

            var dictionary = new Dictionary<byte, byte[]>();

            void completedHandler(object sender, ReadCompletedEventArgs<GenericUsbInputReport> e)
            {
                var inputReport = e.InputReport;

                var inputReportData = inputReport.ToArray();
                var inputNumber = inputReport[0];
                var inputData = new byte[inputReport.Count - 1];
                Array.Copy(inputReportData, 1, inputData, 0, inputData.Length);

                var result = dictionary[inputNumber].SequenceEqual(inputData);

                Console.WriteLine($"Number: {inputNumber}, result: {result}.");
            }

            void readErrorHandler(object sender, ExchangeExceptionEventArgs e)
            {
                Console.WriteLine($"Read error: {e.Exception.Message}.");
            }

            void writeErrorHandler(object sender, ExchangeExceptionEventArgs e)
            {
                Console.WriteLine($"Write error: {e.Exception.Message}.");
            }

            singleConcurrent.ReadCompleted += completedHandler;
            singleConcurrent.ReadExceptionOccurred += readErrorHandler;
            singleConcurrent.WriteExceptionOccurred += writeErrorHandler;

            var tasks = new List<Task>();

            for (byte taskNumber = 0; taskNumber < echoNumber; taskNumber++)
            {
                var taskNumberCopy = taskNumber;
                var data = new byte[singleConcurrent.Info.InputReportLength - 2];
                new Random().NextBytes(data);

                dictionary.Add(taskNumberCopy, data);

                var writeTask = Task.Run(() =>
                {
                    var outputData = dictionary[taskNumberCopy];
                    var outputReportData = new List<byte> { taskNumberCopy };
                    outputReportData.AddRange(outputData);
                    var outputReport = new GenericUsbOutputReport(new ReadOnlyCollection<byte>(outputReportData));

                    singleConcurrent.Enqueue(outputReport);
                });

                tasks.Add(writeTask);
            }

            await Task.WhenAll(tasks);

            await Task.Delay(echoNumber * 50);

            singleConcurrent.ReadCompleted -= completedHandler;
            singleConcurrent.ReadExceptionOccurred -= readErrorHandler;
            singleConcurrent.WriteExceptionOccurred -= writeErrorHandler;
        }
    }
}
