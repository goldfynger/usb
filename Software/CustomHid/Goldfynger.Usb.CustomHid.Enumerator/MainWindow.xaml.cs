﻿using System;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;

using Goldfynger.Usb.CustomHid.Win32;
using Goldfynger.Usb.CustomHid.Wmi;

namespace Goldfynger.Usb.CustomHid.Enumerator
{
    public partial class MainWindow : Window, IDisposable
    {
        private Action<int, IntPtr> _addHook = null;

        private ushort _vid = 1155;

        private ushort _pid = 22352;

        private GenericUsbHid _usbDevice = null;

        private ObservableWin32HidWatcher _win32Watcher = null;
        

        public MainWindow()
        {
            InitializeComponent();

            Loaded += MainWindow_Loaded;
        }


        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var handle = new WindowInteropHelper(this).Handle;
            HwndSource.FromHwnd(handle).AddHook(new HwndSourceHook(WndProc));

            _win32Watcher = new ObservableWin32HidWatcher(handle, addHook => _addHook = addHook);

            _win32Watcher.DeviceArrived += Win32Watcher_DeviceArrived;
            _win32Watcher.DeviceRemoved += Win32Watcher_DeviceRemoved;

            tbVid.Text = _vid.ToString();
            tbPid.Text = _pid.ToString();

            tbVid.TextChanged += TbVid_TextChanged;
            tbPid.TextChanged += TbPid_TextChanged;

            cbAutoConnect.Checked += CbAutoConnect_Checked;
            cbAutoConnect.Unchecked += CbAutoConnect_Unchecked;

            cbAutoConnect.IsChecked = true;

            bConnect.Click += BConnect_Click;

            miClearLog.Click += MiClearLog_Click;


            dgWin32Watcher.ItemsSource = _win32Watcher.DevicePaths;


            dgWmiWatcher.ItemsSource = new ObservableWmiHidWatcher(SynchronizationContext.Current).DeviceIds;
        }

        private IntPtr WndProc(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            _addHook?.Invoke(msg, wParam);

            return IntPtr.Zero;
        }

        private void Win32Watcher_DeviceArrived(object sender, EventArgs e)
        {
            if (cbAutoConnect.IsChecked.Value)
            {
                TryConnect();
            }
        }

        private void Win32Watcher_DeviceRemoved(object sender, EventArgs e)
        {
            if (_usbDevice == null)
            {
                return;
            }

            var path = DevicePath.FindDevicePaths().FirstOrDefault(dev => dev.Vid == _vid && dev.Pid == _pid);

            if (path == null)
            {
                TryDispose();
            }
        }

        private void TbVid_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (ushort.TryParse(tbVid.Text, out ushort vid) && _vid != vid)
            {
                _vid = vid;

                TryDispose();
            }
            else
            {
                tbVid.Text = _vid.ToString();
            }
        }

        private void TbPid_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (ushort.TryParse(tbPid.Text, out ushort pid) && _pid != pid)
            {
                _pid = pid;

                TryDispose();
            }
            else
            {
                tbPid.Text = _pid.ToString();
            }
        }

        private void CbAutoConnect_Checked(object sender, RoutedEventArgs e)
        {
            tbVid.IsReadOnly = true;
            tbPid.IsReadOnly = true;
            bConnect.IsEnabled = false;

            TryConnect();
        }

        private void CbAutoConnect_Unchecked(object sender, RoutedEventArgs e)
        {
            tbVid.IsReadOnly = false;
            tbPid.IsReadOnly = false;
            bConnect.IsEnabled = true;
        }

        private void BConnect_Click(object sender, RoutedEventArgs e)
        {
            if (_usbDevice != null)
            {
                TryDispose();
            }
            else
            {
                TryConnect();
            }
        }

        private void MiClearLog_Click(object sender, RoutedEventArgs e)
        {
            tbLog.Clear();
        }


        private void LogLine(string message)
        {
            if (tbLog.Text.Length != 0)
            {
                tbLog.Text += Environment.NewLine;
            }

            tbLog.Text += $"{DateTime.Now:dd.MM.yy HH:mm:ss.ffffff}: {message}";

            tbLog.ScrollToEnd();
        }

        private void TryConnect()
        {
            if (_usbDevice != null)
            {
                return;
            }

            var path = DevicePath.FindDevicePaths().FirstOrDefault(dev => dev.Vid == _vid && dev.Pid == _pid);

            if (path != null)
            {
                LogLine("Connection...");

                try
                {
                    _usbDevice = new GenericUsbHid(path, GenericUsbHidReadModes.Single);
                }
                catch (Exception ex)
                {
                    LogLine($"Connection exception: {ex.Message}");

                    bConnect.Content = "Connect";

                    return;
                }

                bConnect.Content = "Disconnect";

                LogLine($"Device connected: {_usbDevice}.");
            }
            else
            {
                LogLine("Can not find device with specified VID and PID.");
            }
        }

        private void TryDispose()
        {
            if (_usbDevice == null)
            {
                return;
            }

            _usbDevice.Dispose();
            _usbDevice = null;

            bConnect.Content = "Connect";

            LogLine("Device disconnected.");
        }


        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    _usbDevice?.Dispose();
                    _win32Watcher?.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~MainWindow()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
