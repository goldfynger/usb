﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;

using Goldfynger.Usb.CustomHid.Win32;

namespace Goldfynger.Usb.CustomHid.BlinkButtons
{
    public partial class MainWindow : Window, IDisposable
    {
        private Action<int, IntPtr> _addHook = null;

        private UsbDevice _usbDevice = null;

        private States _state = States.None;

        private Win32HidWatcher _win32Watcher;
        

        public MainWindow()
        {
            InitializeComponent();

            Loaded += MainWindow_Loaded;
        }


        private async void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var handle = new WindowInteropHelper(this).Handle;
            HwndSource.FromHwnd(handle).AddHook(new HwndSourceHook(WndProc));

            _win32Watcher = new Win32HidWatcher(handle, addHook => _addHook = addHook);

            _win32Watcher.DeviceArrived += Win32Watcher_DeviceArrived;
            _win32Watcher.DeviceRemoved += Win32Watcher_DeviceRemoved;

            DisableButtons();

            bBlue.Click += BBlue_Click;
            bGreen.Click += BGreen_Click;
            bRed.Click += BRed_Click;
            bYellow.Click += BYellow_Click;

            await TryConnect();
        }

        private IntPtr WndProc(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            _addHook?.Invoke(msg, wParam);

            return IntPtr.Zero;
        }

        private async void Win32Watcher_DeviceArrived(object sender, EventArgs e)
        {
            await TryConnect();
        }

        private void Win32Watcher_DeviceRemoved(object sender, EventArgs e)
        {
            if (_usbDevice == null)
            {
                return;
            }

            var path = DevicePath.FindDevicePaths().FirstOrDefault(dev => dev.Vid == 1155 && dev.Pid == 22352);

            if (path == null)
            {
                TryDispose();
            }
        }

        private async void BBlue_Click(object sender, RoutedEventArgs e)
        {
            DisableButtons();

            var state = ToggleState(_state, States.Blue);

            if (await WriteState(state))
            {
                _state = state;
                UpdateLabels();
            }
            else
            {
                await WriteState(_state);
            }

            EnableButtons();
        }

        private async void BGreen_Click(object sender, RoutedEventArgs e)
        {
            DisableButtons();

            var state = ToggleState(_state, States.Green);

            if (await WriteState(state))
            {
                _state = state;
                UpdateLabels();
            }
            else
            {
                await WriteState(_state);
            }

            EnableButtons();
        }

        private async void BRed_Click(object sender, RoutedEventArgs e)
        {
            DisableButtons();

            var state = ToggleState(_state, States.Red);

            if (await WriteState(state))
            {
                _state = state;
                UpdateLabels();
            }
            else
            {
                await WriteState(_state);
            }

            EnableButtons();
        }

        private async void BYellow_Click(object sender, RoutedEventArgs e)
        {
            DisableButtons();

            var state = ToggleState(_state, States.Yellow);

            if (await WriteState(state))
            {
                _state = state;
                UpdateLabels();
            }
            else
            {
                await WriteState(_state);
            }

            EnableButtons();
        }

        private async void UsbDevice_StateReadCompleted(object sender, StateEventArgs e)
        {
            await Dispatcher.InvokeAsync(async () =>
            {
                DisableButtons();

                var state = _state;

                if (e.State.HasFlag(States.Blue))
                {
                    state = ToggleState(state, States.Blue);
                }

                if (e.State.HasFlag(States.Green))
                {
                    state = ToggleState(state, States.Green);
                }

                if (e.State.HasFlag(States.Red))
                {
                    state = ToggleState(state, States.Red);
                }

                if (e.State.HasFlag(States.Yellow))
                {
                    state = ToggleState(state, States.Yellow);
                }

                if (await WriteState(state))
                {
                    _state = state;
                    UpdateLabels();
                }
                else
                {
                    await WriteState(_state);
                }

                EnableButtons();
            });
        }

        private async void UsbDevice_ReadExceptionOccurred(object sender, ExchangeExceptionEventArgs e)
        {
            await Dispatcher.InvokeAsync(() =>
            {
                TryDispose();
            });
        }


        private void DisableButtons()
        {
            bBlue.IsEnabled = false;
            bGreen.IsEnabled = false;
            bRed.IsEnabled = false;
            bYellow.IsEnabled = false;
        }

        private void EnableButtons()
        {
            bBlue.IsEnabled = true;
            bGreen.IsEnabled = true;
            bRed.IsEnabled = true;
            bYellow.IsEnabled = true;
        }

        private async Task TryConnect()
        {
            if (_usbDevice != null)
            {
                return;
            }

            var path = DevicePath.FindDevicePaths().FirstOrDefault(dev => dev.Vid == 1155 && dev.Pid == 22352);

            if (path != null)
            {
                try
                {
                    _usbDevice = new UsbDevice(path);
                }
                catch
                {
                    bStatus.BorderBrush = Brushes.Red;

                    return;
                }

                await WriteState(_state);

                _usbDevice.StateReadCompleted += UsbDevice_StateReadCompleted;
                _usbDevice.ReadExceptionOccurred += UsbDevice_ReadExceptionOccurred;

                bStatus.BorderBrush = Brushes.Green;

                EnableButtons();
            }
        }

        private void TryDispose()
        {
            DisableButtons();

            if (_usbDevice == null)
            {
                return;
            }

            _usbDevice.StateReadCompleted -= UsbDevice_StateReadCompleted;
            _usbDevice.ReadExceptionOccurred -= UsbDevice_ReadExceptionOccurred;

            _usbDevice.Dispose();
            _usbDevice = null;

            bStatus.BorderBrush = Brushes.Red;
        }


        private void UpdateLabels()
        {
            if (_state.HasFlag(States.Blue))
            {
                lBlue.Opacity = 1;
            }
            else
            {
                lBlue.Opacity = 0.2;
            }

            if (_state.HasFlag(States.Green))
            {
                lGreen.Opacity = 1;
            }
            else
            {
                lGreen.Opacity = 0.2;
            }

            if (_state.HasFlag(States.Red))
            {
                lRed.Opacity = 1;
            }
            else
            {
                lRed.Opacity = 0.2;
            }

            if (_state.HasFlag(States.Yellow))
            {
                lYellow.Opacity = 1;
            }
            else
            {
                lYellow.Opacity = 0.2;
            }
        }

        private async Task<bool> WriteState(States state)
        {
            try
            {
                if (_usbDevice != null)
                {
                    await _usbDevice.WriteStateAsync(state);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (TimeoutException)
            {
                return false;
            }
            catch
            {
                TryDispose();

                return false;
            }
        }


        private static States ToggleState(States current, States flag)
        {
            if (current.HasFlag(flag))
            {
                return current &= ~flag;
            }
            else
            {
                return current |= flag;
            }
        }


        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    _usbDevice.Dispose();
                    _win32Watcher.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~MainWindow()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }


        private sealed class UsbDevice : GenericUsbHid<UsbDeviceInputReport, UsbDeviceOutputReport>
        {
            public event EventHandler<StateEventArgs> StateReadCompleted;

            public UsbDevice(DevicePath devPath) : base(devPath, array => new UsbDeviceInputReport(array), GenericUsbHidReadModes.Continuous)
            {
                ReadCompleted += UsbDevice_ReadCompleted;
            }
            

            public async Task WriteStateAsync(States state)
            {
                await WriteAsync(new UsbDeviceOutputReport(state), TimeSpan.FromSeconds(2)).ConfigureAwait(false);
            }

            private void UsbDevice_ReadCompleted(object sender, ReadCompletedEventArgs<UsbDeviceInputReport> e)
            {
                StateReadCompleted?.Invoke(sender, new StateEventArgs(e.InputReport.State));
            }
        }

        private sealed class UsbDeviceInputReport : BaseInputReport
        {
            internal UsbDeviceInputReport(byte[] rawData) : base(rawData)
            {
            }

            internal States State => (States)this[0];
        }

        private sealed class UsbDeviceOutputReport : BaseOutputReport
        {
            internal UsbDeviceOutputReport(States state) : base(CreateOutputData((byte)state), 0)
            {
            }

            internal States State => (States)this[0];

            private static ReadOnlyCollection<byte> CreateOutputData(byte state)
            {
                var data = new byte[64];

                data[0] = state;

                return new ReadOnlyCollection<byte>(data);
            }
        }

        private sealed class StateEventArgs : EventArgs
        {
            internal StateEventArgs(States state)
            {
                State = state;
            }

            internal States State { get; }
        }


        [Flags]
        private enum States : byte
        {
            None = 0,
            Blue = 1,
            Green = 2,
            Red = 4,
            Yellow = 8
        }
    }
}
