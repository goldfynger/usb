﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

using Goldfynger.ExchangeMonitor.ConsoleView.Communication;
using Goldfynger.Usb.CustomHid.Win32;

namespace Goldfynger.Usb.CustomHid.Echo
{
    public partial class MainWindow : Window
    {
        private ushort _vid = 1155;

        private ushort _pid = 22352;

        private int _length = 0;

        private int _repeat = 0;

        private byte _reportId = 0;

        private MonitorWrapper _readMonitor = null;

        private MonitorWrapper _writeMonitor = null;

        private MonitorWrapper _infoMonitor = null;


        public MainWindow()
        {
            InitializeComponent();

            Loaded += MainWindow_Loaded;

            Closed += MainWindow_Closed;
        }


        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            tbVid.Text = _vid.ToString();
            tbPid.Text = _pid.ToString();
            tbLength.Text = _length.ToString();
            tbRepeat.Text = _repeat.ToString();
            tbReportId.Text = _reportId.ToString();

            tbVid.TextChanged += TbVid_TextChanged;
            tbPid.TextChanged += TbPid_TextChanged;
            tbLength.TextChanged += TbLength_TextChanged;
            tbRepeat.TextChanged += TbRepeat_TextChanged;
            tbReportId.TextChanged += TbReportId_TextChanged;

            bWrite.Click += BWrite_Click;

            var readMonitorTask = MonitorActivator
                .Activate(@"C:\_bitbucket\Usb\Software\ExchangeMonitor\Goldfynger.ExchangeMonitor.ConsoleView.Monitor\bin\Debug\net5.0-windows\Goldfynger.ExchangeMonitor.ConsoleView.Monitor.exe");
            var writeMonitorTask = MonitorActivator
                .Activate(@"C:\_bitbucket\Usb\Software\ExchangeMonitor\Goldfynger.ExchangeMonitor.ConsoleView.Monitor\bin\Debug\net5.0-windows\Goldfynger.ExchangeMonitor.ConsoleView.Monitor.exe");
            var infoMonitorTask = MonitorActivator
                .Activate(@"C:\_bitbucket\Usb\Software\ExchangeMonitor\Goldfynger.ExchangeMonitor.ConsoleView.Monitor\bin\Debug\net5.0-windows\Goldfynger.ExchangeMonitor.ConsoleView.Monitor.exe");

            Task.WaitAll(readMonitorTask, writeMonitorTask, infoMonitorTask);

            _readMonitor = readMonitorTask.Result;
            _writeMonitor = writeMonitorTask.Result;
            _infoMonitor = infoMonitorTask.Result;

            _readMonitor.Client.WriteInQueue(new MonitorSettingsMessage(new MonitorSettings(24, "Read monitor", ConsoleColor.Red, ConsoleColor.Black, ConsoleColor.White)));
            _writeMonitor.Client.WriteInQueue(new MonitorSettingsMessage(new MonitorSettings(24, "Write monitor", ConsoleColor.Blue, ConsoleColor.Black, ConsoleColor.White)));
            _infoMonitor.Client.WriteInQueue(new MonitorSettingsMessage(new MonitorSettings(16, "Info monitor", ConsoleColor.Black, ConsoleColor.Black, ConsoleColor.White)));
        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            _readMonitor?.Dispose();
            _writeMonitor?.Dispose();
            _infoMonitor?.Dispose();
        }

        private void TbVid_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateOrCancel(tbVid, ref _vid, ushort.TryParse(tbVid.Text, out ushort value), value);
        }

        private void TbPid_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateOrCancel(tbPid, ref _pid, ushort.TryParse(tbPid.Text, out ushort value), value);
        }

        private void TbLength_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateOrCancel(tbLength, ref _length, int.TryParse(tbLength.Text, out int value), value);
        }

        private void TbRepeat_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateOrCancel(tbRepeat, ref _repeat, int.TryParse(tbRepeat.Text, out int value), value);
        }

        private void TbReportId_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateOrCancel(tbReportId, ref _reportId, byte.TryParse(tbReportId.Text, out byte value), value);
        }

        private async void BWrite_Click(object sender, RoutedEventArgs e)
        {
            var path = DevicePath.FindDevicePaths().FirstOrDefault(dev => dev.Vid == _vid && dev.Pid == _pid);

            if (path == null)
            {
                MessageBox.Show("Can not find HID with specified VID and PID.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                return;
            }

            bWrite.IsEnabled = false;

            try
            {
                using var usbDevice = new UsbDevice(path, _readMonitor, _writeMonitor);
                var repeat = 0;
                var beginTime = DateTime.Now;

                do
                {
                    var echoMessage = new EchoMessage(_length, _reportId, usbDevice.Info.OutputReportLength);

                    var echoResult = echoMessage.Compare(await usbDevice.ExchangeAsync(echoMessage.OutputPacket));

                    _infoMonitor.Client.WriteInQueue(new MonitorStringMessage($"Result of echo: {echoResult} repeat: {repeat}."));
                }
                while (++repeat <= _repeat);

                var bytesTotal = (_repeat + 1) * _length;
                var executionTime = (DateTime.Now - beginTime).TotalSeconds;

                _infoMonitor.Client.WriteInQueue(new MonitorStringMessage($"Times:{_repeat} bytes total:{bytesTotal} seconds total:{executionTime} speed bits/s:{(bytesTotal * 8) / executionTime}."));
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Exception: {ex.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            bWrite.IsEnabled = true;
        }


        private static void UpdateOrCancel<T>(TextBox sender, ref T field, bool update, T value) where T : IEquatable<T>
        {
            if (update)
            {
                if (!field.Equals(value))
                {
                    field = value;
                }
            }
            else
            {
                sender.Text = field.ToString();
            }
        }


        private sealed class UsbDevice : Win32Hid
        {
            private readonly MonitorWrapper _readMonitor;

            private readonly MonitorWrapper _writeMonitor;


            public UsbDevice(DevicePath devPath, MonitorWrapper readMonitor, MonitorWrapper writeMonitor) : base(devPath)
            {
                _readMonitor = readMonitor;
                _writeMonitor = writeMonitor;
            }


            public async Task<EchoInputPacket> ExchangeAsync(EchoOutputPacket outputPacket)
            {
                foreach (var outReport in outputPacket.Reports)
                {
                    await WriteAsyncProtected(outReport, TimeSpan.FromSeconds(1));

                    _writeMonitor.Client.WriteInQueue(new MonitorDataMessage(outReport.RawData));
                }

                var inReports = new List<UsbDeviceInputReport>(outputPacket.Reports.Count);

                for (var i = 0; i < outputPacket.Reports.Count; i++)
                {
                    var report = await ReadAsyncProtected(rawData => new UsbDeviceInputReport(rawData), TimeSpan.FromSeconds(3));

                    _readMonitor.Client.WriteInQueue(new MonitorDataMessage(report.RawData));

                    inReports.Add(report);
                }

                return new EchoInputPacket(inReports.AsReadOnly());
            }
        }

        private sealed class UsbDeviceInputReport : BaseInputReport
        {
            public const int WrongEffectiveLength = -1;


            public UsbDeviceInputReport(byte[] data) : base(data)
            {
            }


            public int EffectiveLength => ((Count >= 1) && (this[0] <= (Count - 1))) ? this[0] : WrongEffectiveLength;
        }

        private sealed class UsbDeviceOutputReport : BaseOutputReport
        {
            public UsbDeviceOutputReport(ReadOnlyCollection<byte> data, byte reportId) : base(data, reportId)
            {
            }


            public int EffectiveLength => this[0];
        }

        private sealed class DataEventArgs : EventArgs
        {
            public DataEventArgs(ReadOnlyCollection<byte> data)
            {
                Data = data;
            }


            public ReadOnlyCollection<byte> Data { get; }
        }

        private sealed class EchoInputPacket
        {
            public EchoInputPacket(ReadOnlyCollection<UsbDeviceInputReport> reports)
            {
                if (reports == null)
                {
                    IsValid = false;
                    Data = null;
                    Reports = null;
                    return;
                }


                var reportsArray = new UsbDeviceInputReport[reports.Count];

                reports.CopyTo(reportsArray, 0);

                Reports = new ReadOnlyCollection<UsbDeviceInputReport>(reportsArray);


                var data = new List<byte>();

                Data = new ReadOnlyCollection<byte>(data);


                if (reportsArray.Length == 0)
                {
                    IsValid = false;
                    return;
                }
                

                foreach (var report in reportsArray)
                {
                    if (report.Count < 1)
                    {
                        IsValid = false;
                        return;
                    }

                    if (report.EffectiveLength == UsbDeviceInputReport.WrongEffectiveLength)
                    {
                        IsValid = false;
                        return;
                    }

                    for (var i = 0; i < report.EffectiveLength; i++)
                    {
                        data.Add(report[i + 1]);
                    }
                }

                IsValid = true;
            }


            public ReadOnlyCollection<byte> Data { get; }

            public bool IsValid { get; }

            public ReadOnlyCollection<UsbDeviceInputReport> Reports { get; }
        }

        private sealed class EchoOutputPacket
        {
            public EchoOutputPacket(ReadOnlyCollection<byte> data, byte outputReportId, int outputReportLength)
            {
                if (data == null)
                {
                    throw new ArgumentNullException(nameof(data));
                }

                if (outputReportLength < 2)
                {
                    throw new ArgumentOutOfRangeException(nameof(outputReportLength));
                }

                if (outputReportLength < 3 && data.Count != 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(outputReportLength));
                }

                if (outputReportLength > byte.MaxValue)
                {
                    throw new ArgumentOutOfRangeException(nameof(outputReportLength));
                }

                var dataArray = new byte[data.Count];

                data.CopyTo(dataArray, 0);

                Data = new ReadOnlyCollection<byte>(dataArray);


                var reportMaxEffectiveLength = outputReportLength - 2;

                if (reportMaxEffectiveLength > 0)
                {
                    var isLastReportFull = data.Count != 0 && ((data.Count % reportMaxEffectiveLength) == 0);

                    var lastReportEffectiveLength = isLastReportFull ? reportMaxEffectiveLength : (data.Count % reportMaxEffectiveLength);

                    var reportsLength = (data.Count / reportMaxEffectiveLength) + (isLastReportFull ? 0 : 1);

                    var reports = new UsbDeviceOutputReport[reportsLength];


                    for (var i = 0; i < reportsLength; i++)
                    {
                        var isLastReport = (i + 1) == reportsLength;

                        var reportEffectiveLength = isLastReport ? lastReportEffectiveLength : reportMaxEffectiveLength;


                        var reportData = new List<byte> { (byte)reportEffectiveLength };

                        for (var j = 0; j < reportMaxEffectiveLength; j++)
                        {
                            if (j < reportEffectiveLength)
                            {
                                reportData.Add(data[(i * reportMaxEffectiveLength) + j]);
                            }
                            else
                            {
                                reportData.Add(0);
                            }
                        }

                        reports[i] = new UsbDeviceOutputReport(reportData.AsReadOnly(), outputReportId);
                    }


                    Reports = new ReadOnlyCollection<UsbDeviceOutputReport>(reports);
                }
                else
                {
                    Reports = new List<UsbDeviceOutputReport> { new UsbDeviceOutputReport(new List<byte> { 0 }.AsReadOnly(), outputReportId) }.AsReadOnly();
                }
            }


            public ReadOnlyCollection<byte> Data { get; }

            public ReadOnlyCollection<UsbDeviceOutputReport> Reports { get; }
        }

        private sealed class EchoMessage
        {
            public EchoMessage(int length, byte outputReportId, int outputReportLength)
            {
                var data = new byte[length];

                new Random().NextBytes(data);

                Data = new ReadOnlyCollection<byte>(data);

                OutputPacket = new EchoOutputPacket(Data, outputReportId, outputReportLength);
            }


            public EchoOutputPacket OutputPacket { get; }

            public ReadOnlyCollection<byte> Data { get; }


            public bool Compare(EchoInputPacket inputPacket)
            {
                if (inputPacket == null)
                {
                    return false;
                }

                if (!inputPacket.IsValid)
                {
                    return false;
                }

                if (inputPacket.Data.Count != Data.Count)
                {
                    return false;
                }

                for (var i = 0; i < Data.Count; i++)
                {
                    if (inputPacket.Data[i] != Data[i])
                    {
                        return false;
                    }
                }

                if (inputPacket.Reports.Count != OutputPacket.Reports.Count)
                {
                    return false;
                }

                return true;
            }
        }
    }
}
