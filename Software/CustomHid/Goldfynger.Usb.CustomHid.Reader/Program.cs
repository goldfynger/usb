﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Goldfynger.Usb.CustomHid.Win32;

namespace Goldfynger.Usb.CustomHid.Reader
{
    class Program
    {
        static async Task Main(string[] args)
        {
            if (args.Length < 1 || !ushort.TryParse(args[0], out ushort vid))
            {
                Console.WriteLine("First argument must be VID of device.");
                Console.ReadLine();
                return;
            }

            if (args.Length < 2 || !ushort.TryParse(args[1], out ushort pid))
            {
                Console.WriteLine("Second argument must be PID of device.");
                Console.ReadLine();
                return;
            }

            Console.WriteLine($"Reader started with VID:{vid} PID:{pid}.");

            while (true)
            {
                Console.WriteLine($"Waiting for device...");

                try
                {
                    while (true)
                    {
                        var devicePath = DevicePath.FindDevicePaths(vid, pid).FirstOrDefault();

                        if (devicePath == null)
                        {
                            Thread.Sleep(1000);
                            continue;
                        }

                        using var usbDevice = new GenericUsbHid(devicePath, GenericUsbHidReadModes.Single);

                        Console.WriteLine($"Connected to device {usbDevice}. Waiting for data (up to {usbDevice.Info.InputReportLength} bytes)...");

                        while (true)
                        {
                            Console.WriteLine($"{DateTime.Now:HH:mm:ss.ffffff}: {string.Join(" ", (await usbDevice.ReadAsync()).RawData.Select(x => x.ToString("X2")))}");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Exception: {ex.Message}");
                }
            }
        }
    }
}
