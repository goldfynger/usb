﻿using System;
using System.Management;

namespace Goldfynger.Usb.CustomHid.Wmi
{
    /// <summary>
    /// Watching for adding or removing USB HIDs.
    /// </summary>
    public class WmiHidWatcher : IDisposable
    {
        /// <summary>
        /// Device created event watcher.
        /// </summary>
        private readonly ManagementEventWatcher _creationWatcher;

        /// <summary>
        /// Device removed event watcher.
        /// </summary>
        private readonly ManagementEventWatcher _deletionWatcher;


        /// <summary>
        /// This event will be triggered when a device is arrived.
        /// </summary>
        public event EventHandler DeviceArrived;

        /// <summary>
        /// This event will be triggered when a device is removed.
        /// </summary>
        public event EventHandler DeviceRemoved;


        /// <summary>
        /// Creates new instance of WMI HID watcher.
        /// </summary>
        public WmiHidWatcher()
        {
            // Create and start watching events.
            _creationWatcher = new ManagementEventWatcher(
                new ManagementScope("root\\CIMV2") { Options = { EnablePrivileges = true } },
                new WqlEventQuery { EventClassName = "__InstanceCreationEvent", WithinInterval = new TimeSpan(0, 0, 1), Condition = @"TargetInstance ISA 'Win32_USBControllerDevice'" });

            _deletionWatcher = new ManagementEventWatcher(
                new ManagementScope("root\\CIMV2") { Options = { EnablePrivileges = true } },
                new WqlEventQuery { EventClassName = "__InstanceDeletionEvent", WithinInterval = new TimeSpan(0, 0, 1), Condition = @"TargetInstance ISA 'Win32_USBControllerDevice'" });


            _creationWatcher.EventArrived += (s, e) => OnDeviceArrived();

            _deletionWatcher.EventArrived += (s, e) => OnDeviceRemoved();


            _creationWatcher.Start();

            _deletionWatcher.Start();
        }


        /// <summary>
        /// Function invoked when USB device arrived.
        /// </summary>
        protected virtual void OnDeviceArrived() => DeviceArrived?.Invoke(this, EventArgs.Empty);

        /// <summary>
        /// Function invoked when USB device removed.
        /// </summary>
        protected virtual void OnDeviceRemoved() => DeviceRemoved?.Invoke(this, EventArgs.Empty);


        /// <summary>
        /// Flag: Has Dispose already been called?
        /// </summary>
        private bool _disposed = false;

        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    _creationWatcher?.Dispose();
                    _deletionWatcher?.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~WmiHidWatcher()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        /// <summary>
        /// Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
