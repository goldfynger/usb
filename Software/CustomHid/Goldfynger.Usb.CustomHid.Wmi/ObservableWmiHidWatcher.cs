﻿using System;
using System.Collections.ObjectModel;
using System.Threading;

namespace Goldfynger.Usb.CustomHid.Wmi
{
    /// <summary>
    /// Watching for adding or removing USB HIDs. Can be created only with existing synchronization context.
    /// </summary>
    public class ObservableWmiHidWatcher : WmiHidWatcher
    {
        /// <summary>
        /// Collection of available devices.
        /// </summary>
        private readonly ObservableCollection<DeviceId> _deviceIds;

        /// <summary>
        /// Used for access device observable collection.
        /// </summary>
        private readonly SynchronizationContext _syncContext;


        /// <summary>
        /// Creates new instance of observable HID watcher using synchronization context.
        /// </summary>
        /// <param name="syncContext">Synchronization context for event syncronization.</param>
        public ObservableWmiHidWatcher(SynchronizationContext syncContext)
        {
            _syncContext = syncContext ?? throw new ArgumentNullException(nameof(syncContext));


            _deviceIds = new ObservableCollection<DeviceId>(DeviceId.FindDeviceIds()); /* Search for devices first time. */

            DeviceIds = new ReadOnlyObservableCollection<DeviceId>(_deviceIds);
        }


        /// <summary>
        /// Read only collection of available devices.
        /// </summary>
        public ReadOnlyObservableCollection<DeviceId> DeviceIds { get; }


        /// <summary>
        /// Function invoked when USB device arrived.
        /// </summary>
        protected override void OnDeviceArrived()
        {
            base.OnDeviceArrived();

            UpdateCollection();
        }

        /// <summary>
        /// Function invoked when USB device removed.
        /// </summary>
        protected override void OnDeviceRemoved()
        {
            base.OnDeviceRemoved();

            UpdateCollection();
        }

        /// <summary>
        /// Update device collection.
        /// </summary>
        private void UpdateCollection()
        {
            var actualDevIds = DeviceId.FindDeviceIds();

            _syncContext.Post(state =>
            {
                var removeIdx = 0;

                while (true) /* Remove all unavailable paths. */
                {
                    if (removeIdx >= _deviceIds.Count) /* End of collection. */
                        break;

                    if (actualDevIds.IndexOf(_deviceIds[removeIdx]) < 0) /* Not found in actual collection - device removed. */
                    {
                        _deviceIds.RemoveAt(removeIdx);
                    }
                    else /* Else increment index. */
                    {
                        removeIdx++;
                    }
                }

                /* Here we have old collection and new actual collection that can have more items than old. */
                /* We can save order returned by FindDeviceIds and insert new devices in right places. */

                for (var addIdx = 0; addIdx < actualDevIds.Count; addIdx++) /* Add. */
                {
                    if (_deviceIds.IndexOf(actualDevIds[addIdx]) < 0) /* Not found in exisiting collection - new device. */
                    {
                        _deviceIds.Insert(addIdx, actualDevIds[addIdx]);
                    }
                }
            }, null);
        }
    }
}
