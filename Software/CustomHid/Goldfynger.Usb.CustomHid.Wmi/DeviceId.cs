﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Management;

namespace Goldfynger.Usb.CustomHid.Wmi
{
    /// <summary>
    /// Container for USB HID WMI identifier.
    /// </summary>
    public sealed record DeviceId : IEquatable<DeviceId>
    {
        private const string __vidPrefix = "VID_";

        private const string __pidPrefix = "PID_";


        private DeviceId(string deviceId)
        {
            Id = deviceId;

            Vid = ushort.Parse(deviceId.Substring(deviceId.IndexOf(__vidPrefix) + __vidPrefix.Length, 4), NumberStyles.HexNumber);

            Pid = ushort.Parse(deviceId.Substring(deviceId.IndexOf(__pidPrefix) + __pidPrefix.Length, 4), NumberStyles.HexNumber);
        }


        /// <summary>
        /// Unique ID of device in OS.
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// Vendor ID.
        /// </summary>
        public ushort Vid { get; }

        /// <summary>
        /// Product ID.
        /// </summary>
        public ushort Pid { get; }


        /// <summary>
        /// Returns ID of device.
        /// </summary>
        /// <returns>Identifier.</returns>
        public override string ToString() => Id;

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns><see langword="true"/> if the current object is equal to the other parameter; otherwise, <see langword="false"/>.</returns>
        public bool Equals(DeviceId other) => other != null && other.Id == Id;

        /// <summary>
        /// Returns the hash code for this object.
        /// </summary>
        /// <returns>A 32-bit signed integer hash code.</returns>
        public override int GetHashCode() => Id.GetHashCode();

        /// <summary>
        /// Returns information about USB HID WMI identifiers.
        /// </summary>
        /// <returns>Read only collection of all HID WMI identifiers ordered by identifier string representation.</returns>
        public static ReadOnlyCollection<DeviceId> FindDeviceIds()
        {
            return new ReadOnlyCollection<DeviceId>(new ManagementObjectSearcher(
                new ManagementScope("root\\CIMV2") { Options = { EnablePrivileges = true } },
                new WqlObjectQuery("SELECT * FROM Win32_PnPEntity WHERE DeviceID LIKE 'HID%'"))
                .Get()
                .OfType<ManagementObject>()
                .Select(o => new DeviceId(o.ToString()))
                .OrderBy(d => d.Id)
                .ToList());
        }
    }
}
