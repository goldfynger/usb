﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Windows;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Win32;

namespace UsbProjectUpdater
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Extension of file with settings.
        /// </summary>
        private const string __fileExtension = ".uvpupd";

        /// <summary>
        /// Application directory default name.
        /// </summary>
        private const string __defaultApplicationDirectory = "Usb\\Application";

        /// <summary>
        /// Driver directory default name.
        /// </summary>
        private const string __defaultDriverDirectory = "Usb\\Driver";

        private const string __cdcDirectoryName = "Cdc";

        private const string __customHidDirectoryName = "CustomHid";

        private const string __coreDirectoryName = "Core";

        private const string __classDirectoryName = "Class";


        /// <summary>
        /// Content of CDC application directory.
        /// </summary>
        private static readonly Dictionary<string, string> __cdcApplicationContent = new Dictionary<string, string>
        {
            { "usb_cdc_api.c",          __cdcDirectoryName },
            { "usb_cdc_api.h",          __cdcDirectoryName },
            { "usb_device.c",           __cdcDirectoryName },
            { "usb_device.h",           __cdcDirectoryName },
            { "usbd_cdc_if.c",          __cdcDirectoryName },
            { "usbd_cdc_if.h",          __cdcDirectoryName },
            { "usbd_conf.c",            __cdcDirectoryName },
            { "usbd_conf.h",            __cdcDirectoryName },
            { "usbd_desc.c",            __cdcDirectoryName },
            { "usbd_desc.h",            __cdcDirectoryName },
        };

        /// <summary>
        /// Content of Custom HID application directory.
        /// </summary>
        private static readonly Dictionary<string, string> __customHidApplicationContent = new Dictionary<string, string>
        {
            { "usb_customhid_api.c",    __customHidDirectoryName },
            { "usb_customhid_api.h",    __customHidDirectoryName },
            { "usb_device.c",           __customHidDirectoryName },
            { "usb_device.h",           __customHidDirectoryName },
            { "usbd_conf.c",            __customHidDirectoryName },
            { "usbd_conf.h",            __customHidDirectoryName },
            { "usbd_custom_hid_if.c",   __customHidDirectoryName },
            { "usbd_custom_hid_if.h",   __customHidDirectoryName },
            { "usbd_desc.c",            __customHidDirectoryName },
            { "usbd_desc.h",            __customHidDirectoryName },
        };

        /// <summary>
        /// Content of driver directory common for all classes.
        /// </summary>
        private static readonly Dictionary<string, string> __commonDriverContent = new Dictionary<string, string>
        {
            { "usbd_core.c",            __coreDirectoryName },
            { "usbd_core.h",            __coreDirectoryName },
            { "usbd_ctlreq.c",          __coreDirectoryName },
            { "usbd_ctlreq.h",          __coreDirectoryName },
            { "usbd_def.h",             __coreDirectoryName },
            { "usbd_ioreq.c",           __coreDirectoryName },
            { "usbd_ioreq.h",           __coreDirectoryName },
        };

        /// <summary>
        /// Content of driver directory for CDC.
        /// </summary>
        private static readonly Dictionary<string, string> __cdcDriverContent = new Dictionary<string, string>
        {
            { "usbd_cdc.c",             __classDirectoryName },
            { "usbd_cdc.h",             __classDirectoryName },
        };

        /// <summary>
        /// Content of driver directory for custom HID.
        /// </summary>
        private static readonly Dictionary<string, string> __customHidDriverContent = new Dictionary<string, string>
        {
            { "usbd_customhid.c",       __classDirectoryName },
            { "usbd_customhid.h",       __classDirectoryName },
        };

        /// <summary>
        /// Default content of project directories (all known files for all USB classes).
        /// </summary>
        private static readonly Dictionary<string, string> __originalApplicationFiles = new Dictionary<string, string>
        {
            { "usb_device.c",           "Src" },
            { "usb_device.h",           "Inc" },
            { "usbd_cdc_if.c",          "Src" },
            { "usbd_cdc_if.h",          "Inc" },
            { "usbd_conf.c",            "Src" },
            { "usbd_conf.h",            "Inc" },
            { "usbd_custom_hid_if.c",   "Src" },
            { "usbd_custom_hid_if.h",   "Inc" },
            { "usbd_desc.c",            "Src" },
            { "usbd_desc.h",            "Inc" },
        };


        /// <summary>
        /// File path dependency property key.
        /// </summary>
        private static readonly DependencyPropertyKey FilePathPropertyKey = DependencyProperty.RegisterReadOnly(nameof(FilePath), typeof(string), typeof(MainWindow), new PropertyMetadata(null));

        /// <summary>
        /// Project path dependency property key.
        /// </summary>
        private static readonly DependencyPropertyKey ProjectPathPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(ProjectPath), typeof(string), typeof(MainWindow), new PropertyMetadata(null, ProjectPath_PropertyChanged));

        /// <summary>
        /// Application path dependency property key.
        /// </summary>
        private static readonly DependencyPropertyKey ApplicationPathPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(ApplicationPath), typeof(string), typeof(MainWindow), new PropertyMetadata(null, ApplicationPath_PropertyChanged));

        /// <summary>
        /// Driver path dependency property key.
        /// </summary>
        private static readonly DependencyPropertyKey DriverPathPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(DriverPath), typeof(string), typeof(MainWindow), new PropertyMetadata(null, DriverPath_PropertyChanged));

        /// <summary>
        /// Project path is selected dependency property key.
        /// </summary>
        private static readonly DependencyPropertyKey ProjectPathSelectedPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(ProjectPathSelected), typeof(bool), typeof(MainWindow), new PropertyMetadata(false));

        /// <summary>
        /// Application path is valid dependency property key.
        /// </summary>
        private static readonly DependencyPropertyKey ApplicationPathValidPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(ApplicationPathValid), typeof(bool), typeof(MainWindow), new PropertyMetadata(false));

        /// <summary>
        /// Driver path is valid dependency property key.
        /// </summary>
        private static readonly DependencyPropertyKey DriverPathValidPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(DriverPathValid), typeof(bool), typeof(MainWindow), new PropertyMetadata(false));

        /// <summary>
        /// Log dependency property key.
        /// </summary>
        private static readonly DependencyPropertyKey LogPropertyKey = DependencyProperty.RegisterReadOnly(nameof(Log), typeof(string), typeof(MainWindow), new PropertyMetadata(null));

        /// <summary>
        /// Usb class is selected dependency property key.
        /// </summary>
        private static readonly DependencyPropertyKey UsbClassSelectedPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(UsbClassSelected), typeof(bool), typeof(MainWindow), new PropertyMetadata(false));


        /// <summary>
        /// File path dependency property.
        /// </summary>
        public static readonly DependencyProperty FilePathProperty = FilePathPropertyKey.DependencyProperty;

        /// <summary>
        /// Project path dependency property.
        /// </summary>
        public static readonly DependencyProperty ProjectPathProperty = ProjectPathPropertyKey.DependencyProperty;

        /// <summary>
        /// Application path dependency property.
        /// </summary>
        public static readonly DependencyProperty ApplicationPathProperty = ApplicationPathPropertyKey.DependencyProperty;

        /// <summary>
        /// Driver path dependency property.
        /// </summary>
        public static readonly DependencyProperty DriverPathProperty = DriverPathPropertyKey.DependencyProperty;

        /// <summary>
        /// Project path is selected dependency property.
        /// </summary>
        public static readonly DependencyProperty ProjectPathSelectedProperty = ProjectPathSelectedPropertyKey.DependencyProperty;

        /// <summary>
        /// Application path is valid dependency property.
        /// </summary>
        public static readonly DependencyProperty ApplicationPathValidProperty = ApplicationPathValidPropertyKey.DependencyProperty;

        /// <summary>
        /// Driver path is valid dependency property.
        /// </summary>
        public static readonly DependencyProperty DriverPathValidProperty = DriverPathValidPropertyKey.DependencyProperty;

        /// <summary>
        /// Log dependency property.
        /// </summary>
        public static readonly DependencyProperty LogProperty = LogPropertyKey.DependencyProperty;

        /// <summary>
        /// CDC is selected dependency property.
        /// </summary>
        public static readonly DependencyProperty CdcCheckedProperty =
            DependencyProperty.Register(nameof(CdcChecked), typeof(bool), typeof(MainWindow), new PropertyMetadata(false, CdcChecked_PropertyChanged));

        /// <summary>
        /// Custom HID is selected dependency property.
        /// </summary>
        public static readonly DependencyProperty CustomHidCheckedProperty =
            DependencyProperty.Register(nameof(CustomHidChecked), typeof(bool), typeof(MainWindow), new PropertyMetadata(false, CustomHidChecked_PropertyChanged));

        /// <summary>
        /// Usb class is selected dependency property.
        /// </summary>
        public static readonly DependencyProperty UsbClassSelectedProperty = UsbClassSelectedPropertyKey.DependencyProperty;

        /// <summary>
        /// Selected USB class.
        /// </summary>
        private UsbClass _selectedUsbClass = UsbClass.None;

        /// <summary>
        /// Flag for selected USB class update control.
        /// </summary>
        private bool _stopUsbClassUpdate = false;


        /// <summary>
        /// Main .ctor.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            Loaded += MainWindow_Loaded;

            bOpenFile.Click += BOpenFile_Click;
            bSaveFileAs.Click += BSaveFileAs_Click;
            bSelectProject.Click += BSelectProject_Click;
            bSelectApplication.Click += BSelectApplication_Click;
            bSelectDriver.Click += BSelectDriver_Click;
            bOpenHelp.Click += BOpenHelp_Click;
            bUpdate.Click += BUpdate_Click;

            tbLog.TextChanged += (s, e) => tbLog.ScrollToEnd();
        }


        /// <summary>
        /// Window loaded event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var args = Environment.GetCommandLineArgs();

            if (args.Length != 0)
            {
                var path = args.FirstOrDefault(arg => Path.GetExtension(arg) == __fileExtension);

                if (File.Exists(path))
                {
                    try
                    {
                        WriteLineToLog($"Try open file {path}...");

                        ApplySettings(this, TryLoadSettings(path));
                        FilePath = path;

                        WriteLineToLog("File opened successfully.");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Unable to open file: {ex.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                        WriteLineToLog($"File openinig failed: {ex.Message}");
                    }
                }
            }
        }

        /// <summary>
        /// Open file button click event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BOpenFile_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog { Filter = $"uVision5 project updater files (*{__fileExtension})|*{__fileExtension}", Multiselect = false };

            if (File.Exists(FilePath))
            {
                openFileDialog.InitialDirectory = Path.GetDirectoryName(FilePath);
            }

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    WriteLineToLog($"Try open file {openFileDialog.FileName}...");

                    ApplySettings(this, TryLoadSettings(openFileDialog.FileName));
                    FilePath = openFileDialog.FileName;

                    WriteLineToLog("File opened successfully.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Unable to open file: {ex.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                    WriteLineToLog($"File openinig failed: {ex.Message}");
                }
            }
        }

        /// <summary>
        /// Save file button click event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BSaveFileAs_Click(object sender, RoutedEventArgs e)
        {
            var saveFileDialog = new SaveFileDialog { Filter = $"   s (*{__fileExtension})|*{__fileExtension}", DefaultExt = __fileExtension, AddExtension = true };

            if (saveFileDialog.ShowDialog() == true)
            {
                var fileName = saveFileDialog.FileName;

                if (Path.GetExtension(saveFileDialog.FileName) != __fileExtension)
                {
                    fileName += $"{__fileExtension}";
                }

                try
                {
                    WriteLineToLog($"Try save file {saveFileDialog.FileName}...");

                    TrySaveSettings(fileName, CreateSettings(this));
                    FilePath = fileName;

                    WriteLineToLog("File saved successfully.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Unable to save file: {ex.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                    WriteLineToLog($"File saving failed: {ex.Message}");
                }
            }
        }

        /// <summary>
        /// Select project button click event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BSelectProject_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog { Filter = "µVision5 Projects (*.uvprojx)|*.uvprojx", Multiselect = false };

            if (File.Exists(ProjectPath))
            {
                openFileDialog.InitialDirectory = Path.GetDirectoryName(ProjectPath);
            }

            if (openFileDialog.ShowDialog() == true)
            {
                if (File.Exists(openFileDialog.FileName))
                {
                    ProjectPath = openFileDialog.FileName;



                    TryAutoFindForProject(this);
                }
                else
                {
                    MessageBox.Show("Invalid path to project file.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        /// <summary>
        /// Select application button click event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BSelectApplication_Click(object sender, RoutedEventArgs e)
        {
            using var dialog = new System.Windows.Forms.FolderBrowserDialog();

            if (Directory.Exists(ApplicationPath))
            {
                dialog.SelectedPath = ApplicationPath;
            }
            else if (File.Exists(ProjectPath))
            {
                dialog.SelectedPath = Path.GetDirectoryName(ProjectPath);
            }

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (Directory.Exists(dialog.SelectedPath))
                {
                    ApplicationPath = dialog.SelectedPath;
                }
                else
                {
                    MessageBox.Show("Invalid path to application directory.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        /// <summary>
        /// Select driver button click event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BSelectDriver_Click(object sender, RoutedEventArgs e)
        {
            using var dialog = new System.Windows.Forms.FolderBrowserDialog();

            if (Directory.Exists(DriverPath))
            {
                dialog.SelectedPath = DriverPath;
            }
            else if (File.Exists(ProjectPath))
            {
                dialog.SelectedPath = Path.GetDirectoryName(ProjectPath);
            }

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (Directory.Exists(dialog.SelectedPath))
                {
                    DriverPath = dialog.SelectedPath;
                }
                else
                {
                    MessageBox.Show("Invalid path to driver directory.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        /// <summary>
        /// Open help link.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BOpenHelp_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("https://bitbucket.org/goldfynger/usb/wiki/Keil%20project%20updater");
        }

        /// <summary>
        /// Update button click event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BUpdate_Click(object sender, RoutedEventArgs e)
        {
            WriteLineToLog("Begin update project.");

            TryUpdateProject(this);

            TryRemoveDefaultFiles(this);
        }

        /// <summary>
        /// Invokes when project path changed.
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void ProjectPath_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var window = (MainWindow)d;

            window.SaveSettings();

            window.WriteLineToLog($"New project selected: {window.ProjectPath}.");

            window.ProjectPathSelected = File.Exists(window.ProjectPath);
        }

        /// <summary>
        /// Invokes when application path changed.
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void ApplicationPath_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var window = (MainWindow)d;

            window.SaveSettings();

            window.WriteLineToLog($"New application directory selected: {window.ApplicationPath}.");

            window.ApplicationPathValid = CheckApplicationContent(window);
        }

        /// <summary>
        /// Invokes when driver path changed.
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void DriverPath_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var window = (MainWindow)d;

            window.SaveSettings();

            window.WriteLineToLog($"New driver directory selected: {window.DriverPath}.");

            window.DriverPathValid = CheckDriverContent(window);
        }

        /// <summary>
        /// Invokes when CDC selection changed.
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void CdcChecked_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var window = (MainWindow)d;

            if (window._stopUsbClassUpdate)
                return;

            window._stopUsbClassUpdate = true;

            ApplySelectedUsbClass(window, UsbClass.Cdc);

            window._stopUsbClassUpdate = false;

            window.SaveSettings();

            window.WriteLineToLog($"New USB class selected: {window._selectedUsbClass}.");
        }

        /// <summary>
        /// Invokes when Custom HID selection changed.
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void CustomHidChecked_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var window = (MainWindow)d;

            if (window._stopUsbClassUpdate)
                return;

            window._stopUsbClassUpdate = true;

            ApplySelectedUsbClass(window, UsbClass.CustomHid);

            window._stopUsbClassUpdate = false;

            window.SaveSettings();

            window.WriteLineToLog($"New USB class selected: {window._selectedUsbClass}.");
        }


        /// <summary>
        /// Settings file path.
        /// </summary>
        public string FilePath
        {
            get { return (string)GetValue(FilePathProperty); }
            private set { SetValue(FilePathPropertyKey, value); }
        }

        /// <summary>
        /// uVision project file path.
        /// </summary>
        public string ProjectPath
        {
            get { return (string)GetValue(ProjectPathProperty); }
            private set { SetValue(ProjectPathPropertyKey, value); }
        }

        /// <summary>
        /// Application directory path.
        /// </summary>
        public string ApplicationPath
        {
            get { return (string)GetValue(ApplicationPathProperty); }
            private set { SetValue(ApplicationPathPropertyKey, value); }
        }

        /// <summary>
        /// Driver directory path.
        /// </summary>
        public string DriverPath
        {
            get { return (string)GetValue(DriverPathProperty); }
            private set { SetValue(DriverPathPropertyKey, value); }
        }

        /// <summary>
        /// Is project path selected?
        /// </summary>
        public bool ProjectPathSelected
        {
            get { return (bool)GetValue(ProjectPathSelectedProperty); }
            private set { SetValue(ProjectPathSelectedPropertyKey, value); }
        }

        /// <summary>
        /// Is application path valid?
        /// </summary>
        public bool ApplicationPathValid
        {
            get { return (bool)GetValue(ApplicationPathValidProperty); }
            private set { SetValue(ApplicationPathValidPropertyKey, value); }
        }

        /// <summary>
        /// Is driver path valid?
        /// </summary>
        public bool DriverPathValid
        {
            get { return (bool)GetValue(DriverPathValidProperty); }
            private set { SetValue(DriverPathValidPropertyKey, value); }
        }

        /// <summary>
        /// String with full log.
        /// </summary>
        public string Log
        {
            get { return (string)GetValue(LogProperty); }
            private set { SetValue(LogPropertyKey, value); }
        }

        /// <summary>
        /// Is CDC selected?
        /// </summary>
        public bool CdcChecked
        {
            get { return (bool)GetValue(CdcCheckedProperty); }
            set { SetValue(CdcCheckedProperty, value); }
        }

        /// <summary>
        /// Is Custom HID selected?
        /// </summary>
        public bool CustomHidChecked
        {
            get { return (bool)GetValue(CustomHidCheckedProperty); }
            set { SetValue(CustomHidCheckedProperty, value); }
        }

        /// <summary>
        /// Is USB class selected?
        /// </summary>
        public bool UsbClassSelected
        {
            get { return (bool)GetValue(UsbClassSelectedProperty); }
            private set { SetValue(UsbClassSelectedPropertyKey, value); }
        }


        /// <summary>
        /// Saves current settings.
        /// </summary>
        private void SaveSettings()
        {
            if (FilePath != null)
            {
                try
                {
                    TrySaveSettings(FilePath, CreateSettings(this));
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Unable to save file: {ex.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        /// <summary>
        /// Log with endline characters.
        /// </summary>
        /// <param name="message"></param>
        private void WriteLineToLog(string message) => Log += $"{message}{Environment.NewLine}";

        /// <summary>
        /// Applies settings from file.
        /// </summary>
        /// <param name="window"></param>
        /// <param name="settings"></param>
        private static void ApplySettings(MainWindow window, Settings settings)
        {
            window.ProjectPath = settings.ProjectPath;
            window.ApplicationPath = settings.ApplicationPath;
            window.DriverPath = settings.DriverPath;


            if (window._stopUsbClassUpdate)
                throw new AccessViolationException($"Field \"{nameof(window._stopUsbClassUpdate)}\" already set.");

            window._stopUsbClassUpdate = true;

            ApplySelectedUsbClass(window, settings.UsbClass);

            window._stopUsbClassUpdate = false;
        }

        /// <summary>
        /// Applies selected USB class.
        /// </summary>
        /// <param name="window"></param>
        /// <param name="usbClass"></param>
        private static void ApplySelectedUsbClass(MainWindow window, UsbClass usbClass)
        {
            window._selectedUsbClass = ValidateUsbClass(usbClass);

            window.UsbClassSelected = usbClass.HasFlag(UsbClass.Cdc) || usbClass.HasFlag(UsbClass.CustomHid);

            window.ApplicationPathValid = CheckApplicationContent(window);

            window.DriverPathValid = CheckDriverContent(window);

            window.tbCdc.IsChecked = window._selectedUsbClass.HasFlag(UsbClass.Cdc);

            window.tbCustomHid.IsChecked = window._selectedUsbClass.HasFlag(UsbClass.CustomHid);
        }

        /// <summary>
        /// Validate USB class.
        /// </summary>
        /// <param name="usbClass"></param>
        /// <returns></returns>
        private static UsbClass ValidateUsbClass(UsbClass usbClass)
        {
            if (usbClass.HasFlag(UsbClass.Cdc))
                return UsbClass.Cdc;

            if (usbClass.HasFlag(UsbClass.CustomHid))
                return UsbClass.CustomHid;

            return UsbClass.None;
        }

        /// <summary>
        /// Creates file with settings.
        /// </summary>
        /// <param name="window"></param>
        /// <returns></returns>
        private static Settings CreateSettings(MainWindow window)
        {
            return new Settings(window.ProjectPath, window.ApplicationPath, window.DriverPath, window._selectedUsbClass);
        }

        /// <summary>
        /// Tries to load settings from a file.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private static Settings TryLoadSettings(string filePath)
        {
            using var stream = new FileStream(filePath, FileMode.Open);

            var serializer = new DataContractJsonSerializer(typeof(Settings));

            return (Settings)serializer.ReadObject(stream);
        }

        /// <summary>
        /// Tries to save settings in a file.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="settings"></param>
        private static void TrySaveSettings(string filePath, Settings settings)
        {
            using var stream = new FileStream(filePath, FileMode.Create);

            var serializer = new DataContractJsonSerializer(typeof(Settings));

            serializer.WriteObject(stream, settings);
        }

        /// <summary>
        /// Checks for application files.
        /// </summary>
        /// <param name="window"></param>
        /// <param name="selectedUsbClass"></param>
        /// <returns></returns>
        private static bool CheckApplicationContent(MainWindow window)
        {
            if (!Directory.Exists(window.ApplicationPath))
            {
                window.WriteLineToLog("Application directory path not exist.");
                return false;
            }

            switch (window._selectedUsbClass)
            {
                case UsbClass.Cdc:
                    if (!__cdcApplicationContent.All(p => File.Exists($"{window.ApplicationPath}\\{p.Value}\\{p.Key}")))
                    {
                        window.WriteLineToLog("Application directory check failed.");
                        return false;
                    }
                    break;

                case UsbClass.CustomHid:
                    if (!__customHidApplicationContent.All(p => File.Exists($"{window.ApplicationPath}\\{p.Value}\\{p.Key}")))
                    {
                        window.WriteLineToLog("Application directory check failed.");
                        return false;
                    }
                    break;

                default:
                    window.WriteLineToLog("Driver directory check failed: unknown USB class.");
                    return false;
            }

            window.WriteLineToLog("Application directory content checked successfully.");

            return true;
        }

        /// <summary>
        /// Checks for driver files.
        /// </summary>
        /// <param name="window"></param>
        /// <returns></returns>
        private static bool CheckDriverContent(MainWindow window)
        {
            if (!Directory.Exists(window.DriverPath))
            {
                window.WriteLineToLog("Driver directory path not exist.");
                return false;
            }

            if (!__commonDriverContent.All(p => File.Exists($"{window.DriverPath}\\{p.Value}\\{p.Key}")))
            {
                window.WriteLineToLog("Driver directory check failed (common part of driver).");
                return false;
            }

            switch (window._selectedUsbClass)
            {
                case UsbClass.Cdc:
                    if (!__cdcDriverContent.All(p => File.Exists($"{window.DriverPath}\\{p.Value}\\{p.Key}")))
                    {
                        window.WriteLineToLog("Driver directory check failed (CDC part of driver).");
                        return false;
                    }
                    break;

                case UsbClass.CustomHid:
                    if (!__customHidDriverContent.All(p => File.Exists($"{window.DriverPath}\\{p.Value}\\{p.Key}")))
                    {
                        window.WriteLineToLog("Driver directory check failed (custom HID part of driver).");
                        return false;
                    }
                    break;

                default:
                    window.WriteLineToLog("Driver directory check failed: unknown USB class.");
                    return false;
            }

            window.WriteLineToLog("Driver directory content checked successfully.");

            return true;
        }

        /// <summary>
        /// Searches for settings file and application and driver libraries.
        /// </summary>
        /// <param name="window"></param>
        private static void TryAutoFindForProject(MainWindow window)
        {
            if (File.Exists(window.ProjectPath))
            {
                if (window.FilePath == null && window.ApplicationPath == null && window.DriverPath == null)
                {
                    var filePath = $"{Path.GetDirectoryName(window.ProjectPath)}\\{Path.GetFileNameWithoutExtension(window.ProjectPath)}{__fileExtension}";

                    if (!File.Exists(filePath))
                    {
                        try
                        {
                            TrySaveSettings(filePath, CreateSettings(window));
                            window.FilePath = filePath;


                            var defaultApplicationPath = $"{Path.GetDirectoryName(Path.GetDirectoryName(window.ProjectPath))}\\{__defaultApplicationDirectory}";

                            if (Directory.Exists(defaultApplicationPath))
                                window.ApplicationPath = defaultApplicationPath;


                            var defaultDriverPath = $"{Path.GetDirectoryName(Path.GetDirectoryName(window.ProjectPath))}\\{__defaultDriverDirectory}";

                            if (Directory.Exists(defaultDriverPath))
                                window.DriverPath = defaultDriverPath;
                        }
                        catch
                        {
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Tries to update uVision project.
        /// </summary>
        /// <param name="window"></param>
        private static void TryUpdateProject(MainWindow window)
        {
            try
            {
                if (File.Exists(window.ProjectPath) && Directory.Exists(window.ApplicationPath) && Directory.Exists(window.DriverPath))
                {
                    // Root paths.
                    var rootProjectPath = Path.GetDirectoryName(Path.GetDirectoryName(window.ProjectPath));
                    var rootApplicationPath = Path.GetDirectoryName(window.ApplicationPath);
                    var rootDriverPath = Path.GetDirectoryName(window.DriverPath);

                    // Paths to USB application and USB driver include folders. Can be relative if root paths are the same.
                    var applicationPath = rootProjectPath == rootApplicationPath ? $"../{Path.GetFileName(window.ApplicationPath)}" : string.Join("/", window.ApplicationPath.Split('\\'));
                    var driverPath = rootProjectPath == rootDriverPath ? $"../{Path.GetFileName(window.DriverPath)}" : string.Join("/", window.DriverPath.Split('\\'));


                    var project = XDocument.Load(window.ProjectPath);


                    // Updates IncludePath variable.

                    var includePathRootElement = project.Root
                        .Element("Targets")
                        .Descendants("Target").First(target => target.Element("TargetName").Value == Path.GetFileNameWithoutExtension(window.ProjectPath))
                        .Element("TargetOption")
                        .Element("TargetArmAds")
                        .Element("Cads")
                        .Element("VariousControls");

                    var previousIncludePath = includePathRootElement.Element("IncludePath").Value;

                    var includePaths = previousIncludePath.Split(';').Select(p => p.Trim()).ToList();

                    // Removes paths to original driver includes if exists.
                    var originalCoreIncPath = includePaths.FirstOrDefault(p => p.Contains("Middlewares/ST/STM32_USB_Device_Library/Core/Inc"));
                    var originalCdcIncPath = includePaths.FirstOrDefault(p => p.Contains("Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc"));
                    var originalCustomHidIncPath = includePaths.FirstOrDefault(p => p.Contains("Middlewares/ST/STM32_USB_Device_Library/Class/CustomHID/Inc"));

                    if (originalCoreIncPath != null)
                    {
                        includePaths.Remove(originalCoreIncPath);
                        window.WriteLineToLog($"Include path removed: {originalCoreIncPath}.");
                    }

                    if (originalCdcIncPath != null)
                    {
                        includePaths.Remove(originalCdcIncPath);
                        window.WriteLineToLog($"Include path removed: {originalCdcIncPath}.");
                    }

                    if (originalCustomHidIncPath != null)
                    {
                        includePaths.Remove(originalCustomHidIncPath);
                        window.WriteLineToLog($"Include path removed: {originalCustomHidIncPath}.");
                    }

                    // Removes paths to previous version of driver includes if exists.
                    var possiblePreviousCdcApplicationIncPaths = includePaths.Where(p => p.Contains($"/{__cdcDirectoryName}"));
                    var possiblePreviousCustomHidIncPaths = includePaths.Where(p => p.Contains($"/{__customHidDirectoryName}"));
                    var possiblePreviousClassIncPaths = includePaths.Where(p => p.Contains($"/{__classDirectoryName}"));
                    var possiblePreviousCoreIncPaths = includePaths.Where(p => p.Contains($"/{__coreDirectoryName}"));

                    var pathsToRemove = new List<string>();

                    foreach (var path in possiblePreviousCdcApplicationIncPaths)
                    {
                        var substringForRemove = $"/{__cdcDirectoryName}";

                        if (path.EndsWith(substringForRemove))
                        {
                            if (path.Substring(0, path.LastIndexOf(substringForRemove)) != applicationPath)
                            {
                                if (MessageBox.Show($"Possible include path to previous application directory:{Environment.NewLine}{path}{Environment.NewLine}Remove?",
                                    "Remove?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                                        pathsToRemove.Add(path);
                            }
                        }
                    }

                    foreach (var path in possiblePreviousCustomHidIncPaths)
                    {
                        var substringForRemove = $"/{__customHidDirectoryName}";

                        if (path.EndsWith(substringForRemove))
                        {
                            if (path.Substring(0, path.LastIndexOf(substringForRemove)) != applicationPath)
                            {
                                if (MessageBox.Show($"Possible include path to previous application directory:{Environment.NewLine}{path}{Environment.NewLine}Remove?",
                                    "Remove?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                                        pathsToRemove.Add(path);
                            }
                        }
                    }

                    foreach (var path in possiblePreviousClassIncPaths)
                    {
                        var substringForRemove = $"/{__classDirectoryName}";

                        if (path.EndsWith(substringForRemove))
                        {
                            if (path.Substring(0, path.LastIndexOf(substringForRemove)) != driverPath)
                            {
                                if (MessageBox.Show($"Possible include path to previous driver directory:{Environment.NewLine}{path}{Environment.NewLine}Remove?",
                                    "Remove?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                                        pathsToRemove.Add(path);
                            }
                        }
                    }

                    foreach (var path in possiblePreviousCoreIncPaths)
                    {
                        var substringForRemove = $"/{__coreDirectoryName}";

                        if (path.EndsWith(substringForRemove))
                        {
                            if (path.Substring(0, path.LastIndexOf(substringForRemove)) != driverPath)
                            {
                                if (MessageBox.Show($"Possible include path to previous driver directory:{Environment.NewLine}{path}{Environment.NewLine}Remove?",
                                    "Remove?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                                        pathsToRemove.Add(path);
                            }
                        }
                    }

                    foreach (var pathToRemove in pathsToRemove)
                    {
                        includePaths.Remove(pathToRemove);
                        window.WriteLineToLog($"Include path removed: {pathToRemove}.");
                    }

                    // Adds paths to new application and driver includes if not exists.
                    var newCdcApplicationIncPath = applicationPath + "/Cdc";
                    var newCustomHidApplicationIncPath = applicationPath + "/CustomHid";
                    var newClassIncPath = driverPath + "/Class";
                    var newCoreIncPath = driverPath + "/Core";

                    switch (window._selectedUsbClass)
                    {
                        case UsbClass.Cdc:
                            if (!includePaths.Contains(newCdcApplicationIncPath))
                                includePaths.Add(newCdcApplicationIncPath);
                            break;

                        case UsbClass.CustomHid:
                            if (!includePaths.Contains(newCustomHidApplicationIncPath))
                                includePaths.Add(newCustomHidApplicationIncPath);
                            break;

                        default:
                            throw new ApplicationException("Unknown USB class");
                    }

                    if (!includePaths.Contains(newClassIncPath))
                        includePaths.Add(newClassIncPath);

                    if (!includePaths.Contains(newCoreIncPath))
                        includePaths.Add(newCoreIncPath);

                    window.WriteLineToLog($"New include paths:");
                    foreach (var path in includePaths)
                    {
                        window.WriteLineToLog($"{path}");
                    }

                    var newIncludePath = string.Join(";", includePaths);

                    includePathRootElement.SetElementValue("IncludePath", newIncludePath);


                    // Updates info about source files.

                    var groupsElement = project.Root
                        .Element("Targets")
                        .Descendants("Target").First(t => t.Element("TargetName").Value == Path.GetFileNameWithoutExtension(window.ProjectPath))
                        .Element("Groups");

                    var groups = groupsElement.Descendants("Group").ToDictionary(group => group.Element("GroupName").Value);

                    // Remove all groups from Groups element.
                    foreach (var group in groups)
                    {
                        group.Value.Remove();
                    }

                    // Removes original source files from Application/User if exists.
                    var filesApplicationUser = groups.TryGetValue("Application/User", out XElement groupApplicationUser) ? groupApplicationUser.Element("Files") : null;

                    if (filesApplicationUser != null)
                    {
                        foreach (var fileName in __originalApplicationFiles.Keys)
                        {
                            if (__originalApplicationFiles[fileName].Split('\\').Last() == "Src")
                            {
                                filesApplicationUser.Descendants("File").FirstOrDefault(file => file.Element("FileName").Value == fileName)?.Remove();
                            }
                        }
                    }

                    // Removes original source group Middlewares/USB_Device_Library if exists.
                    groups.Remove("Middlewares/USB_Device_Library");


                    // Select dictionaries with content.
                    Dictionary<string, string> applicationContent = null;
                    Dictionary<string, string> classDriverContent = null;

                    switch(window._selectedUsbClass)
                    {
                        case UsbClass.Cdc:
                            applicationContent = __cdcApplicationContent;
                            classDriverContent = __cdcDriverContent;
                            break;

                        case UsbClass.CustomHid:
                            applicationContent = __customHidApplicationContent;
                            classDriverContent = __customHidDriverContent;
                            break;

                        default:
                            throw new ApplicationException("Unknown USB class");
                    }

                    if (!groups.ContainsKey("Application/Usb"))
                    {
                        var groupApplicationUsb = new XElement("Group");
                        groupApplicationUsb.Add(new XElement("GroupName", "Application/Usb"));

                        var filesApplicationUsb = new XElement("Files");

                        foreach (var fileName in applicationContent.Keys)
                        {
                            if (Path.GetExtension(fileName) == ".c")
                            {
                                filesApplicationUsb.Add(new XElement("File",
                                    new XElement("FileName", fileName),
                                    new XElement("FileType", 1),
                                    new XElement("FilePath", $"{applicationPath}/{string.Join("/", applicationContent[fileName].Split('\\'))}/{fileName}")));

                                window.WriteLineToLog($"File {fileName} was added in Application/Usb.");
                            }
                        }

                        groupApplicationUsb.Add(filesApplicationUsb);

                        groups.Add("Application/Usb", groupApplicationUsb);
                    }
                    else
                    {
                        var filesApplicationUsb = groups["Application/Usb"].Element("Files");

                        foreach (var fileName in applicationContent.Keys)
                        {
                            if (Path.GetExtension(fileName) == ".c")
                            {
                                filesApplicationUsb.Descendants("File").FirstOrDefault(file => file.Element("FileName").Value == fileName)?.Remove();
                            }
                        }

                        foreach (var fileName in applicationContent.Keys)
                        {
                            if (Path.GetExtension(fileName) == ".c")
                            {
                                filesApplicationUsb.Add(new XElement("File",
                                    new XElement("FileName", fileName),
                                    new XElement("FileType", 1),
                                    new XElement("FilePath", $"{applicationPath}/{string.Join("/", applicationContent[fileName].Split('\\'))}/{fileName}")));

                                window.WriteLineToLog($"File {fileName} was added or updated in Application/Usb.");
                            }
                        }
                    }


                    // Adds new source group Drivers/Usb if not exists.
                    if (!groups.ContainsKey("Drivers/Usb"))
                    {
                        var groupDriversUsb = new XElement("Group");
                        groupDriversUsb.Add(new XElement("GroupName", "Drivers/Usb"));

                        var filesDriversUsb = new XElement("Files");

                        foreach (var fileName in __commonDriverContent.Keys)
                        {
                            if (Path.GetExtension(fileName) == ".c")
                            {
                                filesDriversUsb.Add(new XElement("File",
                                    new XElement("FileName", fileName),
                                    new XElement("FileType", 1),
                                    new XElement("FilePath", $"{driverPath}/{string.Join("/", __commonDriverContent[fileName].Split('\\'))}/{fileName}")));

                                window.WriteLineToLog($"File {fileName} was added in Drivers/Usb.");
                            }
                        }

                        foreach (var fileName in classDriverContent.Keys)
                        {
                            if (Path.GetExtension(fileName) == ".c")
                            {
                                filesDriversUsb.Add(new XElement("File",
                                    new XElement("FileName", fileName),
                                    new XElement("FileType", 1),
                                    new XElement("FilePath", $"{driverPath}/{string.Join("/", classDriverContent[fileName].Split('\\'))}/{fileName}")));

                                window.WriteLineToLog($"File {fileName} was added in Drivers/Usb.");
                            }
                        }

                        groupDriversUsb.Add(filesDriversUsb);

                        groups.Add("Drivers/Usb", groupDriversUsb);
                    }
                    else
                    {
                        var filesDriversUsb = groups["Drivers/Usb"].Element("Files");

                        foreach (var fileName in __commonDriverContent.Keys)
                        {
                            if (Path.GetExtension(fileName) == ".c")
                            {
                                filesDriversUsb.Descendants("File").FirstOrDefault(file => file.Element("FileName").Value == fileName)?.Remove();
                            }
                        }

                        foreach (var fileName in classDriverContent.Keys)
                        {
                            if (Path.GetExtension(fileName) == ".c")
                            {
                                filesDriversUsb.Descendants("File").FirstOrDefault(file => file.Element("FileName").Value == fileName)?.Remove();
                            }
                        }

                        foreach (var fileName in __commonDriverContent.Keys)
                        {
                            if (Path.GetExtension(fileName) == ".c")
                            {
                                filesDriversUsb.Add(new XElement("File",
                                    new XElement("FileName", fileName),
                                    new XElement("FileType", 1),
                                    new XElement("FilePath", $"{driverPath}/{string.Join("/", __commonDriverContent[fileName].Split('\\'))}/{fileName}")));

                                window.WriteLineToLog($"File {fileName} was added or updated in Drivers/Usb.");
                            }
                        }

                        foreach (var fileName in classDriverContent.Keys)
                        {
                            if (Path.GetExtension(fileName) == ".c")
                            {
                                filesDriversUsb.Add(new XElement("File",
                                    new XElement("FileName", fileName),
                                    new XElement("FileType", 1),
                                    new XElement("FilePath", $"{driverPath}/{string.Join("/", classDriverContent[fileName].Split('\\'))}/{fileName}")));

                                window.WriteLineToLog($"File {fileName} was added or updated in Drivers/Usb.");
                            }
                        }
                    }


                    // Try find CMSIS group.
                    var cmsisGroup = groups.FirstOrDefault(g => g.Key == "::CMSIS").Value;
                    if (cmsisGroup != null)
                    {
                        // Remove - sort - add.
                        groups.Remove("::CMSIS");
                    }

                    // Sort groups by name.
                    var sortedGroups = groups.ToList();
                    sortedGroups.Sort((x, y) => x.Key.CompareTo(y.Key));

                    // Add all groups to Groups element.
                    groupsElement.Add(sortedGroups.Select(g => g.Value));
                    groupsElement.Add(cmsisGroup);

                    // Write XML file with ASCII encoding. 
                    using var writer = XmlWriter.Create(window.ProjectPath, new XmlWriterSettings { Encoding = new ASCIIEncoding() });

                    project.Save(writer);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Unable to update project: {ex.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// Tries to remove unnecessary default files.
        /// </summary>
        /// <param name="window"></param>
        private static void TryRemoveDefaultFiles(MainWindow window)
        {
            try
            {
                // Remove unnecessary files from %PROJECT_ROOT%\Middlewares\ST\STM32_USB_Device_Library (USB driver files).
                var rootProjectPath = Path.GetDirectoryName(Path.GetDirectoryName(window.ProjectPath));

                var rootMiddlewaresPath = $"{rootProjectPath}\\Middlewares";
                if (Directory.Exists(rootMiddlewaresPath))
                {
                    var rootStPath = $"{rootMiddlewaresPath}\\ST";
                    if (Directory.Exists(rootStPath))
                    {
                        var rootUsbDevicePath = $"{rootStPath}\\STM32_USB_Device_Library";
                        if (Directory.Exists(rootUsbDevicePath))
                        {
                            // Driver files not backed up.
                            Directory.Delete(rootUsbDevicePath, true);

                            window.WriteLineToLog(@"Directory %PROJECT_ROOT%\Middlewares\ST\STM32_USB_Device_Library was deleted.");
                        }

                        if (!Directory.EnumerateFileSystemEntries(rootStPath).Any())
                        {
                            Directory.Delete(rootStPath);

                            window.WriteLineToLog(@"Directory %PROJECT_ROOT%\Middlewares\ST was deleted.");
                        }
                    }

                    if (!Directory.EnumerateFileSystemEntries(rootMiddlewaresPath).Any())
                    {
                        Directory.Delete(rootMiddlewaresPath);

                        window.WriteLineToLog(@"Directory %PROJECT_ROOT%\Middlewares was deleted.");
                    }
                }

                // Date and time of backup.
                var backupDateTime = DateTime.Now;

                // Remove unnecessary files from %PROJECT_ROOT%\Src and %PROJECT_ROOT%\Inc (files in Application/User).
                foreach (var fileName in __originalApplicationFiles.Keys)
                {
                    var dir = __originalApplicationFiles[fileName].Split('\\').Last(); // Src or Inc

                    var filePath = $"{rootProjectPath}\\{dir}\\{fileName}";

                    if (File.Exists(filePath))
                    {
                        // Backup files before deletion.
                        TryBackup(window.ProjectPath, filePath, backupDateTime);

                        File.Delete(filePath);

                        window.WriteLineToLog($"File %PROJECT_ROOT%\\{dir}\\{fileName} was deleted.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Unable to delete default libraries and files: {ex.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                window.WriteLineToLog($"Deleting files failed: {ex.Message}");
            }
        }

        /// <summary>
        /// Tries to backup unnecessary default files.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="backupDateTime"></param>
        private static void TryBackup(string projectPath, string filePath, DateTime backupDateTime)
        {
            var backupRootDirectory = $"{Path.GetDirectoryName(projectPath)}\\{__fileExtension}backup";

            if (!Directory.Exists(backupRootDirectory))
            {
                Directory.CreateDirectory(backupRootDirectory);
            }

            var backupCurrentDirectory = $"{backupRootDirectory}\\{backupDateTime:ddMMyyHHmmssf}";

            if (!Directory.Exists(backupCurrentDirectory))
            {
                Directory.CreateDirectory(backupCurrentDirectory);
            }

            // Files backed up in  %PROJECT_ROOT%\MDK-ARM\.uvpupdbackup\ddMMyyHHmmssf.
            var backupFilePath = $"{backupCurrentDirectory}\\{Path.GetFileName(filePath)}";

            File.Copy(filePath, backupFilePath);
        }


        /// <summary>
        /// USB classes.
        /// </summary>
        [Flags]
        private enum UsbClass : int
        {
            None        = 0,
            Cdc         = 1,
            CustomHid   = 2,
        }


        /// <summary>
        /// Settings for serialization.
        /// </summary>
        [DataContract]
        private sealed class Settings
        {
            /// <summary>
            /// Path to uVision project file.
            /// </summary>
            [DataMember]
            private readonly string _projectPath;

            /// <summary>
            /// Path to USB library apllication part.
            /// </summary>
            [DataMember]
            private readonly string _applicationPath;

            /// <summary>
            /// Path to USB library driver part.
            /// </summary>
            [DataMember]
            private readonly string _driverPath;

            /// <summary>
            /// Selected USB class.
            /// </summary>
            [DataMember]
            private readonly UsbClass _usbClass;

            /// <summary>
            /// Version of utility in which settings file was created.
            /// </summary>
            [DataMember]
            private readonly Version _version;

            public Settings(string projectPath, string applicationPath, string driverPath, UsbClass usbClass)
            {
                _projectPath = projectPath;
                _applicationPath = applicationPath;
                _driverPath = driverPath;
                _usbClass = usbClass;
                _version = Assembly.GetExecutingAssembly().GetName().Version;
            }

            public string ProjectPath => _projectPath;

            public string ApplicationPath => _applicationPath;

            public string DriverPath => _driverPath;

            public UsbClass UsbClass => _usbClass;

            public Version Version => _version;
        }
    }
}
