﻿using System;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Goldfynger.Usb.Cdc.Reader
{
    class Program
    {
        const string __dateTimeFormat = "HH:mm:ss.ffffff";

        const string __byteFormat = "X2";

        const string __decimalFormat = "D8";

        const int __bytesInLine = 32;

        static readonly int __lineWidth = (__dateTimeFormat.Length /*HH:mm:ss.ffffff*/) + (1 /*":"*/) + (3 * __bytesInLine /*" XX"*/) + (1 /*" "*/) + (__bytesInLine /*"c"*/) + (2 /*" :"*/) + (8 /*"DDDDDDDD"*/);


        static async Task Main(string[] args)
        {
            Console.SetWindowSize(__lineWidth, Console.WindowHeight);
            Console.CursorVisible = false;

            if (args.Length < 1 || string.IsNullOrWhiteSpace(args[0]))
            {
                Console.WriteLine("First argument must be serial port name.");
                Console.ReadLine();
                return;
            }

            if (args.Length == 1)
            {
                Console.WriteLine($"Reader started with serial port name: \"{args[0]}\" and baud rate \"{9600}\".");
            }
            else
            {
                Console.WriteLine($"Reader started with serial port name: \"{args[0]}\" and baud rate \"{args[1]}\".");
            }


            while (true)
            {
                Console.WriteLine($"Waiting for serial port...");


                try
                {
                    while (true)
                    {
                        var serialPortName = SerialPort.GetPortNames().FirstOrDefault(port => string.Compare(port, args[0], true) == 0);

                        if (serialPortName == null)
                        {
                            Thread.Sleep(100);
                            continue;
                        }

                        int baudRate = 9600;

                        if (args.Length > 1)
                        {
                            if (int.TryParse(args[1], out int parsedBaudRate))
                            {
                                baudRate = parsedBaudRate;
                            }
                        }

                        using var serialPort = new SerialPort(serialPortName, baudRate);

                        serialPort.Open();

                        Console.WriteLine($"Serial port {serialPort.PortName} opened. Waiting for data...");

                        var counter = 0;
                        var buffer = new byte[__bytesInLine];

                        while (true)
                        {
                            var readBuffer = new byte[1];

                            await serialPort.BaseStream.ReadAsync(readBuffer.AsMemory(0, 1));


                            var offset = counter % __bytesInLine;

                            // New line.
                            if (offset == 0)
                            {
                                Console.Write($"{DateTime.Now.ToString(__dateTimeFormat)}:");
                            }


                            buffer[offset] = readBuffer[0];

                            Console.Write($" {buffer[offset].ToString(__byteFormat)}");


                            offset = ++counter % __bytesInLine;

                            // Previous line is full.
                            if (offset == 0)
                            {
                                var charRepresentation = new string(Encoding.Default.GetChars(buffer).Select(ch => char.IsLetterOrDigit(ch) || char.IsSymbol(ch) ? ch : '?').ToArray());

                                Console.Write($" {charRepresentation} :{counter.ToString(__decimalFormat)}");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (Console.CursorLeft != 0)
                    {
                        Console.WriteLine();
                    }

                    Console.WriteLine($"Exception: {ex.Message.Replace("\r", string.Empty).Replace("\n", string.Empty)}");
                }
            }
        }
    }
}
