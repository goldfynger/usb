﻿using System;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Goldfynger.Usb.Cdc.Echo
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1 || string.IsNullOrWhiteSpace(args[0]))
            {
                Console.WriteLine("First argument must be serial port name.");
                Console.ReadLine();
                return;
            }

            string baudrateArg = null;

            if (args.Length > 1)
            {
                baudrateArg = args[1];
            }

            Console.WriteLine($"Echo started with serial port name: \"{args[0]}\" baudrate: \"{baudrateArg}\".");


            while (true)
            {
                Console.WriteLine($"Waiting for serial port...");


                try
                {
                    while (true)
                    {
                        var serialPortName = SerialPort.GetPortNames().FirstOrDefault(port => string.Compare(port, args[0], true) == 0);

                        if (serialPortName == null)
                        {
                            Thread.Sleep(100);
                            continue;
                        }

                        using var serialPort = new SerialPort(serialPortName);

                        if (int.TryParse(baudrateArg, out int baudrate))
                        {
                            serialPort.BaudRate = baudrate;
                        }

                        serialPort.Open();

                        Console.WriteLine($"Serial port {serialPort.PortName} opened with baudrate {serialPort.BaudRate}.");


                        while (true)
                        {
                            Console.Write("Enter through space: size of first packet (min size), size of last packet (max size), count of each size tries:");

                            int minLength = 0;
                            int maxLength = 0;
                            int repeatCount = 0;

                            while (true)
                            {
                                var values = Console.ReadLine().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                                if (values.Length != 3 ||
                                    !(int.TryParse(values[0], out minLength) && minLength > 0) ||
                                    !(int.TryParse(values[1], out maxLength) && maxLength >= minLength) ||
                                    !(int.TryParse(values[2], out repeatCount) && repeatCount > 0))
                                {
                                    Console.Write("Try again:");
                                }
                                else
                                {
                                    break;
                                }
                            }

                            Console.WriteLine($"{DateTime.Now:yyyy-MM-ddTHH:mm:ss.fffzzz} Start echo:");


                            var length = minLength;
                            var count = 0;
                            var totalBytes = 0;
                            var beginDateTime = DateTime.Now;
                            var offset = 0;

                            do
                            {
                                totalBytes += length;

                                Console.WriteLine($"{DateTime.Now:yyyy-MM-ddTHH:mm:ss.fffzzz} " +
                                    $"Writing {length} byte(s). Try {count + 1}, total writes {((length - minLength) * repeatCount) + count + 1}, total bytes {totalBytes}.");


                                offset++;

                                var writeBuffer = new byte[length];

                                for (var i = 0; i < length; i++)
                                {
                                    writeBuffer[i] = (byte)((i + offset) & 0xFF);
                                }

                                var readBuffer = new byte[length];


                                Console.WriteLine("Data to write:");

                                Console.Write(BufferToString(writeBuffer));

                                var writeTask = serialPort.BaseStream.WriteAsync(writeBuffer, 0, length);


                                var readTask = Task.Run(() =>
                                {
                                    var idx = 0;

                                    while (idx < length)
                                    {
                                        serialPort.BaseStream.Read(readBuffer, idx, 1);

                                        idx++;
                                    }
                                });


                                using (var tokenSource = new CancellationTokenSource(5000))
                                {
                                    Task.WaitAll(new Task[] { writeTask, readTask }, tokenSource.Token);
                                }


                                Console.WriteLine("Readed data:");

                                Console.Write(BufferToString(readBuffer));


                                if (writeBuffer.SequenceEqual(readBuffer))
                                {
                                    Console.WriteLine($"{DateTime.Now:yyyy-MM-ddTHH:mm:ss.fffzzz} Success.");
                                }
                                else
                                {
                                    Console.WriteLine($"{DateTime.Now:yyyy-MM-ddTHH:mm:ss.fffzzz} Fail.");

                                    throw new ApplicationException("Fail.");
                                }


                                if (++count == repeatCount)
                                {
                                    count = 0;
                                    length++;
                                }
                            }
                            while (length <= maxLength);


                            var totalSeconds = (DateTime.Now - beginDateTime).TotalSeconds;

                            Console.WriteLine($"{DateTime.Now:yyyy-MM-ddTHH:mm:ss.fffzzz} Total seconds: {totalSeconds}, total bytes: {totalBytes}, average speed: {totalBytes / totalSeconds * 8} bits/s.");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"{DateTime.Now:yyyy-MM-ddTHH:mm:ss.fffzzz} Exception: {ex.Message.Replace("\r", string.Empty).Replace("\n", string.Empty)}");
                }
            }
        }


        static string BufferToString(byte[] buffer)
        {
            const int lineLengthInBytes = 32;
            const int lineLengthInChars = lineLengthInBytes * 4;

            Console.SetWindowSize(lineLengthInChars, Console.WindowHeight);

            var isLastLineFull = buffer.Length % lineLengthInBytes == 0;
            var lastLineBytes = isLastLineFull ? lineLengthInBytes : buffer.Length % lineLengthInBytes;
            var lines = buffer.Length / lineLengthInBytes + (isLastLineFull ? 0 : 1);

            var bufferBuilder = new StringBuilder(lines * lineLengthInChars);
            var lineBuffer = new char[lineLengthInChars];
            var stringRepresentation = new string(Encoding.Default.GetChars(buffer).Select(ch => char.IsLetterOrDigit(ch) || char.IsSymbol(ch) ? ch : '?').ToArray());

            for (var line = 0; line < lines; line++)
            {
                var bytesInCurrentLine = (line == lines - 1) ? lastLineBytes : lineLengthInBytes;

                for (var charIdx = 0; charIdx < lineLengthInChars; charIdx++)
                {
                    lineBuffer[charIdx] = ' ';
                }

                for (var byteIdx = 0; byteIdx < bytesInCurrentLine; byteIdx++)
                {
                    var currentByte = buffer[(line * lineLengthInBytes) + byteIdx];
                    var byteRepresentation = currentByte.ToString("X2");
                    var charRepresentation = stringRepresentation[(line * lineLengthInBytes) + byteIdx];

                    lineBuffer[byteIdx * 3] = byteRepresentation[0];
                    lineBuffer[(byteIdx * 3) + 1] = byteRepresentation[1];
                    lineBuffer[(lineLengthInBytes * 3) + byteIdx] = charRepresentation;
                }

                bufferBuilder.Append(lineBuffer);
            }

            return bufferBuilder.ToString();
        }
    }
}
