﻿using System;
using System.Windows;
using System.Windows.Interop;

using Goldfynger.Usb.Cdc.Win32;

namespace Goldfynger.Usb.Cdc.Enumerator
{
    public partial class MainWindow : Window, IDisposable
    {
        private Action<int, IntPtr> _addHook = null;

        private ObservableWin32SerialWatcher _win32Watcher = null;


        public MainWindow()
        {
            InitializeComponent();

            Loaded += MainWindow_Loaded;
        }


        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var handle = new WindowInteropHelper(this).Handle;
            HwndSource.FromHwnd(handle).AddHook(new HwndSourceHook(WndProc));

            _win32Watcher = new ObservableWin32SerialWatcher(handle, addHook => _addHook = addHook);


            dgWin32Watcher.ItemsSource = _win32Watcher.SerialPorts;
        }

        private IntPtr WndProc(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            _addHook?.Invoke(msg, wParam);

            return IntPtr.Zero;
        }


        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    _win32Watcher?.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~MainWindow()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
