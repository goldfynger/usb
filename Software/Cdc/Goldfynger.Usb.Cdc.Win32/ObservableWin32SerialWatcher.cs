﻿using System;
using System.Collections.ObjectModel;

namespace Goldfynger.Usb.Cdc.Win32
{
    /// <summary>
    /// Represents observable serial ports watcher.
    /// </summary>
    public class ObservableWin32SerialWatcher : Win32SerialWatcher
    {
        /// <summary>
        /// Collection of available serial ports.
        /// </summary>
        private readonly ObservableCollection<SerialPortInfo> _serialPorts;


        /// <summary>
        /// Creates new instance observable of serial ports watcher.
        /// </summary>
        /// <param name="wndHandle">Windows handle.</param>
        /// <param name="addWndProcHook">Callback to return a function that needs to be called from WndProc.</param>
        /// <remarks>This type available only on Windows.</remarks>
        public ObservableWin32SerialWatcher(IntPtr wndHandle, Action<Action<int, IntPtr>> addWndProcHook) : base(wndHandle, addWndProcHook)
        {
            if (addWndProcHook == null)
            {
                throw new ArgumentNullException(nameof(addWndProcHook));
            }


            _serialPorts = new ObservableCollection<SerialPortInfo>(SerialPortInfo.FindSerialPorts()); /* Search for serial ports first time. */

            SerialPorts = new ReadOnlyObservableCollection<SerialPortInfo>(_serialPorts);
        }


        /// <summary>
        /// Read only collection of available serial ports.
        /// </summary>
        public ReadOnlyObservableCollection<SerialPortInfo> SerialPorts { get; }


        /// <summary>
        /// Function invoked when serial port arrived.
        /// </summary>
        protected override void OnSerialPortArrived()
        {
            base.OnSerialPortArrived();

            UpdateCollection();
        }

        /// <summary>
        /// Function invoked when serial port removed.
        /// </summary>
        protected override void OnSerialPortRemoved()
        {
            base.OnSerialPortRemoved();

            UpdateCollection();
        }

        /// <summary>
        /// Update serial ports collection.
        /// </summary>
        private void UpdateCollection()
        {
            var actualPorts = SerialPortInfo.FindSerialPorts();

            var removeIdx = 0;

            while (true) /* Remove all unavailable ports. */
            {
                if (removeIdx >= _serialPorts.Count) /* End of collection. */
                {
                    break;
                }

                if (actualPorts.IndexOf(_serialPorts[removeIdx]) < 0) /* Not found in actual collection - port removed. */
                {
                    _serialPorts.RemoveAt(removeIdx);
                }
                else /* Else increment index. */
                {
                    removeIdx++;
                }
            }

            /* Here we have old collection and new actual collection that can have more items than old. */
            /* We can save order returned by FindSerialPorts and insert new ports in right places. */

            for (var addIdx = 0; addIdx < actualPorts.Count; addIdx++) /* Add. */
            {
                if (_serialPorts.IndexOf(actualPorts[addIdx]) < 0) /* Not found in exisiting collection - new port. */
                {
                    _serialPorts.Insert(addIdx, actualPorts[addIdx]);
                }
            }
        }
    }
}
