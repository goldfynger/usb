﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

using Goldfynger.Utils.Extensions;

namespace Goldfynger.Usb.Cdc.Win32
{
    /// <summary>
    /// Container for serial port info.
    /// </summary>
    public sealed record SerialPortInfo : IEquatable<SerialPortInfo>
    {
        private const string __vidPrefix = "VID_";

        private const string __pidPrefix = "PID_";

        private static readonly ReadOnlyCollection<string> __emptyCollection = new(Array.Empty<string>());


        private SerialPortInfo(int portNumber, string portName, string friendlyName, IList<string>? hardwareIds, string? service)
        {
            PortNumber = portNumber;
            PortName = portName;

            FriendlyName = friendlyName;

            var comStart = friendlyName.LastIndexOf("COM");
            var comEnd = friendlyName.LastIndexOf(')');

            if (comStart != -1 && comEnd != -1 && comStart < comEnd)
            {
                Debug.Assert(portName == friendlyName[comStart..comEnd]);

                if (comStart > 1)
                {
                    Description = friendlyName.Substring(0, comStart - 1).TrimEnd();
                }
            }

            if (hardwareIds != null)
            {
                HardwareIds = new ReadOnlyCollection<string>(hardwareIds);

                var completeString = hardwareIds.Aggregate((i, j) => i + j).ToUpperInvariant();

                if (completeString.Contains(__vidPrefix) && completeString.Contains(__pidPrefix))
                {
                    Vid = ushort.Parse(completeString.Substring(completeString.IndexOf(__vidPrefix) + __vidPrefix.Length, 4), NumberStyles.HexNumber);

                    Pid = ushort.Parse(completeString.Substring(completeString.IndexOf(__pidPrefix) + __pidPrefix.Length, 4), NumberStyles.HexNumber);
                }
            }

            Service = service;
        }


        /// <summary>
        /// Port number.
        /// </summary>
        /// <remarks>For example "5" if <see cref="PortName"/> is "COM5".</remarks>
        public int PortNumber { get; }

        /// <summary>
        /// Port name. This value can be used to open port with <see cref="SerialPort.Open"/>.
        /// </summary>
        /// <remarks>For example "COM5".</remarks>
        public string PortName { get; }

        /// <summary>
        /// Friendly name.
        /// </summary>
        /// <remarks>For example "USB Serial Port (COM5)".</remarks>
        public string FriendlyName { get; }

        /// <summary>
        /// Hardware identifiers.
        /// </summary>
        /// <remarks>Some strings with hardware identifiers or empty collection if hardware identifiers not found.</remarks>
        public ReadOnlyCollection<string> HardwareIds { get; } = __emptyCollection;

        /// <summary>
        /// Service.
        /// </summary>
        /// <remarks><see langword="null"/> if service not found.</remarks>
        public string? Service { get; }

        /// <summary>
        /// Description.
        /// </summary>
        /// <remarks>Part of friendly name: for example if friendly name is "USB Serial Port (COM5)" description will be look like "USB Serial Port".</remarks>
        public string? Description { get; } = null;

        /// <summary>
        /// Vendor ID.
        /// </summary>
        /// <remarks><see langword="null"/> if hardware identifiers not found or hardware identifiers do not contains information about vendor ID in awaitable format.</remarks>
        public ushort? Vid { get; } = null;

        /// <summary>
        /// Product ID.
        /// </summary>
        /// <remarks><see langword="null"/> if hardware identifiers not found or hardware identifiers do not contains information about product ID in awaitable format.</remarks>
        public ushort? Pid { get; } = null;


        /// <summary>
        /// <see cref="FriendlyName"/> of serial port.
        /// </summary>
        /// <returns>Friendly name.</returns>
        public override string ToString() => FriendlyName;

        /// <summary>
        /// Indicates whether the current object is equal to another object.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns><see langword="true"/> if the current object is equal to the other parameter; otherwise, <see langword="false"/>.</returns>
        public bool Equals(SerialPortInfo? other)
        {
            if (other == null) return false;
            if (PortNumber != other.PortNumber || PortName != other.PortName) return false;
            if (FriendlyName != other.FriendlyName || Service != other.Service || Description != other.Description || Vid != other.Vid || Pid != other.Pid) return false;
            if (HardwareIds.Count != other.HardwareIds.Count) return false;
            for (var hardwareIdIdx = 0; hardwareIdIdx < HardwareIds.Count; hardwareIdIdx++)
            {
                if (HardwareIds[hardwareIdIdx] != other.HardwareIds[hardwareIdIdx]) return false;
            }
            return true;
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>A 32-bit signed integer hash code.</returns>
        public override int GetHashCode() => PortNumber;

        /// <summary>
        /// Returns information about known serial ports.
        /// </summary>
        /// <returns>Read only collection of all serial ports ordered by serial port number.</returns>
        /// <remarks>Result collection contains only serial ports with names like "COM5": "COM" and positive integer port number without any other symbols. This function available only on Windows.</remarks>
        public static ReadOnlyCollection<SerialPortInfo> FindSerialPorts()
        {
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                throw new PlatformNotSupportedException($"{nameof(FindSerialPorts)}: function available only on OS Windows.");
            }

            /* Windows implementation of SerialPort.GetPortNames uses registry entries from HKEY_LOCAL_MACHINE\HARDWARE\DEVICEMAP\SERIALCOMM and may return
             * a non-existent serial port if the port was opened by an application and then removed. Another problem is when this port was arrived again,
             * SerialPort.GetPortNames result contains the name of this port twice.
             * To avoid these problems, the current implementation uses ports from Device Manager only with known friendly names.
             * Port names should be exists in SerialPort.GetPortNames for using as SerialPort.PortName. */

            var knownSerialPorts = SerialPort.GetPortNames();

            var serialGuid = Win32Native.GUID_DEVCLASS_PORTS;

            using var devInfoSet = Win32Native.SetupDiGetClassDevs(
                ref serialGuid,
                null,
                IntPtr.Zero,
                Win32Native.DIGCF.DIGCF_PRESENT);

            if (devInfoSet.IsInvalid)
            {
                var win32Exception = new Win32Exception(Marshal.GetLastWin32Error());
                throw new ApplicationException($"Failed to get device info set: ({win32Exception.NativeErrorCode:X8}) {win32Exception.Message}", win32Exception);
            }

            uint propertyBufferSize = 256;
            byte[] propertyBuffer = new byte[propertyBufferSize];

            var devInfoData = new Win32Native.SP_DEVINFO_DATA();
            devInfoData.cbSize = (uint)Marshal.SizeOf(devInfoData);

            var infos = new List<SerialPortInfo>();

            /* Iterates through the InfoSet memory block assigned within Windows in the call to SetupDiGetClassDevs to get device details for each device connected. */
            uint idx = 0;
            var enumerationResult = false;
            do
            {
                enumerationResult = Win32Native.SetupDiEnumDeviceInfo(
                    devInfoSet,
                    idx,
                    ref devInfoData);

                if (enumerationResult)
                {
                    string? friendlyName = null;
                    string? portName = null;
                    int? portNumber = null;
                    string[]? hardwareIds = null;
                    string? service = null;                    


                    if (!Win32Native.SetupDiGetDeviceRegistryProperty(
                        devInfoSet,
                        ref devInfoData,
                        Win32Native.SPDRP.SPDRP_FRIENDLYNAME,
                        out uint PropertyRegDataType,
                        propertyBuffer,
                        propertyBufferSize,
                        out uint RequiredSize))
                    {
                        var win32Exception = new Win32Exception(Marshal.GetLastWin32Error());
                        Debug.WriteLine(
                            $"{nameof(SerialPortInfo)}: {nameof(FindSerialPorts)}: Failed to get {nameof(Win32Native.SPDRP.SPDRP_FRIENDLYNAME)} property: ({win32Exception.NativeErrorCode:X8}) {win32Exception.Message}");

                        continue; /* We can not use this port: we need port friendly name to determine port name and port number.. */
                    }
                    else
                    {
                        friendlyName = Encoding.Unicode.GetString(propertyBuffer).TrimEnd('\0');

                        if (!TryGetPortNameAndNumberForFriendlyName(friendlyName, out portName, out portNumber))
                        {
                            continue; /* We can not use this port: we need port name and port number. */
                        }

                        if (knownSerialPorts.All(p => p != portName))
                        {
                            continue; /* We can not use this port: it is not exists in SerialPort.GetPortNames. */
                        }
                    }

                    propertyBuffer.Clear();


                    if (!Win32Native.SetupDiGetDeviceRegistryProperty(
                        devInfoSet,
                        ref devInfoData,
                        Win32Native.SPDRP.SPDRP_HARDWAREID,
                        out PropertyRegDataType,
                        propertyBuffer,
                        propertyBufferSize,
                        out RequiredSize))
                    {
                        var win32Exception = new Win32Exception(Marshal.GetLastWin32Error());
                        Debug.WriteLine(
                            $"{nameof(SerialPortInfo)}: {nameof(FindSerialPorts)}: Failed to get {nameof(Win32Native.SPDRP.SPDRP_SERVICE)} property: ({win32Exception.NativeErrorCode:X8}) {win32Exception.Message}");
                    }
                    else
                    {
                        hardwareIds = Encoding.Unicode.GetString(propertyBuffer).TrimEnd('\0').Split('\0');
                    }

                    propertyBuffer.Clear();
                    

                    if (!Win32Native.SetupDiGetDeviceRegistryProperty(
                        devInfoSet,
                        ref devInfoData,
                        Win32Native.SPDRP.SPDRP_SERVICE,
                        out PropertyRegDataType,
                        propertyBuffer,
                        propertyBufferSize,
                        out RequiredSize))
                    {
                        var win32Exception = new Win32Exception(Marshal.GetLastWin32Error());
                        Debug.WriteLine(
                            $"{nameof(SerialPortInfo)}: {nameof(FindSerialPorts)}: Failed to get {nameof(Win32Native.SPDRP.SPDRP_SERVICE)} property: ({win32Exception.NativeErrorCode:X8}) {win32Exception.Message}");
                    }
                    else
                    {
                        service = Encoding.Unicode.GetString(propertyBuffer).TrimEnd('\0');
                    }

                    propertyBuffer.Clear();

                    infos.Add(new SerialPortInfo(portNumber.Value, portName, friendlyName, hardwareIds, service));

                    idx++;
                }
                else
                {
                    var win32Exception = new Win32Exception(Marshal.GetLastWin32Error());
                    if (win32Exception.NativeErrorCode != (int)Win32Native.ERROR.ERROR_NO_MORE_ITEMS)
                    {
                        throw new ApplicationException($"Failed to enumerate devices: ({win32Exception.NativeErrorCode:X8}) {win32Exception.Message}", win32Exception);
                    }
                }
            }
            while (enumerationResult);

            return new ReadOnlyCollection<SerialPortInfo>(infos.OrderBy(i => i.PortNumber).ToList());
        }

        private static bool TryGetPortNameAndNumberForFriendlyName(string friendlyName, [NotNullWhen(true)] out string? portName, [NotNullWhen(true)] out int? portNumber)
        {
            /* Awaiting friendly name is string like "USB Serial Port (COM5)". We need find and return port name "COM5" and port number "5". */

            var comStart = friendlyName.LastIndexOf("COM");
            var comEnd = friendlyName.LastIndexOf(')');

            if (comStart != -1 && comEnd != -1 && comStart < comEnd)
            {
                portName = friendlyName[comStart..comEnd];

                var stringPortNumber = portName[3..];

                if (stringPortNumber.All(c => c >= '0' && c <= '9') && int.TryParse(stringPortNumber, out int intPortNumber))
                {
                    portNumber = intPortNumber;
                    return true;
                }
            }

            portName = null;
            portNumber = null;
            return false;
        }
    }
}
