﻿using System;
using System.Runtime.InteropServices;

namespace Goldfynger.Usb.Cdc.Win32
{
    /// <summary>
    /// Represents serial ports watcher.
    /// </summary>
    public class Win32SerialWatcher : IDisposable
    {
        /// <summary>
        /// Internal device notification handle.
        /// </summary>
        private readonly SafeDeviceNotificationHandle _notifyHandle;


        /// <summary>
        /// This event will be triggered when a serial port is arrived.
        /// </summary>
        public event EventHandler? SerialPortArrived;

        /// <summary>
        /// This event will be triggered when a serial port is removed.
        /// </summary>
        public event EventHandler? SerialPortRemoved;


        /// <summary>
        /// Creates new instance of serial ports watcher.
        /// </summary>
        /// <param name="wndHandle">Windows handle.</param>
        /// <param name="addWndProcHook">Callback to return a function that needs to be called from WndProc.</param>
        /// <remarks>This type available only on Windows.</remarks>
        public Win32SerialWatcher(IntPtr wndHandle, Action<Action<int, IntPtr>> addWndProcHook)
        {
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                throw new PlatformNotSupportedException($"{nameof(Win32SerialWatcher)}: type available only on OS Windows.");
            }


            addWndProcHook(new Action<int, IntPtr>((msg, wParam) =>
            {
                if (msg == (int)Win32Native.WM.WM_DEVICECHANGE)
                {
                    switch (wParam.ToInt64())
                    {
                        case (long)Win32Native.DBT.DBT_DEVICEARRIVAL:
                            OnSerialPortArrived();
                            break;

                        case (long)Win32Native.DBT.DBT_DEVICEREMOVECOMPLETE:
                            OnSerialPortRemoved();
                            break;
                    }
                }
            }));


            var devInterface = new Win32Native.DEV_BROADCAST_DEVICEINTERFACE
            {
                dbcc_classguid = Win32Native.GUID_DEVINTERFACE_COMPORT,
                dbcc_devicetype = Win32Native.DBT_DEVTYP.DBT_DEVTYP_DEVICEINTERFACE,
                dbcc_reserved = 0
            };
            devInterface.dbcc_size = (uint)Marshal.SizeOf(devInterface);

            /* Registry for notifications. */
            _notifyHandle = Win32Native.RegisterDeviceNotification(wndHandle, ref devInterface, Win32Native.DEVICE_NOTIFY.DEVICE_NOTIFY_WINDOW_HANDLE);
        }


        /// <summary>
        /// Function invoked when serial ports arrived.
        /// </summary>
        protected virtual void OnSerialPortArrived() => SerialPortArrived?.Invoke(this, EventArgs.Empty);

        /// <summary>
        /// Function invoked when serial ports removed.
        /// </summary>
        protected virtual void OnSerialPortRemoved() => SerialPortRemoved?.Invoke(this, EventArgs.Empty);


        /// <summary>
        /// Flag: Has Dispose already been called?
        /// </summary>
        private bool _disposed = false;

        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    _notifyHandle?.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~Win32SerialWatcher()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        /// <summary>
        /// Public implementation of Dispose pattern callable by consumers.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
