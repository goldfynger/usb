﻿using System;
using System.Runtime.InteropServices;

namespace Goldfynger.Usb.Cdc.Win32
{
    /// <summary>
    /// Internal class with WinAPI infrastructure.
    /// </summary>
    internal static class Win32Native
    {
        /// <summary>
        /// Used with <see cref="SetupDiGetClassDevs"/> to get information about serial ports.
        /// </summary>
        internal static readonly Guid GUID_DEVCLASS_PORTS = new(0x4D36E978, 0xE325, 0x11CE, 0xBF, 0xC1, 0x08, 0x00, 0x2B, 0xE1, 0x03, 0x18);

        /// <summary>
        /// Used with <see cref="DEV_BROADCAST_DEVICEINTERFACE"/> to notify for serial ports events.
        /// </summary>
        internal static readonly Guid GUID_DEVINTERFACE_COMPORT = new(0x86E0D1E0, 0x8089, 0x11D0, 0x9C, 0xE4, 0x08, 0x00, 0x3E, 0x30, 0x1F, 0x73);


        /// <summary>
        /// Defines a device instance that is a member of a device information set.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        internal struct SP_DEVINFO_DATA
        {
            public uint cbSize;
            public Guid ClassGuid;
            public uint DevInst;
            public UIntPtr Reserved;
        }

        /// <summary>
        /// Contains information about a class of devices.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode, Pack = 1)]
        internal struct DEV_BROADCAST_DEVICEINTERFACE
        {
            public uint dbcc_size;
            [MarshalAs(UnmanagedType.U4)]
            public DBT_DEVTYP dbcc_devicetype;
            public uint dbcc_reserved;
            public Guid dbcc_classguid;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string dbcc_name;
        }


        /// <summary>
        /// System error codes returned by the GetLastError function.
        /// </summary>
        internal enum ERROR : uint
        {
            /// <summary>No more data is available.</summary>
            ERROR_NO_MORE_ITEMS = 259,
        }

        /// <summary>
        /// Used in <see cref="SetupDiGetClassDevs"/> function. Specifies control options that filter the device information elements that are added to the device information set.
        /// </summary>
        [Flags]
        internal enum DIGCF : uint
        {
            /// <summary><see cref="SetupDiGetClassDevs"/>: Return only devices that are currently present in a system.</summary>
            DIGCF_PRESENT = 0x00000002,
            /// <summary><see cref="SetupDiGetClassDevs"/>: Return devices that support device interfaces for the specified device interface classes.</summary>
            DIGCF_DEVICEINTERFACE = 0x00000010,
        }

        /// <summary>
        /// Used in <see cref="SetupDiGetDeviceRegistryProperty"/> function. Specifies device property.
        /// </summary>
        internal enum SPDRP : uint
        {
            /// <summary>Device description (REG_SZ).</summary>
            SPDRP_DEVICEDESC = 0x00000000,
            /// <summary>Hardware Ids (REG_MULTI_SZ).</summary>
            SPDRP_HARDWAREID = 0x00000001,
            /// <summary>Service (REG_SZ).</summary>
            SPDRP_SERVICE = 0x00000004,
            /// <summary>Friendly name (REG_SZ).</summary>
            SPDRP_FRIENDLYNAME = 0x0000000C,
        }

        /// <summary>
        /// Used to select device type.
        /// </summary>
        internal enum DBT_DEVTYP : uint
        {
            /// <summary>Class of devices.</summary>
            DBT_DEVTYP_DEVICEINTERFACE = 0x00000005,
        }

        /// <summary>
        /// Used in <see cref="RegisterDeviceNotification"/> function. Specifies function paramters.
        /// </summary>
        [Flags]
        internal enum DEVICE_NOTIFY : uint
        {
            /// <summary>RegisterDeviceNotification : The hRecipient parameter is a window handle.</summary>
            DEVICE_NOTIFY_WINDOW_HANDLE = 0x00000000,
            /// <summary>RegisterDeviceNotification : The hRecipient parameter is a service status handle.</summary>
            DEVICE_NOTIFY_SERVICE_HANDLE = 0x00000001,
        }

        /// <summary>
        /// Windows messages.
        /// </summary>
        internal enum WM : uint
        {
            /// <summary>Device management message.</summary>
            WM_DEVICECHANGE = 0x00000219,
        }

        /// <summary>
        /// Device management events.
        /// </summary>
        internal enum DBT : ulong
        {
            /// <summary>A device or piece of media has been inserted and is now available.</summary>
            DBT_DEVICEARRIVAL = 0x0000000000008000,
            /// <summary>A device or piece of media has been removed.</summary>
            DBT_DEVICEREMOVECOMPLETE = 0x0000000000008004,
        }


        /// <summary>
        /// Allocates an InfoSet memory block within Windows that contains details of devices.
        /// </summary>
        /// <param name="ClassGuid">Class guid.</param>
        /// <param name="Enumerator">Not used.</param>
        /// <param name="hwndParent">Not used.</param>
        /// <param name="Flags">Type of device details required (DIGCF_ constants).</param>
        /// <returns>A reference to the InfoSet.</returns>
        [DllImport("setupapi.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern SafeDeviceInfoSetHandle SetupDiGetClassDevs(
            ref Guid ClassGuid,
            [MarshalAs(UnmanagedType.LPWStr)] string? Enumerator,
            IntPtr hwndParent,
            [MarshalAs(UnmanagedType.U4)] DIGCF Flags);

        /// <summary>
        /// Frees InfoSet allocated in call to above.
        /// </summary>
        /// <param name="DeviceInfoSet">Reference to InfoSet.</param>
        /// <returns>True if successful.</returns>
        [DllImport("setupapi.dll", SetLastError = true)]
        internal static extern bool SetupDiDestroyDeviceInfoList(IntPtr DeviceInfoSet);

        /// <summary>
        /// Returns a <see cref="SP_DEVINFO_DATA"/> structure that specifies a device information element in a device information set.
        /// </summary>
        /// <param name="DeviceInfoSet">InfoSet allocated by <see cref="SetupDiGetClassDevs"/>.</param>
        /// <param name="MemberIndex">A zero-based index of the device information element to retrieve.</param>
        /// <param name="DeviceInfoData">Structure with information about an enumerated device information element.</param>
        /// <returns>True if successful.</returns>
        [DllImport("setupapi.dll", SetLastError = true)]
        internal static extern bool SetupDiEnumDeviceInfo(
            SafeDeviceInfoSetHandle DeviceInfoSet,
            uint MemberIndex,
            ref SP_DEVINFO_DATA DeviceInfoData);

        /// <summary>
        /// Retrieves a specified Plug and Play device property.
        /// </summary>
        /// <param name="DeviceInfoSet">InfoSet allocated by <see cref="SetupDiGetClassDevs"/>.</param>
        /// <param name="DeviceInfoData">InfoData that filled by <see cref="SetupDiEnumDeviceInfo"/>.</param>
        /// <param name="Property">Specifies the property to be retrieved.</param>
        /// <param name="PropertyRegDataType">Property data type.</param>
        /// <param name="PropertyBuffer">Buffer with property data.</param>
        /// <param name="PropertyBufferSize">Size of buffer.</param>
        /// <param name="RequiredSize">Required size of buffer to store property data in it.</param>
        /// <returns>True if successful.</returns>
        [DllImport("setupapi.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern bool SetupDiGetDeviceRegistryProperty(
            SafeDeviceInfoSetHandle DeviceInfoSet,
            ref SP_DEVINFO_DATA DeviceInfoData,
            SPDRP Property,
            out uint PropertyRegDataType,
            byte[] PropertyBuffer,
            uint PropertyBufferSize,
            out uint RequiredSize);

        /// <summary>
        /// Registers a window or service for device insert/remove messages.
        /// </summary>
        /// <param name="hRecipient">Handle to the window or service that will receive the messages.</param>
        /// <param name="NotificationFilter">DeviceBroadcastInterrface structure.</param>
        /// <param name="Flags">Specified flags.</param>
        /// <returns>A handle used when unregistering.</returns>
        [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern SafeDeviceNotificationHandle RegisterDeviceNotification(
            IntPtr hRecipient,
            ref DEV_BROADCAST_DEVICEINTERFACE NotificationFilter,
            [MarshalAs(UnmanagedType.U4)] DEVICE_NOTIFY Flags);

        /// <summary>
        /// Closes device notification handle.
        /// </summary>
        /// <param name="Handle">Handle returned in call to RegisterDeviceNotification.</param>
        /// <returns>True if successful.</returns>
        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool UnregisterDeviceNotification(IntPtr Handle);
    }
}
