﻿using System;
using System.IO.Ports;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Goldfynger.Usb.Cdc.BlinkButtons
{
    public partial class MainWindow : Window
    {
        private SerialPort _serialPort = null;

        private States _state = States.None;

        private event EventHandler<StateEventArgs> ReadCompleted;


        public MainWindow()
        {
            InitializeComponent();

            Loaded += MainWindow_Loaded;
        }


        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            cbSelectPort.DropDownOpened += CbSelectPort_DropDownOpened;
            bOpenPort.Click += BOpenPort_Click;

            bBlue.Click += BBlue_Click;
            bGreen.Click += BGreen_Click;
            bRed.Click += BRed_Click;
            bYellow.Click += BYellow_Click;

            ReadCompleted += UsbDevice_ReadCompleted;
        }

        private void CbSelectPort_DropDownOpened(object sender, EventArgs e)
        {
            cbSelectPort.ItemsSource = SerialPort.GetPortNames();
        }

        private void BOpenPort_Click(object sender, RoutedEventArgs e)
        {
            if (_serialPort == null)
            {
                TryConnect();
            }
            else
            {
                TryDispose();
            }
        }

        private void BBlue_Click(object sender, RoutedEventArgs e)
        {
            ToggleState(States.Blue);
            UpdateLabels();
            WriteState(_state);
        }

        private void BGreen_Click(object sender, RoutedEventArgs e)
        {
            ToggleState(States.Green);
            UpdateLabels();
            WriteState(_state);
        }

        private void BRed_Click(object sender, RoutedEventArgs e)
        {
            ToggleState(States.Red);
            UpdateLabels();
            WriteState(_state);
        }

        private void BYellow_Click(object sender, RoutedEventArgs e)
        {
            ToggleState(States.Yellow);
            UpdateLabels();
            WriteState(_state);
        }

        private void UsbDevice_ReadCompleted(object sender, StateEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                if (e.State.HasFlag(States.Blue))
                {
                    ToggleState(States.Blue);
                }

                if (e.State.HasFlag(States.Green))
                {
                    ToggleState(States.Green);
                }

                if (e.State.HasFlag(States.Red))
                {
                    ToggleState(States.Red);
                }

                if (e.State.HasFlag(States.Yellow))
                {
                    ToggleState(States.Yellow);
                }

                WriteState(_state);
                UpdateLabels();
            });
        }


        private void TryConnect()
        {
            if (_serialPort != null)
                return;

            var serialPortName = cbSelectPort.SelectedItem?.ToString();

            if (serialPortName != null)
            {
                try
                {
                    _serialPort = new SerialPort(serialPortName);
                    _serialPort.Open();
                }
                catch
                {
                    bStatus.BorderBrush = Brushes.Red;

                    return;
                }

                RunRead();

                WriteState(_state);

                bStatus.BorderBrush = Brushes.Green;

                bOpenPort.Content = "Close";

                bBlue.IsEnabled = true;
                bRed.IsEnabled = true;
                bGreen.IsEnabled = true;
                bYellow.IsEnabled = true;
            }
        }

        private void TryDispose()
        {
            if (_serialPort == null)
                return;

            _serialPort.Dispose();
            _serialPort = null;

            bStatus.BorderBrush = Brushes.Red;

            bOpenPort.Content = "Open";

            bBlue.IsEnabled = false;
            bRed.IsEnabled = false;
            bGreen.IsEnabled = false;
            bYellow.IsEnabled = false;
        }

        private void ToggleState(States flag)
        {
            if (_state.HasFlag(flag))
            {
                _state &= ~flag;
            }
            else
            {
                _state |= flag;
            }
        }

        private void UpdateLabels()
        {
            if (_state.HasFlag(States.Blue))
            {
                lBlue.Opacity = 1;
            }
            else
            {
                lBlue.Opacity = 0.2;
            }

            if (_state.HasFlag(States.Green))
            {
                lGreen.Opacity = 1;
            }
            else
            {
                lGreen.Opacity = 0.2;
            }

            if (_state.HasFlag(States.Red))
            {
                lRed.Opacity = 1;
            }
            else
            {
                lRed.Opacity = 0.2;
            }

            if (_state.HasFlag(States.Yellow))
            {
                lYellow.Opacity = 1;
            }
            else
            {
                lYellow.Opacity = 0.2;
            }
        }

        private async void WriteState(States state)
        {
            try
            {
                if (_serialPort != null)
                {
                    await _serialPort.BaseStream.WriteAsync((new byte[] { (byte)state }).AsMemory(0, 1));
                }
            }
            catch
            {
                TryDispose();
            }
        }

        private void RunRead()
        {
            Task.Run(async () =>
            {
                while (true)
                {
                    var buffer = new byte[1];

                    await _serialPort.BaseStream.ReadAsync(buffer.AsMemory(0, 1));

                    ReadCompleted?.Invoke(this, new StateEventArgs((States)buffer[0]));
                }
            });
        }

        private sealed class StateEventArgs : EventArgs
        {
            internal StateEventArgs(States state)
            {
                State = state;
            }

            internal States State { get; }
        }


        [Flags]
        private enum States : byte
        {
            None = 0,
            Blue = 1,
            Green = 2,
            Red = 4,
            Yellow = 8
        }
    }
}
