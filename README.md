USB-device library for STM32F103 MCUs based on STM HAL 1.8.2.

Examples based on BluePill board with extensions:

*  USB Pull-up resistor connected to PB1.

*  External board with 4 leds and 4 buttons.

## Structure

| Project                                                                                  | Description                                                           |
|------------------------------------------------------------------------------------------|-----------------------------------------------------------------------|
| [**Firmware/F103/CdcEcho**](Firmware/F103/CdcEcho)                                       | USB CDC example: simple echo project.                                 |
| [**Firmware/F103/CdcBlinkButtons**](Firmware/F103/CdcBlinkButtons)                       | USB CDC example: simple project with buttons and leds.                |
| [**Firmware/F103/CustomHidEcho**](Firmware/F103/CustomHidEcho)                           | USB custom HID example: simple echo project.                          |
| [**Firmware/F103/CustomHidEchoOs**](Firmware/F103/CustomHidEchoOs)                       | USB custom HID example: simple echo project with FreeRTOS.            |
| [**Firmware/F103/CustomHidBlinkButtons**](Firmware/F103/CustomHidBlinkButtons)           | USB custom HID example: simple project with buttons and leds.         |
| [**Firmware/F103/Usb/1.8.2**](Firmware/F103/Usb/1.8.2)                                   | USB library implementation for STM32F103 MCUs based on STM HAL 1.8.2. |
| [**Software/Cdc/CdcReader**](Software/Cdc/CdcReader)                                     | Console application for serial port data reading.                     |
| [**Software/Cdc/CdcEcho**](Software/Cdc/CdcEcho)                                         | Console application for CDC echo test.                                |
| [**Software/Cdc/CdcBlinkButtons**](Software/Cdc/CdcBlinkButtons)                         | Application for control BlinkButtons over serial port.                |
| [**Software/CustomHid/CustomHidEnumerator**](Software/CustomHid/CustomHidEnumerator)     | Application for custom HID enumeration.                               |
| [**Software/CustomHid/CustomHidReader**](Software/CustomHid/CustomHidReader)             | Console application for data reading over custom HID reports.         |
| [**Software/CustomHid/CustomHidEcho**](Software/CustomHid/CustomHidEcho)                 | Application for custom HID echo test.                                 |
| [**Software/CustomHid/CustomHidBlinkButtons**](Software/CustomHid/CustomHidBlinkButtons) | Application for control BlinkButtons over custom HID reports.         |
| [**Software/CustomHid/CustomHidLib**](Software/CustomHid/CustomHidLib)                   | Custom HID exchange library.                                          |
| [**Software/ExchangeMonitor**](Software/ExchangeMonitor)                                 | Console application for exchange visualization.                       |
| [**Software/UsbProjectUpdater**](Software/UsbProjectUpdater)                             | Application for replacing original USB library with custom.           |
| [**Software/Utils**](Software/Utils)                                                     | Useful utilites.                                                      |
